<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CampoPrivado extends Model
{
    protected $table = "private_fields";
}
