<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $table = 'empresas';

    public function user(){
    	return $this->hasOne('App\User', 'user_id');
    }

    public function categoria(){
    	return $this->hasOne('App\categoria', 'categoria_id');
    }
}
