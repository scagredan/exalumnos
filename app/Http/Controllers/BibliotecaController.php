<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Biblioteca;

class BibliotecaController extends Controller
{
     public function index(){
    	$bibliotecas = Biblioteca::paginate(10);
    	return view('biblioteca.index')->with('bibliotecas', $bibliotecas);
   	}

   	public function store(Request $request){
   		$biblioteca = new Biblioteca();
   		$biblioteca->name = $request->name;
      $biblioteca->descripcion = $request->descripcion;
      $biblioteca->biblioteca_id = $request->biblioteca_id;
      $biblioteca->user_id = $request->user_id;
   		$biblioteca->save();
   		return redirect()->back()->with('Éxito, se ha creado la biblioteca.');
   	}

   	public function delete(Request $request){
   		$biblioteca = Biblioteca::find($request->identificador);
   		$biblioteca->delete();
   		return redirect()->back()->with('Éxito, se ha elminado la biblioteca.');	
   	}

   	public function update(){
   		$biblioteca = Biblioteca::find($request->identificador);
   		$biblioteca = $request->name;
      $biblioteca->descripcion = $request->descripcion;
      $biblioteca->biblioteca_id = $request->biblioteca_id;
      $biblioteca->user_id = $request->user_id;
   		$biblioteca->save();
   		return redirect()->back()->with('Éxito, se ha actualizado la biblioteca correctamente');
   	}
}
