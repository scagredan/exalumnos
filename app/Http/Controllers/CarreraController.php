<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Carrera;

class CarreraController extends Controller
{
    public function index(){
    	$carreras = Carrera::all();
    	return view('carrera.index')->with('carreras', $carreras);
   	}

   	public function store(Request $request){
   		$carrera = new Carrera();
   		$carrera->name = $request->name;
   		$carrera->save();
   		return redirect()->back()->with('Éxito, se ha creado la carrera.');
   	}

   	public function delete(Request $request){
   		$carrera = Carrera::find($request->identificador);
   		$carrera->delete();
   		return redirect()->back()->with('Éxito, se ha elminado la carrera.');	
   	}

   	public function update(){
   		$carrera = Carrera::find($request->identificador);
   		$carrera = $request->name;
   		$carrera->save();
   		return redirect()->back()->with('Éxito, se ha actualizado la carrera correctamente');
   	}
}
