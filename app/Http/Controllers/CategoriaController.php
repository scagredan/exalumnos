<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;

class CategoriaController extends Controller
{
    public function index(){
      $categorias = Categoria::all();
      return view('categoria.index')->with('categorias', $categorias);
    }
    public function storeView(){
      return view('categoria.store');
    }

    public function store(Request $request){
      $categoria = new Categoria();
      $categoria->name = $request->name;
      $categoria->save();
      $request->session()->flash('titulo', 'Éxito');
      $request->session()->flash('mensaje', 'Categoria creada correctamente.');
      $request->session()->flash('tipo', 'success');
      return redirect()->back();
    }

    public function delete(Request $request){
      $categoria = Categoria::find($request->identificador);
      $categoria->delete();
      $request->session()->flash('titulo', 'Éxito');
      $request->session()->flash('mensaje', 'Categoria eliminada correctamente.');
      $request->session()->flash('tipo', 'success');
      return redirect()->back();  
    }

    public function update(Request $request){
      $categoria = Categoria::find($request->identificador);
      $categoria->name = $request->name;
      $categoria->save();
      $request->session()->flash('titulo', 'Éxito');
      $request->session()->flash('mensaje', 'Categoria actualizada correctamente.');
      $request->session()->flash('tipo', 'success');
      return redirect()->back();
    }

    public function editOne($identificador){
         $categoria = Categoria::find($identificador);
         return view('categoria.edit')->with('categoria', $categoria);
      }
}
