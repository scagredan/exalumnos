<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;
use Auth;

class EmpresasController extends Controller
{
 public function index(){
  if (Auth::user()->roll == 1) {
    $empresas = Empresa::orderBy('created_at','asc')->get();
    return view('user.empresa.index')->with('empresas', $empresas);
  }
  $empresas = Empresa::all();
  return view('empresa.index')->with('empresas', $empresas);
}

public function storeView(){
 return view('empresa.store');
}


public function store(Request $request){
  $empresa = new Empresa();
  $empresa->name = $request->name;
  if (Auth::user()->roll == 1) {
    $empresa->user_id = Auth::user()->id;
  }else{
    $empresa->user_id = $request->user_id;
  }
  $empresa->descripcion = $request->descripcion;
  $empresa->telefono = $request->telefono;
  $empresa->celular = $request->celular;
  $empresa->nit = $request->nit;
  $empresa->direccion = $request->direccion;
  $empresa->tipo_empresa = $request->tipo_empresa;
  $empresa->correo = $request->correo;
  $empresa->categoria = $request->categoria;
  $empresa->active = $request->activo;
  $empresa->save();
  $request->session()->flash('titulo', 'Éxito');
  $request->session()->flash('mensaje', 'Empresa creada correctamente.');
  $request->session()->flash('tipo', 'success');
  return redirect()->back();
}

public function delete(Request $request){
  $empresa = Empresa::find($request->identificador);
  if (Auth::user()->roll == 1) {
    if (!$empresa->user_id == Auth::user()->id) {
      $request->session()->flash('titulo', 'Error');
      $request->session()->flash('mensaje', 'Tu no puedes eliminar esta empresa.');
      $request->session()->flash('tipo', 'error');
      return redirect()->back();
    }
  }
  $empresa->delete();
  $request->session()->flash('titulo', 'Éxito');
  $request->session()->flash('mensaje', 'Empresa eliminada correctamente.');
  $request->session()->flash('tipo', 'success');
  return redirect()->back();	
}

public function update(Request $request){
  $empresa = Empresa::find($request->identificador);
  $empresa->name = $request->name;
  $empresa->user_id = $request->user_id;
  $empresa->descripcion = $request->descripcion;
  $empresa->telefono = $request->telefono;
  $empresa->celular = $request->celular;
  $empresa->nit = $request->nit;
  $empresa->direccion = $request->direccion;
  $empresa->tipo_empresa = $request->tipo_empresa;
  $empresa->correo = $request->correo;
  $empresa->categoria = $request->categoria;
  $empresa->active = $request->activo;
  $empresa->save();
  $request->session()->flash('titulo', 'Éxito');
  $request->session()->flash('mensaje', 'Empresa editada correctamente.');
  $request->session()->flash('tipo', 'success');
  return redirect()->back();
}
public function editOne($identificador){
 $empresa = Empresa::find($identificador);
 return view('empresa.edit')->with('empresa', $empresa);
}
}
