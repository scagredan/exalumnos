<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hobbie;
class HobbieController extends Controller
{
    public function index(){
        $hobbies = Hobbie::all();
        return view('hobbie.index')->with('hobbies', $hobbies);
      }
      public function storeView(){
        return view('hobbie.store');
      }
  
      public function store(Request $request){
        $hobbie = new Hobbie();
        $hobbie->name = $request->name;
        $hobbie->save();
        $request->session()->flash('titulo', 'Éxito');
        $request->session()->flash('mensaje', 'Hobbie creado correctamente.');
        $request->session()->flash('tipo', 'success');
        return redirect()->back();
      }
  
      public function delete(Request $request){
        $hobbie = Hobbie::find($request->identificador);
        $hobbie->delete();
        $request->session()->flash('titulo', 'Éxito');
        $request->session()->flash('mensaje', 'Hobbie eliminado correctamente.');
        $request->session()->flash('tipo', 'success');
        return redirect()->back();  
      }
  
      public function update(Request $request){
        $hobbie = Hobbie::find($request->identificador);
        $hobbie->name = $request->name;
        $hobbie->save();
        $request->session()->flash('titulo', 'Éxito');
        $request->session()->flash('mensaje', 'Hobbie actualizado correctamente.');
        $request->session()->flash('tipo', 'success');
        return redirect()->back();
      }
  
      public function editOne($identificador){
           $hobbie = Hobbie::find($identificador);
           return view('hobbie.edit')->with('hobbie', $hobbie);
        }
}
