<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\user;
use App\Oferta;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->roll != 0) {
            $usuario = Auth::user();
            $companeros = User::where('graduacion','=',$usuario->graduacion)->count();
            $colegas = User::where('ocupacion_id','=',$usuario->ocupacion_id)->count();
            if ($colegas>6000) {
                $colegas= 0;
            }
            $ofertas = Oferta::orderBy('created_at', 'asc')->get();
            $misofertas = Oferta::where('user_id', '=', Auth::user()->id)->orderBy('created_at','=', 'asc')->get();
            return view('normaldash.index')
            ->with('usuario',$usuario)
            ->with('colegas', $colegas)
            ->with('companeros',$companeros)
            ->with('misofertas',$misofertas)
            ->with('ofertas', $ofertas);
        }
        return view('home');
    }
}
