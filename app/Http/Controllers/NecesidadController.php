<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Necesidad;

class NecesidadController extends Controller
{
    public function index(){
    	$necesidads = Necesidad::all();
    	return view('necesidad.index')->with('necesidads', $necesidads);
   	}

   	public function store(Request $request){
   		$necesidad = new Necesidad();
   		$necesidad->name = $request->name;
   		$necesidad->descripcion = $request->descripcion;
   		$necesidad->ocupacion_id = $request->ocupacion_id;
   		$necesidad->user_id = $request->user_id;
   		$necesidad->save();
   		return redirect()->back()->with('Éxito, se ha creado la necesidad.');
   	}

   	public function delete(Request $request){
   		$necesidad = Necesidad::find($request->identificador);
   		$necesidad->delete();
   		return redirect()->back()->with('Éxito, se ha elminado la necesidad.');	
   	}

   	public function update(){
   		$necesidad = Necesidad::find($request->identificador);
   		$necesidad = $request->name;
   		$necesidad->descripcion = $request->descripcion;
   		$necesidad->ocupacion_id = $request->ocupacion_id;
   		$necesidad->user_id = $request->user_id;
   		$necesidad->save();
   		return redirect()->back()->with('Éxito, se ha actualizado la necesidad correctamente');
   	}
}
