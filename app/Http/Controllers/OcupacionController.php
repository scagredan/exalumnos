<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ocupacion;

class OcupacionController extends Controller
{
      public function index(){
      $ocupacions = Ocupacion::all();
      return view('ocupacion.index')->with('ocupacions', $ocupacions);
    }
    public function storeView(){
      return view('ocupacion.store');
    }

    public function store(Request $request){
      $ocupacion = new Ocupacion();
      $ocupacion->name = $request->name;
      $ocupacion->save();
      $request->session()->flash('titulo', 'Éxito');
      $request->session()->flash('mensaje', 'Ocupacion creada correctamente.');
      $request->session()->flash('tipo', 'success');
      return redirect()->back();
    }

    public function delete(Request $request){
      $ocupacion = Ocupacion::find($request->identificador);
      $ocupacion->delete();
      $request->session()->flash('titulo', 'Éxito');
      $request->session()->flash('mensaje', 'Ocupacion eliminada correctamente.');
      $request->session()->flash('tipo', 'success');
      return redirect()->back();  
    }

    public function update(Request $request){
      $ocupacion = Ocupacion::find($request->identificador);
      $ocupacion->name = $request->name;
      $ocupacion->save();
      $request->session()->flash('titulo', 'Éxito');
      $request->session()->flash('mensaje', 'Ocupacion actualizada correctamente.');
      $request->session()->flash('tipo', 'success');
      return redirect()->back();
    }

    public function editOne($identificador){
         $ocupacion = Ocupacion::find($identificador);
         return view('ocupacion.edit')->with('ocupacion', $ocupacion);
      }
}
