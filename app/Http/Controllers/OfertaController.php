<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\NuevaOferta;
use App\Oferta;
use App\User;

class OfertaController extends Controller
{
    public function index(){
    	$ofertas = Oferta::all();
    	return view('oferta.index')->with('ofertas', $ofertas);
   	}

    public function storeView(){
      return view('oferta.store');
    }

   	public function store(Request $request){
   		$oferta = new Oferta();
   		$oferta->name = $request->name;
      $oferta->descripcion = $request->descripcion;
      $oferta->ocupacion_id = $request->ocupacion_id;
      $oferta->user_id = $request->user_id;
      $oferta->active = $request->activo;
   		$oferta->save();
      $request->session()->flash('titulo', 'Éxito');
      $request->session()->flash('mensaje', 'Oferta creada correctamente.');
      $request->session()->flash('tipo', 'success');

      try{
        Mail::to(User::where('email', 'LIKE', '%@%.%')->get())->send(new NuevaOferta($oferta));
      }catch(Exception $e){
        
      }
   		return redirect()->back();
   	}

   	public function delete(Request $request){
   		$oferta = Oferta::find($request->identificador);
   		$oferta->delete();
      $request->session()->flash('titulo', 'Éxito');
      $request->session()->flash('mensaje', 'Oferta eliminada correctamente.');
      $request->session()->flash('tipo', 'success');
   		return redirect()->back();	
   	}

   	public function update(Request $request){
      $oferta = Oferta::find($request->identificador);
   		$oferta->name = $request->name;
      $oferta->descripcion = $request->descripcion;
      $oferta->ocupacion_id = $request->ocupacion_id;
      $oferta->user_id = $request->user_id;
      $oferta->active = $request->activo;
   		$oferta->save();
      $request->session()->flash('titulo', 'Éxito');
      $request->session()->flash('mensaje', 'Oferta editada correctamente.');
      $request->session()->flash('tipo', 'success');
   		return redirect()->back();
   	}
    public function editOne($identificador){
         $oferta = Oferta::find($identificador);
         return view('oferta.edit')->with('oferta', $oferta);
      }
}
