<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Universidad;

class UniversidadController extends Controller
{
    public function index(){
    	$universidads = Universidad::all();
    	return view('universidad.index')->with('universidads', $universidads);
   	}
    public function storeView(){
      return view('universidad.store');
    }

   	public function store(Request $request){
   		$universidad = new Universidad();
   		$universidad->name = $request->name;
   		$universidad->save();
      $request->session()->flash('titulo', 'Éxito');
      $request->session()->flash('mensaje', 'Universidad creada correctamente.');
      $request->session()->flash('tipo', 'success');
   		return redirect()->back();
   	}

   	public function delete(Request $request){
   		$universidad = Universidad::find($request->identificador);
   		$universidad->delete();
      $request->session()->flash('titulo', 'Éxito');
      $request->session()->flash('mensaje', 'Universidad eliminada correctamente.');
      $request->session()->flash('tipo', 'success');
   		return redirect()->back();	
   	}

   	public function update(Request $request){
   		$universidad = Universidad::find($request->identificador);
   		$universidad->name = $request->name;
   		$universidad->save();
      $request->session()->flash('titulo', 'Éxito');
      $request->session()->flash('mensaje', 'Universidad actualizada correctamente.');
      $request->session()->flash('tipo', 'success');
   		return redirect()->back();
   	}

    public function editOne($identificador){
         $universidad = Universidad::find($identificador);
         return view('universidad.edit')->with('universidad', $universidad);
      }
}
