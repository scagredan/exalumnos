<?php

namespace App\Http\Controllers;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use App\User;
use App\Universidad;
use App\Ocupacion;
use App\Carrera;
use App\Hobbie;
use App\CampoPrivado;

class UsuariosController extends Controller
{
  public function filter()
  {
    return view('usuarios.filter')->with('universidades', Universidad::all())
                                  ->with('ocupaciones', Ocupacion::all())
                                  ->with('carreras', Carrera::all())
                                  ->with('hobbies', Hobbie::all())
                                  ->with('estados_civiles', User::select('estado_civil')->groupBy('estado_civil')->get())
                                  ->with('anio_graduacion', User::select('graduacion')->groupBy('graduacion')->get());
  }

 public function index(Request $request){
  $usuarios = User::where('id', '!=', 1);
  if($request->genero)
    $usuarios->where('genero', $request->genero);
  if($request->universidad)
    $usuarios->where('universidad_id', $request->universidad);
  if($request->ocupacion)
    $usuarios->where('ocupacion_id', $request->ocupacion);
  if($request->carrera)
    $usuarios->where('carrera_id', $request->carrera);
  if($request->anio_graduacion)
    $usuarios->where('graduacion', $request->anio_graduacion);
  if($request->estado_civil)
    $usuarios->where('estado_civil', $request->estado_civil);
  if($request->hobbie)
    $usuarios->where('hobbie_id', $request->hobbie)->orWhere('hobbie_id_2', $request->hobbie);
  if($request->apellido)
    $usuarios->where('apellido', 'like', '%' . $request->apellido . '%');

  return view('usuarios.index')->with('usuarios', $usuarios->paginate(20));
}

public function exportarExcel(Request $request)
{
  $usuarios = User::where('id', '!=', 1);
  if($request->genero)
    $usuarios->where('genero', $request->genero);
  if($request->universidad)
    $usuarios->where('universidad_id', $request->universidad);
  if($request->ocupacion)
    $usuarios->where('ocupacion_id', $request->ocupacion);
  if($request->carrera)
    $usuarios->where('carrera_id', $request->carrera);
  if($request->anio_graduacion)
    $usuarios->where('graduacion', $request->anio_graduacion);
  if($request->estado_civil)
    $usuarios->where('estado_civil', $request->estado_civil);
  if($request->hobbie)
    $usuarios->where('hobbie_id', $request->hobbie)->orWhere('hobbie_id_2', $request->hobbie);
  if($request->apellido)
    $usuarios->where('apellido', 'like', '%' . $request->apellido . '%');

  Excel::create('Excel', function($excel) use($usuarios) {
 
    $excel->sheet('Usuarios', function($sheet) use($usuarios)  {
      $sheet->appendRow(array(
          'Nombre', 'Apellido', 'Documento', 'Año de graduación', 'Género', 'Cumpleaños', 'Pais de nacimiento', 'Ciudad', 'Celular', 'Teléfono', 'Dirección', 
          'Profesión', 'Universidad', 'Carrera', 'Año de graduación', 'Especialización', 'Año de graduación', 'Otro curso', 'Año de graduación',
          'Ocupación','Hobbie','Compañia','Teléfono','Estado Civil','Hijos','Hijos estudiando','Hijos Graduados',
      ));
      $usuarios->chunk(500, function($modelInstance) use($sheet) {
        // dd($modelInstance);

        foreach ($modelInstance as $user)
        {
            if($user->universidad){
              $user->universidadT = $user->universidad->name;
            }else{
              $user->universidadT = "";
            }
            if($user->carrera){
              $user->carreraT = $user->carrera->name;
            }else{
              $user->carreraT = "";
            }
            if($user->ocupacion){
              $user->ocupacionT = $user->ocupacion->name;
            }else{
              $user->ocupacionT = "";
            }
            if($user->hobbie){
              $user->hobbieT = $user->hobbie->name;
            }else{
              $user->hobbieT = "";
            }
            $sheet->appendRow(array(
                $user->name, 
                $user->apellido,
                $user->numero_documento,
                $user->graduacion,
                $user->genero,
                $user->cumpleanos,
                $user->pais_nacimiento,
                $user->ciudad,
                $user->celular,
                $user->telefono,
                $user->direccion,
                $user->profesion_label,
                $user->universidadT,
                $user->carreraT,
                $user->ano_graduacion,
                $user->especializacion,
                $user->ano_graduacion_especializacion,
                $user->otro_curso,
                $user->graduacion_otro_curso,
                $user->ocupacionT,
                $user->hobbieT,
                $user->nombre_compania,
                $user->telefono_compania,
                $user->estado_civil,
                $user->hijos,
                $user->hijos_estudiando,
                $user->hijos_graduados,
            ));
        }
      });
        // foreach (User::all() as $user)
        // {
        //     if($user->universidad){
        //       $user->universidadT = $user->universidad->name;
        //     }else{
        //       $user->universidadT = "";
        //     }
        //     if($user->carrera){
        //       $user->carreraT = $user->carrera->name;
        //     }else{
        //       $user->carreraT = "";
        //     }
        //     if($user->ocupacion){
        //       $user->ocupacionT = $user->ocupacion->name;
        //     }else{
        //       $user->ocupacionT = "";
        //     }
        //     if($user->hobbie){
        //       $user->hobbieT = $user->hobbie->name;
        //     }else{
        //       $user->hobbieT = "";
        //     }
        //     $sheet->appendRow(array(
        //         $user->name, 
        //         $user->apellido,
        //         $user->numero_documento,
        //         $user->graduacion,
        //         $user->genero,
        //         $user->cumpleanos,
        //         $user->pais_nacimiento,
        //         $user->ciudad,
        //         $user->celular,
        //         $user->telefono,
        //         $user->direccion,
        //         $user->profesion_label,
        //         $user->universidadT,
        //         $user->carreraT,
        //         $user->ano_graduacion,
        //         $user->especializacion,
        //         $user->ano_graduacion_especializacion,
        //         $user->otro_curso,
        //         $user->graduacion_otro_curso,
        //         $user->ocupacionT,
        //         $user->hobbieT,
        //         $user->nombre_compania,
        //         $user->telefono_compania,
        //         $user->estado_civil,
        //         $user->hijos,
        //         $user->hijos_estudiando,
        //         $user->hijos_graduados,
        //     ));
        // }
    });
  })->export('xls');
}

public function storeView(){
   return view('usuarios.store');
}

public function store(Request $request){
  $usuario = new User();
  $usuario->name 				= $request->name;
  $usuario->numero_documento 			= $request->numero_documento;
  $usuario->email 			= $request->correo;
  $usuario->hobbie_id 			= $request->hobbie_id;
  $usuario->hobbie_id_2			= $request->hobbie_id_2;
  $usuario->password 			= bcrypt($request->password);
  $usuario->apellido 			= $request->apellido;
  $usuario->graduacion		= $request->graduacion;
  $usuario->genero			= $request->genero;
  $usuario->cumpleanos		= $request->cumpleanos;
  $usuario->pais_nacimiento	= $request->pais_nacimiento;
  $usuario->ciudad			= $request->ciudad;
  $usuario->celular			= $request->celular;
  $usuario->telefono			= $request->telefono;
  $usuario->direccion			= $request->direccion;
  $usuario->universidad_id 	= $request->universidad_id;
  $usuario->ano_graduacion	= $request->ano_graduacion;
  $usuario->especializacion	= $request->especializacion;
  $usuario->especializacion_id 		= $request->especializacion_id;
  $usuario->ano_graduacion_especializacion	= $request->ano_graduacion_especializacion;
  $usuario->otro_curso		= $request->otro_curso;
  $usuario->graduacion_otro_curso	= $request->graduacion_otro_curso;
  $usuario->ocupacion_id		= $request->ocupacion_id;
  $usuario->nombre_compania	= $request->nombre_compania;
  $usuario->telefono_compania	= $request->telefono_compania;
  $usuario->estado_civil		= $request->estado_civil;
  $usuario->hijos				= $request->hijos;
  $usuario->hijos_estudiando	= $request->hijos_estudiando;
  $usuario->hijos_graduados	= $request->hijos_graduados;
  $usuario->save();

  $request->session()->flash('titulo', 'Éxito');
  $request->session()->flash('mensaje', 'Usuario creado correctamente.');
  $request->session()->flash('tipo', 'success');
  return back();
}

public function delete(Request $request){
  $usuario = User::find($request->identificador);
  $usuario->delete();
  $request->session()->flash('titulo', 'Éxito');
  $request->session()->flash('mensaje', 'Usuario eliminado correctamente.');
  $request->session()->flash('tipo', 'success');
  return back();
}

public function update(Request $request){
  $usuario = User::find($request->identificador);
  if($request->name)
    $usuario->name 				= $request->name;
  $usuario->email 			= $request->correo;
  $usuario->hobbie_id 			= $request->hobbie_id;
  $usuario->hobbie_id_2			= $request->hobbie_id_2;
  $usuario->numero_documento 			= $request->numero_documento;
  $usuario->apellido 			= $request->apellido;
  $usuario->graduacion		= $request->graduacion;
  $usuario->genero			= $request->genero;
  $usuario->cumpleanos		= $request->cumpleanos;
  $usuario->pais_nacimiento	= $request->pais_nacimiento;
  $usuario->ciudad			= $request->ciudad;
  $usuario->celular			= $request->celular;
  $usuario->telefono			= $request->telefono;
  $usuario->direccion			= $request->direccion;
  $usuario->universidad_id 	= $request->universidad_id;
  $usuario->ano_graduacion	= $request->ano_graduacion;
  $usuario->especializacion	= $request->especializacion;
  $usuario->especializacion_id 		= $request->especializacion_id;
  $usuario->ano_graduacion_especializacion	= $request->ano_graduacion_especializacion;
  $usuario->otro_curso		= $request->otro_curso;
  $usuario->graduacion_otro_curso	= $request->graduacion_otro_curso;
  $usuario->ocupacion_id		= $request->ocupacion_id;
  $usuario->nombre_compania	= $request->nombre_compania;
  $usuario->telefono_compania	= $request->telefono_compania;
  $usuario->estado_civil		= $request->estado_civil;
  $usuario->hijos				= $request->hijos;
  $usuario->hijos_estudiando	= $request->hijos_estudiando;
  $usuario->hijos_graduados	= $request->hijos_graduados;
  $usuario->save();
if(CampoPrivado::where('user_id', $request->identificador)->count() > 0){
  $campos = CampoPrivado::where('user_id', $request->identificador)->first();
}else{
  $campos = new CampoPrivado;
}
  $campos->user_id = $request->identificador;
  $campos->numero_documento = $request->privado_numero_documento;
  $campos->graduacion = $request->privado_graduacion;

  $campos->genero = $request->privado_genero;

  $campos->cumpleanos = $request->privado_cumpleanos;

  $campos->pais_nacimiento = $request->privado_pais_nacimiento;

  $campos->ciudad = $request->privado_numero_documento;

  $campos->celular = $request->privado_ciudad;

  $campos->telefono = $request->privado_telefono;

  $campos->direccion = $request->privado_direccion;

  $campos->profesion_label = $request->privado_profesion_label;
  $campos->universidad_id = $request->privado_universidad_id;
  $campos->carrera_id = $request->privado_carrera_id;
  $campos->ano_graduacion = $request->privado_ano_graduacion;
  $campos->especializacion = $request->privado_especializacion;
  $campos->especializacion_id = $request->privado_especializacion_id;
  $campos->ano_graduacion_especializacion = $request->privado_ano_graduacion_especializacion;
  $campos->otro_curso = $request->privado_otro_curso;
  $campos->graduacion_otro_curso = $request->privado_graduacion_otro_curso;
  $campos->ocupacion_id = $request->privado_ocupacion_id;
  $campos->nombre_compania = $request->privado_nombre_compania;
  $campos->telefono_compania = $request->privado_telefono_compania;
  $campos->estado_civil = $request->privado_estado_civil;
  $campos->hijos = $request->privado_hijos;
  $campos->hijos_estudiando = $request->privado_hijos_estudiando;
  $campos->hijos_graduados = $request->privado_hijos_graduados;

  $campos->save();

  $request->session()->flash('titulo', 'Éxito');
  $request->session()->flash('mensaje', 'Usuario actualizado correctamente.');
  $request->session()->flash('tipo', 'success');
  return redirect()->back();
}

public function completar(Request $request){
  $usuario = User::find($request->identificador);
  if($request->name)
    $usuario->name 				= $request->name;
  $usuario->email 			= $request->correo;
  $usuario->hobbie_id 			= $request->hobbie_id;
  $usuario->hobbie_id_2			= $request->hobbie_id_2;
  $usuario->numero_documento 			= $request->numero_documento;
  $usuario->graduacion		= $request->graduacion;
  $usuario->genero			= $request->genero;
  $usuario->cumpleanos		= $request->cumpleanos;
  $usuario->pais_nacimiento	= $request->pais_nacimiento;
  $usuario->ciudad			= $request->ciudad;
  $usuario->celular			= $request->celular;
  $usuario->telefono			= $request->telefono;
  $usuario->direccion			= $request->direccion;
  $usuario->universidad_id 	= $request->universidad_id;
  $usuario->ano_graduacion	= $request->ano_graduacion;
  $usuario->especializacion	= $request->especializacion;
  $usuario->especializacion_id 		= $request->especializacion_id;
  $usuario->ano_graduacion_especializacion	= $request->ano_graduacion_especializacion;
  $usuario->otro_curso		= $request->otro_curso;
  $usuario->graduacion_otro_curso	= $request->graduacion_otro_curso;
  $usuario->ocupacion_id		= $request->ocupacion_id;
  $usuario->nombre_compania	= $request->nombre_compania;
  $usuario->telefono_compania	= $request->telefono_compania;
  $usuario->estado_civil		= $request->estado_civil;
  $usuario->hijos				= $request->hijos;
  $usuario->hijos_estudiando	= $request->hijos_estudiando;
  $usuario->hijos_graduados	= $request->hijos_graduados;
  $usuario->save();
if(CampoPrivado::where('user_id', $request->identificador)->count() > 0){
  $campos = CampoPrivado::where('user_id', $request->identificador)->first();
}else{
  $campos = new CampoPrivado;
}
  $campos->user_id = $request->identificador;
  $campos->numero_documento = $request->privado_numero_documento;
  $campos->graduacion = $request->privado_graduacion;

  $campos->genero = $request->privado_genero;

  $campos->cumpleanos = $request->privado_cumpleanos;

  $campos->pais_nacimiento = $request->privado_pais_nacimiento;

  $campos->ciudad = $request->privado_numero_documento;

  $campos->celular = $request->privado_ciudad;

  $campos->telefono = $request->privado_telefono;

  $campos->direccion = $request->privado_direccion;

  $campos->profesion_label = $request->privado_profesion_label;
  $campos->universidad_id = $request->privado_universidad_id;
  $campos->carrera_id = $request->privado_carrera_id;
  $campos->ano_graduacion = $request->privado_ano_graduacion;
  $campos->especializacion = $request->privado_especializacion;
  $campos->especializacion_id = $request->privado_especializacion_id;
  $campos->ano_graduacion_especializacion = $request->privado_ano_graduacion_especializacion;
  $campos->otro_curso = $request->privado_otro_curso;
  $campos->graduacion_otro_curso = $request->privado_graduacion_otro_curso;
  $campos->ocupacion_id = $request->privado_ocupacion_id;
  $campos->nombre_compania = $request->privado_nombre_compania;
  $campos->telefono_compania = $request->privado_telefono_compania;
  $campos->estado_civil = $request->privado_estado_civil;
  $campos->hijos = $request->privado_hijos;
  $campos->hijos_estudiando = $request->privado_hijos_estudiando;
  $campos->hijos_graduados = $request->privado_hijos_graduados;

  $campos->save();

  $request->session()->flash('titulo', 'Éxito');
  $request->session()->flash('mensaje', 'Usuario actualizado correctamente.');
  $request->session()->flash('tipo', 'success');
  return redirect()->route('home');
}

public function view($identificador){
   $usuario = User::find($identificador);
   return view('usuarios.view')->with('usuario', $usuario);
}

public function editOne($identificador){
   $usuario = User::find($identificador);
   return view('usuarios.edit')->with('usuario', $usuario);
}

public function perfil($identificador){
   $usuario = User::find($identificador);
   return view('user.perfil')->with('usuario', $usuario);
}
public function registrar(Request $request){

   if ($request->step == '1') {
    $verificacion = User::where('graduacion','=',$request->generacion)->where('apellido','like' ,'%'.$request->apellido.'%')->where('name','like' ,'%'.$request->nombre.'%')->count();
    if (!$verificacion>0) {
      $request->session()->flash('titulo', 'Error');
        $request->session()->flash('mensaje', 'No hay exalumnos registrados, Comuniquese con el administrador.');
        $request->session()->flash('tipo', 'success');
        return redirect('register');
    }
   
      $alumno = User::where('graduacion','=',$request->generacion)->inRandomOrder()->take(1)->first();
      $alumno2 = User::where('graduacion','!=',$request->generacion)->inRandomOrder()->take(1)->first();
      $alumno3 = User::where('graduacion','!=',$request->generacion)->inRandomOrder()->take(1)->first();
      $alumno4 = User::where('graduacion','!=',$request->generacion)->inRandomOrder()->take(1)->first();

      $alumnos = [$alumno, $alumno2, $alumno3, $alumno4];
      return view('user.register')->with('alumnos',$alumnos)->with('graduacion',$request->generacion)->with('apellido', $request->apellido)->with('nombre', $request->nombre);
   }

   if ($request->step == '2') {
      if ($request->generacion == User::find($request->opcion)->graduacion) {
         $alumnos = User::where('graduacion','=',$request->generacion)->where('apellido', 'like', '%'.$request->apellido.'%')->where('name', 'like', '%'.$request->nombre.'%')->get();
         return view('user.completar')->with('alumnos', $alumnos)->with('usuario',$alumnos);
      }
      else{
        $request->session()->flash('titulo', 'Error');
        $request->session()->flash('mensaje', 'La respuesta ha sido incorrecta.');
        $request->session()->flash('tipo', 'success');
        return redirect('register');
     }
  }

}


}
