<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Oferta;

class NuevaOferta extends Mailable
{
    use Queueable, SerializesModels;

    protected $oferta;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Oferta $oferta)
    {
        $this->oferta = $oferta;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.ofertas.nueva')->with('oferta', $this->oferta);
    }
}
