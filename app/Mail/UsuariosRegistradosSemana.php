<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use Carbon\Carbon;

class UsuariosRegistradosSemana extends Mailable
{
    use Queueable, SerializesModels;

    protected $registros;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->registros = User::where('created_at', '>=', Carbon::now()->subDay(7))->get();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.usuarios.reporte_semanal')->with('registros', $this->registros);
    }
}
