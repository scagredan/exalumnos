<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Necesidad extends Model
{
    protected $table = 'necesidads';

     public function usuario(){
        return $this->hasOne('App\User', 'usuario_id');
     }

     public function ocupacion(){
     	return $this->hasOne('App\Ocupacion', 'ocupacion_id');
     }
}
