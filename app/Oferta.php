<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Oferta extends Model
{
     protected $table = 'ofertas';

     public function usuario(){
        return $this->belongsTo('App\User','user_id');
     }

     public function ocupacion(){
     	return $this->belongsTo('App\Ocupacion', 'ocupacion_id');
     }

}
