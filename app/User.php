<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Universidad;
use App\Ocupacion;
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function universidad()
    {   
       
        return $this->belongsTo('App\Universidad');
    }
    public function ocupacion()
    {   
        return $this->belongsTo('App\Ocupacion');
    }

    public function hobbie()
    {
        return $this->belongsTo('App\Hobbie');
    }

    public function hobbie2()
    {
        return $this->belongsTo('App\Hobbie','hobbie_id_2');
    }

    public function especializacion()
    {
        return $this->belongsTo('App\Universidad');
    }

    public function campos()
    {
        return $this->hasOne('App\CampoPrivado');
    }

    public function completo(){
        $completo  = 0;
        if ($this->password == "$2y$10\$jZXE8UpT1LERrPWGKara0e4JIo9kgj3I6pCGDbxN07Nv6EQcsnpoS") {
            return $completo;
        }
        if(!is_null($this->apellido)){ $completo = $completo + 6.25;}
        if(!is_null($this->graduacion)){ $completo = $completo + 6.25;}
        if(!is_null($this->genero)){ $completo = $completo + 6.25;}
        if(!is_null($this->cumpleanos)){ $completo = $completo + 6.25;}
        if(!is_null($this->pais_nacimiento)){ $completo = $completo + 6.25;}
        if(!is_null($this->ciudad)){ $completo = $completo + 6.25;}
        if(!is_null($this->celular)){ $completo = $completo + 6.25;}
        if(!is_null($this->direccion)){ $completo = $completo + 6.25;}
        if(!is_null($this->universidad_id)){ $completo = $completo + 6.25;}
        if(!is_null($this->ocupacion_id)){ $completo = $completo + 6.25;}
        if(!is_null($this->nombre_compania)){ $completo = $completo + 6.25;}
        if(!is_null($this->telefono_compania)){ $completo = $completo + 6.25;}
        if(!is_null($this->estado_civil)){ $completo = $completo + 6.25;}
        if(!is_null($this->hijos)){ $completo = $completo + 6.25;}
        if(!is_null($this->hijos_estudiando)){ $completo = $completo + 6.25;}
        if(!is_null($this->email)){ $completo = $completo + 6.25;}
        return $completo;
    }


}
