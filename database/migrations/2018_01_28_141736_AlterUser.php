<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('apellido')->nullable();
            $table->integer('graduacion');
            $table->string('genero')->nullable();
            $table->date('cumpleanos')->nullable();
            $table->string('pais_nacimiento')->nullable();
            $table->string('ciudad')->nullable();
            $table->string('celular')->nullable();
            $table->string('telefono')->nullable();
            $table->string('direccion')->nullable();
            $table->string('profesion_label')->nullable();
            $table->integer('universidad_id')->nullable()->unsigned();
            $table->integer('carrera_id')->nullable()->unsigned();
            $table->string('ano_graduacion')->nullable();
            $table->string('especializacion')->nullable();
            $table->integer('especializacion_id')->nullable()->unsigned();
            $table->string('ano_graduacion_especializacion')->nullable();
            $table->string('otro_curso')->nullable();
            $table->string('graduacion_otro_curso')->nullable();
            $table->integer('ocupacion_id')->nullable()->unsigned();
            $table->string('nombre_compania')->nullable();
            $table->string('telefono_compania')->nullable();
            $table->string('estado_civil')->nullable();
            $table->integer('hijos')->nullable();
            $table->integer('hijos_estudiando')->nullable();
            $table->integer('hijos_graduados')->nullable();
            $table->boolean('informacion_completa')->default(false);
            $table->integer('roll')->default(1);
            $table->string('image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
