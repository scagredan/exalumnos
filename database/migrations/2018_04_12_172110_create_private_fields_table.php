<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrivateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('private_fields', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id');
            
            $table->string('numero_documento')->nullable()->default(false);
            $table->string('graduacion')->nullable()->default(false);
            $table->string('genero')->nullable()->default(false);
            $table->string('cumpleanos')->nullable()->default(false);
            $table->string('pais_nacimiento')->nullable()->default(false);
            $table->string('ciudad')->nullable()->default(false);
            $table->string('celular')->nullable()->default(false);
            $table->string('telefono')->nullable()->default(false);
            $table->string('direccion')->nullable()->default(false);
            $table->string('profesion_label')->nullable()->default(false);
            $table->string('universidad_id')->nullable()->default(false);
            $table->string('carrera_id')->nullable()->default(false);
            $table->string('ano_graduacion')->nullable()->default(false);
            $table->string('especializacion')->nullable()->default(false);
            $table->string('especializacion_id')->nullable()->default(false);
            $table->string('ano_graduacion_especializacion')->nullable()->default(false);
            $table->string('otro_curso')->nullable()->default(false);
            $table->string('graduacion_otro_curso')->nullable()->default(false);
            $table->string('ocupacion_id')->nullable()->default(false);
            $table->string('nombre_compania')->nullable()->default(false);
            $table->string('telefono_compania')->nullable()->default(false);
            $table->string('estado_civil')->nullable()->default(false);
            $table->string('hijos')->nullable()->default(false);
            $table->string('hijos_estudiando')->nullable()->default(false);
            $table->string('hijos_graduados')->nullable()->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('private_fields');
    }
}
