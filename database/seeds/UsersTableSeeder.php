<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'administrador@admin.com',
            'password' => bcrypt('bartolina09'),
            'apellido' => 'Admin',
            'graduacion' => '2004',
            'genero' => 'M',
            'estado_civil' => 'Soltero',
            'name' => 'Admin',
            'roll' => 0,
        ]);
    }
}