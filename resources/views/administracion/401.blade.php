@extends('layouts.dashboard')
@section('title')
<title>Cosmos - Permiso Denegado</title>
@endsection
@section('content')
<div class="container-fluid">
	<!-- /row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box text-center">
				<h3 class="box-title m-b-0">Ups! No tienes permiso para esta sección. Consulta con el administrador ;)</h3>
			</div>
		</div>
	</div>
</div>
@endsection