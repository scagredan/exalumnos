@extends('layouts.dashboard')
@section('title')
<title>Cosmos - Listado de Códigos Contables</title>
@endsection
@section('css')
<link href="../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="../plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<div class="container-fluid">
	<!-- /row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Listado de Códigos Contables</h3>
				<p class="text-muted m-b-30">
					<button data-toggle="modal" data-target="#crear" class="btn btn-info waves-effect waves-light" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Agregar nuevo código</button>
				</p>
				<div class="table-responsive">
					<table id="tabla_registros" class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Nombre</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($registros as $index=>$registro)
							<tr>
								<td>{{$index+1}}</td>
								<td>{{$registro->nombre}}</td>
								<td>
									<button data-toggle="modal" title="Editar código"  type="button" data-target="#editar-{{ $registro->id }}" class="btn btn-danger btn-outline btn-circle btn-lg m-r-5"><i class="ti-pencil"></i>
									</button>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Modal Creación -->
<div id="crear" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Nuevo Código Contable</h4>
			</div>
			<div class="modal-body">
				<form role="form" method="POST" action="{{ url('/administracion/crear-codigo') }}">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="recipient-name" class="control-label">Nombre:</label>
						<input type="text" class="form-control" name="nombre" required>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-danger waves-effect waves-light">Crear</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> 
<!--Modal Edición -->
@foreach($registros as $registro)
<div id="editar-{{ $registro->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Edición Código Contable</h4>
			</div>
			<div class="modal-body">
				<form role="form" method="POST" action="{{ url('/administracion/editar-codigo') }}">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="recipient-name" class="control-label">Nombre:</label>
						<input type="text" class="form-control" name="nombre" placeholder="Nombre" value="{{ $registro->nombre }}" required>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-danger waves-effect waves-light">Editar</button>
					</div>
					<input type="hidden" name="id" value="{{ $registro->id }}">
				</form>
			</div>
		</div>
	</div>
</div>
@endforeach
@endsection
@section('scripts')
<script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script>
	$(document).ready(function() {
		$('#tabla_registros').DataTable({
			"language": {
				"url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
			}
		});
	});

</script>

@if (session('mensaje'))
<script type="text/javascript">
	swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
</script>
@endif
@endsection