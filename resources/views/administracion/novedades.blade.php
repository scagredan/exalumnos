@extends('layouts.dashboard')
@section('title')
<title>Cosmos - Listado de Novedades</title>
@endsection
@section('css')
<link href="../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.9/css/fileinput.css" rel="stylesheet" >
<link href="../plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
@endsection
@section('content')
<div class="container-fluid">
	<!-- /row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Listado Novedades</h3>
				<p class="text-muted m-b-30">
					<button data-toggle="modal" data-titulo="Novedades" data-target="#modal_fechas" class="btn btn-primary waves-effect waves-light" type="button"><span class="btn-label"><i class="fa fa-calendar"></i></span>Reporte Fechas</button>
				</p>
				<div id="div_novedades" class="table-responsive">
					<table id="tabla_registros" class="table table-striped">
						<thead>
							<tr>
								<th>Código</th>
								<th>Funcionario</th>
								<th>Cargo</th>
								<th># Identificación</th>
								<th>Tipo</th>
								<th>Sede</th>
								<th>Fecha Creación</th>
								<th>Fecha Novedad</th>
								<th># Días</th>
								<th># Horas</th>
								<th>Tipo Hora Extra</th>
								<th>Hora Inicial</th>
								<th>Horal Final</th>
								<th>Nueva Sede</th>
								<th>Fecha Retiro</th>
								<th>Nuevo Salario</th>
								<th>Nuevo Horario</th>
								<th>Año Vacaciones</th>
								<th>Días pendientes vacaciones</th>
								<th>Días compensados en dinero</th>
								<th>Fecha Inicio Vacaciones</th>
								<th>Fecha Final Vacaciones</th>
								<th>Tipo Descuento</th>
								<th>Valor Descuento</th>
								<th># Cuotas Quincenales</th>
								<th># Quincena</th>
								<th>Mes Descuento</th>
								<th>Año Descuento</th>
								<th>Concepto Descuento</th>
								<th>Observaciones</th>
								<th>Entrega Soportes</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($novedades as $index=>$novedad)
							<tr>
								<td><b>{{$novedad->codigo}}</b></td>
								<td>{{$novedad->funcionario->obtenerNombreCompleto()}}</td>
								@if(isset($novedad->funcionario->cargo->name))
								<td>{{$novedad->funcionario->cargo->name}}</td>
								@else
								<td></td>
								@endif
								<td>{{$novedad->funcionario->numero_documento}}</td>
								<td><b>{{$novedad->tipoNovedad->name}}</b></td>
								<td>{{$novedad->funcionario->obtenerSede()}}</td>
								<td>{{$novedad->created_at}}</td>
								<td>{{$novedad->fecha_novedad}}</td>
								<td>{{$novedad->numero_dias}}</td>
								<td>{{$novedad->numero_horas}}</td>
								@if(empty($novedad->tipoHoraExtra))
								<td>{{$novedad->tipo_hora_extra_id}}</td>
								@else
								<td>{{$novedad->tipoHoraExtra->name}}</td>
								@endif
								<td>{{$novedad->hora_inicial}}</td>
								<td>{{$novedad->hora_final}}</td>
								@if(empty($novedad->nueva_sede))
								<td>{{$novedad->nueva_sede}}</td>
								@else
								<td>{{$novedad->obtenerSede()}}</td>
								@endif
								<td>{{$novedad->fecha_retiro}}</td>
								<td>{{$novedad->nuevo_salario}}</td>
								<td>{{$novedad->nuevo_horario}}</td>
								<td>{{$novedad->anio_vacaciones}}</td>
								<td>{{$novedad->dias_pendientes_vacaciones}}</td>
								<td>{{$novedad->dias_compensados}}</td>
								<td>{{$novedad->fecha_inicio_vacaciones}}</td>
								<td>{{$novedad->fecha_final_vacaciones}}</td>
								@if(empty($novedad->tipoDescuento))
								<td>{{$novedad->tipo_descuento_id}}</td>
								@else
								<td>{{$novedad->tipoDescuento->name}}</td>
								@endif
								<td>{{$novedad->valor_descuento}}</td>
								<td>{{$novedad->numero_cuotas_quincenales}}</td>
								<td>{{$novedad->numero_quincena}}</td>
								<td>{{$novedad->mes_descuento}}</td>
								<td>{{$novedad->anio_descuento}}</td>
								<td>{{$novedad->concepto_descuento}}</td>
								<td>{{$novedad->observaciones}}</td>
								<td>{{$novedad->entrega_soportes}}</td>
								<td>
									@if(Auth::user()->tipoUsuario->permisos->novedades_ver)
									<button data-toggle="modal" title="Ver Novedad" type="button" data-target="#ver-{{ $novedad->id }}" class="btn btn-danger btn-outline btn-circle btn-md m-r-5"><i class="ti-eye"></i>
									</button>
									@endif
									@if(Auth::user()->tipoUsuario->permisos->novedades_adjuntar)
									<button data-toggle="modal" title="Agregar Archivos" type="button" data-target="#agregar-archivos-{{ $novedad->id }}" class="btn btn-danger btn-outline btn-circle btn-md m-r-5"><i class="ti-cloud-up"></i>
									</button>
									@endif
									@if(Auth::user()->tipoUsuario->permisos->novedades_editar)
									<button data-toggle="modal" title="Editar Novedad" type="button" data-target="#editar-{{ $novedad->id }}" class="btn btn-danger btn-outline btn-circle btn-md m-r-5"><i class="ti-pencil"></i></button>
									@endif
									@if(Auth::user()->tipoUsuario->permisos->novedades_verArchivos)
									@if($novedad->archivos->count() > 0)
									<button data-toggle="modal" title="Listado Archivos" type="button" data-target="#archivos-{{ $novedad->id }}" class="btn btn-danger btn-outline btn-circle btn-md m-r-5"><i class="ti-files"></i>
									</button>
									@endif
									@endif
								</td>						
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Modal Ver Novedad -->
@foreach($novedades as $novedad)
<div id="ver-{{ $novedad->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Detalle Novedad <b>{{$novedad->codigo}} - {{$novedad->funcionario->obtenerNombreCompleto()}}</b></h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal">
					@if($novedad->fecha_novedad != '')
					<div class="form-group">
						<label class="col-sm-3">Fecha Novedad:</label>
						<div class="col-sm-9">
							{{$novedad->fecha_novedad}}
						</div>
					</div>
					@endif
					@if($novedad->numero_dias != '')
					<div class="form-group">
						<label class="col-sm-3">Número Días:</label>
						<div class="col-sm-9">
							{{$novedad->numero_dias}}
						</div>
					</div>
					@endif
					@if($novedad->numero_horas != '')
					<div class="form-group">
						<label class="col-sm-3">Número Horas:</label>
						<div class="col-sm-9">
							{{$novedad->numero_horas}}
						</div>
					</div>
					@endif
					@if($novedad->tipo_hora_extra_id != '')
					<div class="form-group">
						<label class="col-sm-3">Tipo Hora Extra:</label>
						<div class="col-sm-9">
							{{$novedad->tipoHoraExtra->name}}
						</div>
					</div>
					@endif
					@if($novedad->hora_inicial != '')
					<div class="form-group">
						<label class="col-sm-3">Hora Inicial:</label>
						<div class="col-sm-9">
							{{$novedad->hora_inicial}}
						</div>
					</div>
					@endif
					@if($novedad->hora_final != '')
					<div class="form-group">
						<label class="col-sm-3">Hora Final:</label>
						<div class="col-sm-9">
							{{$novedad->hora_final}}
						</div>
					</div>
					@endif
					@if($novedad->nueva_sede != '')
					<div class="form-group">
						<label class="col-sm-3">Nueva Sede:</label>
						<div class="col-sm-9">
							{{$novedad->obtenerSede()}}
						</div>
					</div>
					@endif
					@if($novedad->fecha_retiro != '')
					<div class="form-group">
						<label class="col-sm-3">Fecha Retiro:</label>
						<div class="col-sm-9">
							{{$novedad->fecha_retiro}}
						</div>
					</div>
					@endif
					@if($novedad->nuevo_salario != '')
					<div class="form-group">
						<label class="col-sm-3">Nuevo Salario:</label>
						<div class="col-sm-9">
							{{$novedad->nuevo_salario}}
						</div>
					</div>
					@endif
					@if($novedad->nuevo_horario != '')
					<div class="form-group">
						<label class="col-sm-3">Nuevo Horario:</label>
						<div class="col-sm-9">
							{{$novedad->nuevo_horario}}
						</div>
					</div>
					@endif
					@if($novedad->anio_vacaciones != '')
					<div class="form-group">
						<label class="col-sm-3">Año Vacaciones:</label>
						<div class="col-sm-9">
							{{$novedad->anio_vacaciones}}
						</div>
					</div>
					@endif
					@if($novedad->dias_pendientes_vacaciones != '')
					<div class="form-group">
						<label class="col-sm-3"># Días Pendientes:</label>
						<div class="col-sm-9">
							{{$novedad->dias_pendientes_vacaciones}}
						</div>
					</div>
					@endif
					@if($novedad->dias_compensados != '')
					<div class="form-group">
						<label class="col-sm-3">¿Días compensados en dinero?:</label>
						<div class="col-sm-9">
							{{$novedad->dias_compensados}}
						</div>
					</div>
					@endif
					@if($novedad->fecha_inicio_vacaciones != '')
					<div class="form-group">
						<label class="col-sm-3">Fecha Inicial:</label>
						<div class="col-sm-9">
							{{$novedad->fecha_inicio_vacaciones}}
						</div>
					</div>
					@endif
					@if($novedad->fecha_final_vacaciones != '')
					<div class="form-group">
						<label class="col-sm-3">Fecha Final:</label>
						<div class="col-sm-9">
							{{$novedad->fecha_final_vacaciones}}
						</div>
					</div>
					@endif
					@if($novedad->tipo_descuento_id != '')
					<div class="form-group">
						<label class="col-sm-3">Tipo Descuento:</label>
						<div class="col-sm-9">
							{{$novedad->tipoDescuento->name}}
						</div>
					</div>
					@endif
					@if($novedad->valor_descuento != '')
					<div class="form-group">
						<label class="col-sm-3">Valor Descuento:</label>
						<div class="col-sm-9">
							{{$novedad->obtenerValorDescuento()}}
						</div>
					</div>
					@endif
					@if($novedad->numero_cuotas_quincenales != '')
					<div class="form-group">
						<label class="col-sm-3"># Cuentas Quincenales:</label>
						<div class="col-sm-9">
							{{$novedad->numero_cuotas_quincenales}}
						</div>
					</div>
					@endif
					@if($novedad->numero_quincena != '')
					<div class="form-group">
						<label class="col-sm-3"># Quincenas:</label>
						<div class="col-sm-9">
							{{$novedad->numero_quincena}}
						</div>
					</div>
					@endif
					@if($novedad->mes_descuento != '')
					<div class="form-group">
						<label class="col-sm-3">Mes Descuento:</label>
						<div class="col-sm-9">
							{{$novedad->mes_descuento}}
						</div>
					</div>
					@endif
					@if($novedad->anio_descuento != '')
					<div class="form-group">
						<label class="col-sm-3">Año Descuento:</label>
						<div class="col-sm-9">
							{{$novedad->anio_descuento}}
						</div>
					</div>
					@endif
					@if($novedad->concepto_descuento != '')
					<div class="form-group">
						<label class="col-sm-3">Concepto Descuento:</label>
						<div class="col-sm-9">
							{{$novedad->concepto_descuento}}
						</div>
					</div>
					@endif
					@if($novedad->nombre_cargo != '')
					<div class="form-group">
						<label class="col-sm-3">Nombre del Cargo:</label>
						<div class="col-sm-9">
							{{$novedad->nombre_cargo}}
						</div>
					</div>
					@endif
					@if($novedad->fecha_cambio != '')
					<div class="form-group">
						<label class="col-sm-3">Fecha del cambio:</label>
						<div class="col-sm-9">
							{{$novedad->fecha_cambio}}
						</div>
					</div>
					@endif
					@if($novedad->observaciones != '')
					<div class="form-group">
						<label class="col-sm-3">Observaciones:</label>
						<div class="col-sm-9">
							{{$novedad->observaciones}}
						</div>
					</div>
					@endif
					@if($novedad->entrega_soportes != '')
					<div class="form-group">
						<label class="col-sm-3">Entrega Soportes:</label>
						<div class="col-sm-9">
							{{$novedad->entrega_soportes}}
						</div>
					</div>
					@endif
					@if($novedad->tipo_novedad_id == 4)
					<div class="text-center">
						<a href="/novedades/generar-pdf-vacaciones/{{ $novedad->id }}" target="_blank">
							<img src="/pdfDownloadIcon.png" style="width: 50px;margin-bottom: 7px;">
							Descargar PDF
						</a>
					</div>
					@endif
					@if($novedad->tipo_novedad_id == 11)
					<div class="text-center">
						<a href="/novedades/generar-pdf-descuento/{{ $novedad->id }}" target="_blank">
							<img src="/pdfDownloadIcon.png" style="width: 50px;margin-bottom: 7px;">
							Descargar PDF
						</a>
					</div>
					@endif
					@if($novedad->tipo_novedad_id == 1 || $novedad->tipo_novedad_id == 2)
					<div class="text-center">
						<a href="/novedades/generar-pdf-permiso/{{ $novedad->id }}" target="_blank">
							<img src="/pdfDownloadIcon.png" style="width: 50px;margin-bottom: 7px;">
							Descargar PDF
						</a>
					</div>
					@endif
					@if($novedad->tipo_novedad_id == 5)
					<div class="text-center">
						<a href="/novedades/generar-pdf-horas/{{ $novedad->id }}" target="_blank">
							<img src="/pdfDownloadIcon.png" style="width: 50px;margin-bottom: 7px;">
							Descargar PDF
						</a>
					</div>
					@endif
				</form>
			</div>
		</div>
	</div>
</div>
@endforeach
<!--Fin Modal Ver Novedad -->

<!--Modal Editar Novedad -->
@foreach($novedades as $novedad)
<div id="editar-{{ $novedad->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Editar Novedad <b>{{$novedad->codigo}} - {{$novedad->funcionario->obtenerNombreCompleto()}}</b></h4>
			</div>
			<div class="modal-body">
				<form class="" method="POST" action="{{ url('/novedades/editar-novedad') }}">
					{{ csrf_field() }}
					<input type="hidden" name="novedad_id" value="{{ $novedad->id }}">
					<div class="form-group">
						<label for="tipo_novedad" class="control-label">Tipo de novedad:</label>					
						<input id="tipo_novedad" class="form-control" type="text" name="observaciones" value="{{$novedad->tipoNovedad->name}}" disabled></input>
					</div>
					@if($novedad->fecha_novedad != '')
					<div class="form-group">
						<label for="fecha_novedad" class="control-label">Fecha de novedad:</label>					
						<!-- <input id="fecha_novedad" class="form-control" type="text" name="fecha_novedad" value="{{$novedad->fecha_novedad}}"></input> -->
						<div class="input-group">
							<input id="fecha_novedad" type="text" class="form-control datepicker-autoclose" name="fecha_novedad" placeholder="{{$novedad->fecha_novedad}}">
							<span class="input-group-addon"><i class="icon-calender"></i></span>
						</div>
					</div>
					@else
					<div class="form-group" style="display:none;">
						<label for="fecha_novedad" class="control-label">Fecha de novedad:</label>					
						<!-- <input id="fecha_novedad" class="form-control" type="text" name="fecha_novedad" value="{{$novedad->fecha_novedad}}"></input> -->
						<div class="input-group">
							<input id="fecha_novedad" type="text" class="form-control datepicker-autoclose" name="fecha_novedad" placeholder="{{$novedad->fecha_novedad}}">
							<span class="input-group-addon"><i class="icon-calender"></i></span>
						</div>
					</div>
					@endif
					<div class="row">
						@if($novedad->numero_dias != '')
						<div class="col-sm-6" >
							<div class="form-group">
								<label for="numero_dias" class="control-label">Número Días:</label>					
								<input id="numero_dias" class="form-control" type="text" name="numero_dias" value="{{$novedad->numero_dias}}"></input>
							</div>
						</div>
						@else
						<div class="col-sm-6" style="display:none;">
							<div class="form-group">
								<label for="numero_dias" class="control-label">Número Días:</label>					
								<input id="numero_dias" class="form-control" type="text" name="numero_dias" value="{{$novedad->numero_dias}}"></input>
							</div>
						</div>
						@endif
						@if($novedad->numero_horas != '')
						<div class="col-sm-6">
							<div class="form-group">
								<label for="numero_horas" class="control-label">Número Horas:</label>					
								<input id="numero_horas" class="form-control" type="text" name="numero_horas" value="{{$novedad->numero_horas}}"></input>
							</div>
						</div>
						@else
						<div class="col-sm-6" style="display:none;">
							<div class="form-group">
								<label for="numero_horas" class="control-label">Número Horas:</label>					
								<input id="numero_horas" class="form-control" type="text" name="numero_horas" value="{{$novedad->numero_horas}}"></input>
							</div>
						</div>
						@endif
					</div>
					@if($novedad->tipo_hora_extra_id != '')
					<div class="form-group">
						<label for="tipo_hora_extra" class="control-label">Tipo Hora Extra:</label>
						<select id="tipo_hora_extra" name="tipo_hora_extra" class="form-control" >
							@foreach($tipos_horas_extra as $tipo_hora_extra)
							@if($tipo_hora_extra->id == $novedad->tipo_hora_extra_id)
							<option value="{{$tipo_hora_extra->id}}" selected>{{$tipo_hora_extra->name}}</option>
							@else
							<option value="{{$tipo_hora_extra->id}}">{{$tipo_hora_extra->name}}</option>
							@endif
							@endforeach
						</select>
					</div>
					@else
					<div class="form-group" style="display:none;">
						<label for="tipo_hora_extra" class="control-label">Tipo Hora Extra:</label>
						<select id="tipo_hora_extra" name="tipo_hora_extra" class="form-control" >
							<option value="" disabled="" selected="">Selecciona tipo de hora extra</option>
						</select>
					</div>
					@endif
					<div class="row">
						@if($novedad->hora_inicial != '')
						<div class="col-sm-6">
							<div class="form-group">
								<label for="hora_inicial" class="control-label">Hora Inicial:</label>
								<div class="input-group clockpicker" data-placement="bottom" data-align="top" data-autoclose="true">
									<input id="hora_inicial" type="time" class="form-control" value="{{$novedad->hora_inicial}}" name="hora_inicial"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
								</div>					
							</div>
						</div>
						@else
						<div class="col-sm-6" style="display:none;">
							<div class="form-group">
								<label for="hora_inicial" class="control-label">Hora Inicial:</label>
								<div class="input-group clockpicker" data-placement="bottom" data-align="top" data-autoclose="true">
									<input id="hora_inicial" type="time" class="form-control" value="{{$novedad->hora_inicial}}" name="hora_inicial"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
								</div>					
							</div>
						</div>
						@endif
						@if($novedad->hora_final != '')
						<div class="col-sm-6">
							<div class="form-group">
								<label for="hora_final" class="control-label">Hora Final:</label>
								<div class="input-group clockpicker" data-placement="bottom" data-align="top" data-autoclose="true">
									<input id="hora_final" type="time" class="form-control" value="{{$novedad->hora_final}}" name="hora_final"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
								</div>					
							</div>
						</div>
						@else
						<div class="col-sm-6" style="display:none;">
							<div class="form-group">
								<label for="hora_final" class="control-label">Hora Final:</label>
								<div class="input-group clockpicker" data-placement="bottom" data-align="top" data-autoclose="true">
									<input id="hora_final" type="time" class="form-control" value="{{$novedad->hora_final}}" name="hora_final"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
								</div>					
							</div>
						</div>
						@endif
					</div>
					@if($novedad->nueva_sede != '')
					<div class="form-group">
						<label for="nueva_sede" class="control-label">Nueva Sede:</label>
						<select id="nueva_sede" name="nueva_sede" class="form-control" >
							@foreach($sedes as $sede)
							@if($sede->id == $novedad->nueva_sede)
							<option value="{{$sede->id}}" selected>{{$sede->nombre}}</option>
							@else
							<option value="{{$sede->id}}">{{$sede->nombre}}</option>
							@endif
							@endforeach
						</select>
					</div>
					@else
					<div class="form-group" style="display:none;">
						<label for="nueva_sede" class="control-label">Nueva Sede:</label>
						<select id="nueva_sede" name="nueva_sede" class="form-control" >
							<option value="" disabled="" selected="">Selecciona tipo de hora extra</option>
						</select>
					</div>
					@endif
					@if($novedad->fecha_retiro != '')
					<div class="form-group">
						<label for="fecha_retiro" class="control-label">Fecha Retiro:</label>					
						<div class="input-group">
							<input id="fecha_retiro" type="text" class="form-control datepicker-autoclose" name="fecha_retiro" value="{{$novedad->fecha_retiro}}">
							<span class="input-group-addon"><i class="icon-calender"></i></span>
						</div>
					</div>
					@else
					<div class="form-group" style="display:none;">
						<label for="fecha_retiro" class="control-label">Fecha Retiro:</label>					
						<div class="input-group">
							<input id="fecha_retiro" type="text" class="form-control datepicker-autoclose" name="fecha_retiro" value="{{$novedad->fecha_retiro}}">
							<span class="input-group-addon"><i class="icon-calender"></i></span>
						</div>
					</div>
					@endif
					@if($novedad->nuevo_salario != '')
					<div class="form-group">
						<label for="nuevo_salario" class="control-label">Nuevo Salario:</label>
						<div class="input-group">
							<input id="nuevo_salario" type="text" class="form-control precio" name="nuevo_salario" value="{{$novedad->nuevo_salario}}">
							<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
						</div>
					</div>
					@else
					<div class="form-group" style="display:none;">
						<label for="nuevo_salario" class="control-label">Nuevo Salario:</label>
						<div class="input-group">
							<input id="nuevo_salario" type="text" class="form-control precio" name="nuevo_salario" value="{{$novedad->nuevo_salario}}">
							<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
						</div>
					</div>
					@endif
					@if($novedad->nuevo_horario != '')
					<div class="form-group">
						<label for="nuevo_horario" class="control-label">Nuevo Horario:</label>					
						<input id="nuevo_horario" class="form-control" type="text" name="nuevo_horario" value="{{$novedad->nuevo_horario}}"></input>
					</div>

					@endif
					<div class="row">
						@if($novedad->anio_vacaciones != '')
						<div class="col-md-6">
							<div class="form-group">
								<label for="anio_vacaciones" class="control-label">Año Vacaciones:</label>					
								<input id="anio_vacaciones" class="form-control" type="text" name="anio_vacaciones" value="{{$novedad->anio_vacaciones}}"></input>
							</div>
						</div>
								<!-- <div class="form-group">
									<label class="col-sm-3">Año Vacaciones:</label>
									<div class="col-sm-9">
										{{$novedad->anio_vacaciones}}
									</div>
								</div> -->
								@else
								<div class="col-md-6" style="display:none;">
									<div class="form-group">
										<label for="anio_vacaciones" class="control-label">Año Vacaciones:</label>					
										<input id="anio_vacaciones" class="form-control" type="text" name="anio_vacaciones" value="{{$novedad->anio_vacaciones}}"></input>
									</div>
								</div>
								@endif
								@if($novedad->dias_pendientes_vacaciones != '')
								<div class="col-md-6">
									<div class="form-group">
										<label for="dias_pendientes_vacaciones" class="control-label"># Días Pendientes:</label>					
										<input id="dias_pendientes_vacaciones" class="form-control" type="text" name="dias_pendientes_vacaciones" value="{{$novedad->dias_pendientes_vacaciones}}"></input>
									</div>
								</div>
								<!-- <div class="form-group">
									<label class="col-sm-3"># Días Pendientes:</label>
									<div class="col-sm-9">
										{{$novedad->dias_pendientes_vacaciones}}
									</div>
								</div> -->
								@else
								<div class="col-md-6" style="display:none;">
									<div class="form-group">
										<label for="dias_pendientes_vacaciones" class="control-label"># Días Pendientes:</label>					
										<input id="dias_pendientes_vacaciones" class="form-control" type="text" name="dias_pendientes_vacaciones" value="{{$novedad->dias_pendientes_vacaciones}}"></input>
									</div>
								</div>
								@endif
							</div>
							@if($novedad->dias_compensados != '')
							<div class="form-group">
								<label for="dias_compensados" class="control-label">¿Días compensados en dinero?:</label>					
								<select id="dias_compensados" name="dias_compensados" class="form-control" >
									@if($novedad->dias_compensados == 'Si')
									<option value="Si" selected>Si</option>
									<option value="No">No</option>
									@else
									<option value="Si">Si</option>
									<option value="No" selected>No</option>
									@endif
								</select>
								<!-- <input id="dias_pendientes_vacaciones" class="form-control" type="text" name="dias_pendientes_vacaciones" value="{{$novedad->dias_pendientes_vacaciones}}"></input> -->
							</div>
							<!-- <div class="form-group">
								<label class="col-sm-3">¿Días compensados en dinero?:</label>
								<div class="col-sm-9">
									{{$novedad->dias_compensados}}
								</div>
							</div> -->
							@else
							<div class="form-group" style="display:none;">
								<label for="dias_compensados" class="control-label">¿Días compensados en dinero?:</label>					
								<select id="dias_compensados" name="dias_compensados" class="form-control" >
									<option value="" disabled="" selected="">Selecciona una opción</option>
								</select>
								<!-- <input id="dias_pendientes_vacaciones" class="form-control" type="text" name="dias_pendientes_vacaciones" value="{{$novedad->dias_pendientes_vacaciones}}"></input> -->
							</div>
							@endif
							@if($novedad->fecha_inicio_vacaciones != '')
							<div class="form-group">
								<label for="fecha_inicio_vacaciones" class="control-label">Fecha Inicio Vacaciones:</label>					
								<!-- <input id="fecha_novedad" class="form-control" type="text" name="fecha_novedad" value="{{$novedad->fecha_novedad}}"></input> -->
								<div class="input-group">
									<input id="fecha_inicio_vacaciones" type="text" class="form-control datepicker-autoclose" name="fecha_inicio_vacaciones" value="{{$novedad->fecha_inicio_vacaciones}}">
									<span class="input-group-addon"><i class="icon-calender"></i></span>
								</div>
							</div>
							<!-- <div class="form-group">
								<label class="col-sm-3">Fecha Inicio Vacaciones:</label>
								<div class="col-sm-9">
									{{$novedad->fecha_inicio_vacaciones}}
								</div>
							</div> -->
							@else
							<div class="form-group" style="display:none;">
								<label for="fecha_inicio_vacaciones" class="control-label">Fecha Inicio Vacaciones:</label>					
								<!-- <input id="fecha_novedad" class="form-control" type="text" name="fecha_novedad" value="{{$novedad->fecha_novedad}}"></input> -->
								<div class="input-group">
									<input id="fecha_inicio_vacaciones" type="text" class="form-control datepicker-autoclose" name="fecha_inicio_vacaciones" value="{{$novedad->fecha_inicio_vacaciones}}">
									<span class="input-group-addon"><i class="icon-calender"></i></span>
								</div>
							</div>
							@endif
							@if($novedad->fecha_final_vacaciones != '')
							<div class="form-group">
								<label for="fecha_final_vacaciones" class="control-label">Fecha Final Vacaciones:</label>					
								<!-- <input id="fecha_novedad" class="form-control" type="text" name="fecha_novedad" value="{{$novedad->fecha_novedad}}"></input> -->
								<div class="input-group">
									<input id="fecha_final_vacaciones" type="text" class="form-control datepicker-autoclose" name="fecha_final_vacaciones" value="{{$novedad->fecha_final_vacaciones}}">
									<span class="input-group-addon"><i class="icon-calender"></i></span>
								</div>
							</div>
							<!-- <div class="form-group">
								<label class="col-sm-3">Fecha Final Vacaciones:</label>
								<div class="col-sm-9">
									{{$novedad->fecha_final_vacaciones}}
								</div>
							</div> -->
							@else
							<div class="form-group" style="display:none;">
								<label for="fecha_final_vacaciones" class="control-label">Fecha Final Vacaciones:</label>					
								<!-- <input id="fecha_novedad" class="form-control" type="text" name="fecha_novedad" value="{{$novedad->fecha_novedad}}"></input> -->
								<div class="input-group">
									<input id="fecha_final_vacaciones" type="text" class="form-control datepicker-autoclose" name="fecha_final_vacaciones" value="{{$novedad->fecha_final_vacaciones}}">
									<span class="input-group-addon"><i class="icon-calender"></i></span>
								</div>
							</div>
							@endif
							@if($novedad->tipo_descuento_id != '')
							<!-- <div class="form-group">
								<label class="col-sm-3">Tipo Descuento:</label>
								<div class="col-sm-9">
									{{$novedad->tipoDescuento->name}}
								</div>
							</div> -->
							<div class="form-group">
								<label for="tipo_descuento" class="control-label">Tipo Descuento:</label>
								<select id="tipo_descuento" name="tipo_descuento" class="form-control" >
									@foreach($tipos_descuentos as $tipo_descuento)
									@if($tipo_descuento->id == $novedad->tipo_descuento_id)
									<option value="{{$tipo_descuento->id}}" selected>{{$tipo_descuento->name}}</option>
									@else
									<option value="{{$tipo_descuento->id}}">{{$tipo_descuento->name}}</option>
									@endif
									@endforeach
								</select>
							</div>
							@else
							<div class="form-group" style="display:none;">
								<label for="tipo_descuento" class="control-label">Tipo Descuento:</label>
								<select id="tipo_descuento" name="tipo_descuento" class="form-control" >
									<option value="" disabled="" selected="">Selecciona una opción</option>
								</select>
							</div>
							@endif
							@if($novedad->valor_descuento != '')
							<!-- <div class="form-group">
								<label class="col-sm-3">Valor Descuento:</label>
								<div class="col-sm-9">
									{{$novedad->obtenerValorDescuento()}}
								</div>
							</div> -->

							<div class="form-group">
								<label for="valor_descuento" class="control-label">Valor Descuento:</label>
								<div class="input-group">
									<input id="valor_descuento" type="text" class="form-control precio" name="valor_descuento" value="{{$novedad->valor_descuento}}">
									<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
								</div>
							</div>

							@endif
							<div class="row">
								@if($novedad->numero_cuotas_quincenales != '')
								<div class="col-md-6">
									<div class="form-group">
										<label for="numero_cuotas_quincenales" class="control-label"># Cuentas Quincenales:</label>					
										<input id="numero_cuotas_quincenales" class="form-control" type="number" name="numero_cuotas_quincenales" value="{{$novedad->numero_cuotas_quincenales}}"></input>
									</div>
									<!-- <div class="form-group">
										<label class="col-sm-3"># Cuentas Quincenales:</label>
										<div class="col-sm-9">
											{{$novedad->numero_cuotas_quincenales}}
										</div>
									</div> -->
								</div>
								@else
								<div class="col-md-6" style="display:none;">
									<div class="form-group">
										<label for="numero_cuotas_quincenales" class="control-label"># Cuentas Quincenales:</label>					
										<input id="numero_cuotas_quincenales" class="form-control" type="number" name="numero_cuotas_quincenales" value="{{$novedad->numero_cuotas_quincenales}}"></input>
									</div>
									<!-- <div class="form-group">
										<label class="col-sm-3"># Cuentas Quincenales:</label>
										<div class="col-sm-9">
											{{$novedad->numero_cuotas_quincenales}}
										</div>
									</div> -->
								</div>
								@endif
								@if($novedad->numero_quincena != '')
								<div class="col-md-6">
									<!-- <div class="form-group">
										<label class="col-sm-3"># Quincenas:</label>
										<div class="col-sm-9">
											{{$novedad->numero_quincena}}
										</div>
									</div> -->
									<div class="form-group">
										<label for="numero_quincena" class="control-label"># Quincenas:</label>					
										<input id="numero_quincena" class="form-control" type="number" name="numero_quincena" value="{{$novedad->numero_quincena}}"></input>
									</div>
								</div>
								@else
								<div class="col-md-6" style="display:none;">
									<!-- <div class="form-group">
										<label class="col-sm-3"># Quincenas:</label>
										<div class="col-sm-9">
											{{$novedad->numero_quincena}}
										</div>
									</div> -->
									<div class="form-group">
										<label for="numero_quincena" class="control-label"># Quincenas:</label>					
										<input id="numero_quincena" class="form-control" type="number" name="numero_quincena" value="{{$novedad->numero_quincena}}"></input>
									</div>
								</div>
								@endif
							</div>
							@if($novedad->nombre_cargo != '')
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="mes_descuento" class="control-label">Nombre cargo:</label>	
										<input id="mes_descuento" class="form-control" type="text" name="mes_descuento" value="{{$novedad->nombre_cargo}}"></input>
									</div>
								</div>
							</div>
							@endif
							@if($novedad->fecha_cambio != '')
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="mes_descuento" class="control-label">Nombre cargo:</label>	
										<input id="fecha_cambio" type="text" class="form-control datepicker-autoclose" name="fecha_cambio" value="{{$novedad->fecha_cambio}}">
									<span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>
							</div>
							@endif
							<div class="row">
								@if($novedad->mes_descuento != '')
								<div class="col-md-6">
									<div class="form-group">
										<label for="mes_descuento" class="control-label">Mes Descuento:</label>					
										<input id="mes_descuento" class="form-control" type="text" name="mes_descuento" value="{{$novedad->mes_descuento}}"></input>
									</div>
									<!-- <div class="form-group">
										<label class="col-sm-3">Mes Descuento:</label>
										<div class="col-sm-9">
											{{$novedad->mes_descuento}}
										</div>
									</div> -->
								</div>
								@else
								<div class="col-md-6" style="display:none;">
									<div class="form-group">
										<label for="mes_descuento" class="control-label">Mes Descuento:</label>					
										<input id="mes_descuento" class="form-control" type="text" name="mes_descuento" value="{{$novedad->mes_descuento}}"></input>
									</div>
									<!-- <div class="form-group">
										<label class="col-sm-3">Mes Descuento:</label>
										<div class="col-sm-9">
											{{$novedad->mes_descuento}}
										</div>
									</div> -->
								</div>
								@endif
								@if($novedad->anio_descuento != '')
								<div class="col-md-6">
									<div class="form-group">
										<label for="anio_descuento" class="control-label">Año Descuento:</label>					
										<input id="anio_descuento" class="form-control" type="number" name="anio_descuento" value="{{$novedad->anio_descuento}}"></input>
									</div>
									<!-- <div class="form-group">
										<label class="col-sm-3">Año Descuento:</label>
										<div class="col-sm-9">
											{{$novedad->anio_descuento}}
										</div>
									</div> -->
								</div>
								@else
								<div class="col-md-6" style="display:none;">
									<div class="form-group">
										<label for="anio_descuento" class="control-label">Año Descuento:</label>					
										<input id="anio_descuento" class="form-control" type="number" name="anio_descuento" value="{{$novedad->anio_descuento}}"></input>
									</div>
									<!-- <div class="form-group">
										<label class="col-sm-3">Año Descuento:</label>
										<div class="col-sm-9">
											{{$novedad->anio_descuento}}
										</div>
									</div> -->
								</div>
								@endif
							</div>
							@if($novedad->concepto_descuento != '')
							<div class="form-group">
								<label for="concepto_descuento" class="control-label">Concepto Descuento:</label>					
								<input id="concepto_descuento" class="form-control" type="text" name="concepto_descuento" value="{{$novedad->concepto_descuento}}"></input>
							</div>
							<!-- <div class="form-group">
								<label class="col-sm-3">Concepto Descuento:</label>
								<div class="col-sm-9">
									{{$novedad->concepto_descuento}}
								</div>
							</div> -->
							@else
							<div class="form-group" style="display:none;">
								<label for="concepto_descuento" class="control-label">Concepto Descuento:</label>					
								<input id="concepto_descuento" class="form-control" type="text" name="concepto_descuento" value="{{$novedad->concepto_descuento}}"></input>
							</div>
							<!-- <div class="form-group">
								<label class="col-sm-3">Concepto Descuento:</label>
								<div class="col-sm-9">
									{{$novedad->concepto_descuento}}
								</div>
							</div> -->
							@endif
							@if($novedad->observaciones != '')
							<div class="form-group">
								<label for="observaciones" class="control-label">Observaciones:</label>					
								<textarea id="observaciones" class="form-control" type="text" name="observaciones" value="">{{$novedad->observaciones}}</textarea>
							</div>
							@else
							<div class="form-group" style="display:none;">
								<label for="observaciones" class="control-label">Observaciones:</label>					
								<textarea id="observaciones" class="form-control" type="text" name="observaciones" value="">{{$novedad->observaciones}}</textarea>
							</div>
							@endif
							@if($novedad->entrega_soportes != '')
							<div class="form-group">
								<label class="control-label">Entrega Soportes:</label>
								<div class="">
									{{$novedad->entrega_soportes}}
								</div>
							</div>
							@else
							<div class="form-group" style="display:none;">
								<label class="control-label">Entrega Soportes:</label>
								<div class="">
									{{$novedad->entrega_soportes}}
								</div>
							</div>
							@endif
							<div class="text-center">
								<button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Modificar novedad</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		@endforeach
		<!--Fin Modal Editar Novedad -->
		<!--Modal Ver Archivos -->
		@foreach($novedades as $novedad)
		@if($novedad->archivos->count() > 0)
		<div id="archivos-{{ $novedad->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title">Archivos Novedad <b>{{$novedad->tipoNovedad->name}}</b></h4>
					</div>
					<div class="modal-body">
						<div class="table-responsive">
							<table class="table color-table info-table">
								<thead>
									<tr>
										<th>#</th>
										<th>Nombre</th>
										<th>Fecha</th>
										<th align="center">Descarga</th>
									</tr>
								</thead>
								<tbody>
									@foreach($novedad->archivos as $index=>$archivo)
									<tr>
										<td>{{$index+1}}</td>
										<td>{{$archivo->nombre}}</td>
										<td>{{$archivo->created_at}}</td>
										<td align="center"><a href="/storage/novedades/soportes/{{$archivo->nombre}}" class="waves-effect" download><i class="mdi mdi-download fa-fw"></i> <span class="hide-menu"></span></a></td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endif
		@endforeach
		<!--Fin Modal Ver Archivos -->
		<!--Modal Agregar Archivos -->
		@foreach($novedades as $novedad)
		<div id="agregar-archivos-{{ $novedad->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title">Archivos Novedad <b>{{$novedad->tipoNovedad->name}}</b></h4>
					</div>
					<div class="modal-body">
						<form role="form" method="POST" action="{{ url('/novedades/agregar-archivos-novedad') }}" enctype="multipart/form-data">
							{{ csrf_field() }}
							<div class="form-group">
								<label for="recipient-name" class="control-label">Soportes:</label>
								<input class="file-input soportes" multiple type="file" name="soportes[]" accept="image/*, application/pdf,.doc,.docx,.xls,.xlsx,.ppt,.pptx, application/pdf,application/ms-word">
							</div>
							<input type="hidden" name="novedad_id" value="{{ $novedad->id }}">
							<div class="modal-footer">
								<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
								<button type="submit" class="btn btn-danger waves-effect waves-light">Enviar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		@endforeach
		<!--Fin Modal Agregar Archivos -->
		<!--Modal Fechas -->
		<div id="modal_fechas" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						<h4 class="modal-title" id="myModalLabel">Filtrar Novedades</h4>
					</div>
					<div class="modal-body">
						<div class="menu_seleccion">
							<h4>Selecciona el tipo de consulta que quieres realizar</h4>
							<div class="btn btn-primary btn-block btn-lg" onclick="$('#fdia').show(); $('.menu_seleccion').hide(); ">
								<div class="texto-seleccion">
									Registros por día
								</div>
							</div>
							<div class="btn btn-primary btn-block btn-lg" onclick="$('#ffechas').show(); $('.menu_seleccion').hide();">
								<div class="texto-seleccion">
									Registros Entre Fechas
								</div>
							</div>
						</div>
						<div id="fdia" style="display:none">
							<form id="formulario_reporte_dia" role="form" method="POST" action="">
								{{ csrf_field() }}
								<h3>Filtrar registros por día: </h3>
								<div class="form-group">
									<div class="input-group">
										<input type="text" id="dia" class="form-control datepicker-autoclose" name="fecha_inicial" placeholder="yyyy-mm-dd" required>
										<span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary btn-lg">
										Filtrar
									</button>
									<button class="btn btn-primary btn-lg" onclick="$('#fdia').hide(); $('.menu_seleccion').show();" >
										Atrás
									</button>
								</div>
							</form>
						</div>
						<div id="ffechas" style="display:none">
							<form id="formulario_reporte_fechas" role="form" method="POST" action="">
								{{ csrf_field() }}
								<h3>Filtrar registros entre fechas:</h3>
								<div class="form-group">
									<label class="control-label">Fecha Inicial:</label>
									<div class="input-group">
										<input type="text" id="inicio" class="form-control datepicker-autoclose" name="fecha_inicial" placeholder="yyyy-mm-dd" required>
										<span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label">Fecha Final:</label>
									<div class="input-group">
										<input type="text" id="fin" class="form-control datepicker-autoclose" name="fecha_final" placeholder="yyyy-mm-dd" required disabled>
										<span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" class="btn btn-primary btn-lg">
										Filtrar
									</button>
									<button class="btn btn-primary btn-lg" onclick="$('#ffechas').hide(); $('.menu_seleccion').show();" >
										Atrás
									</button>
								</div>
							</form>    
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--Fin Modal Fechas -->
		@endsection
		@section('scripts')
		<script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
		<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
		<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
		<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.9/js/fileinput.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.9/js/locales/es.js"></script>
		<script src="https://fastcdn.org/FileSaver.js/1.1.20151003/FileSaver.min.js"></script>
		<script src="../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
		<script src="../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
		<script src="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
		<script src="../plugins/bower_components/bootstrap-datepicker/bootstrap-datetimepicker.es.js"></script>
		<script src="../js/jquery.maskMoney.min.js"></script>

		<script>
			$(document).ready(function() {
				$('#tabla_registros').DataTable({
					dom: 'Bfrtip',
					buttons: [
					{
						extend: 'excel',
						exportOptions: {
							columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 ]
						}
					},
					{
						extend: 'pdf',
						exportOptions: {
							columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 ]
						}
					}
					],
					"columnDefs": [
					{
						"targets": [ 2, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30 ],
						"visible": false
					},
					{ "width": 165, "targets": 31 }
					],
					"language": {
						"url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
					}
				});
				$(".soportes").fileinput({
					showUpload:false,
					language: 'es'
				});

				$('.datepicker-autoclose').datepicker({
					autoclose: true,
					todayHighlight: true,
					format: 'yyyy-mm-dd',
					language: 'es'
				});

				$("#inicio").datepicker()
				.on('changeDate', function (selected) {
					$("#fin").attr('disabled',false);
					var minDate = new Date(selected.date.valueOf());
					minDate.setDate(minDate.getDate()+1);
					$('#fin').datepicker('setStartDate', minDate);
				});

				$("#fin").datepicker()
				.on('changeDate', function (selected) {
					var minDate = new Date(selected.date.valueOf());
					$('#inicio').datepicker('setEndDate', minDate);
				});

				$("#formulario_reporte_dia").submit(function(e) {
					e.preventDefault();
					var fecha_inicial = $("input[name='fecha_inicial']").val();
					var token = $("input[name='_token']").val();
					$.ajax({
						url: '{{route("reporte")}}',
						type: 'POST',
						data: {fecha_inicial:fecha_inicial, _token:token},
						dataType: 'JSON',
						success: function(data) {
							$('#modal_fechas').modal('hide');
							$("#div_novedades").html('');
							$("#div_novedades").html(data.options);					
						}
					});
				});

				$("#formulario_reporte_fechas").submit(function(e) {
					e.preventDefault();
					var fecha_inicial 	= $("#inicio").val();
					var fecha_final 	= $("#fin").val();
					var token 			= $("input[name='_token']").val();
					$.ajax({
						url: '{{route("reporte")}}',
						type: 'POST',
						data: {fecha_inicial:fecha_inicial, fecha_final:fecha_final, _token:token},
						dataType: 'JSON',
						success: function(data) {
							$('#modal_fechas').modal('hide');
							$("#div_novedades").html('');
							$("#div_novedades").html(data.options);					
						}
					});
				});
				$(".precio").maskMoney({prefix:'$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false, precision: 0});
			});

		</script>

		@if (session('mensaje'))
		<script type="text/javascript">
			swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
		</script>
		@endif
		@endsection