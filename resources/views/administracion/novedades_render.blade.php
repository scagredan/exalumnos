<table id="tabla_registros" class="table table-striped">
	<thead>
		<tr>
			<th>Código</th>
			<th>Funcionario</th>
			<th># Identificación</th>
			<th>Tipo</th>
			<th>Sede</th>
			<th>Fecha Creación</th>
			<th>Fecha Novedad</th>
			<th># Días</th>
			<th># Horas</th>
			<th>Tipo Hora Extra</th>
			<th>Hora Inicial</th>
			<th>Horal Final</th>
			<th>Nueva Sede</th>
			<th>Fecha Retiro</th>
			<th>Nuevo Salario</th>
			<th>Nuevo Horario</th>
			<th>Año Vacaciones</th>
			<th>Días pendientes vacaciones</th>
			<th>Días compensados en dinero</th>
			<th>Fecha Inicio Vacaciones</th>
			<th>Fecha Final Vacaciones</th>
			<th>Tipo Descuento</th>
			<th>Valor Descuento</th>
			<th># Cuotas Quincenales</th>
			<th># Quincena</th>
			<th>Mes Descuento</th>
			<th>Año Descuento</th>
			<th>Concepto Descuento</th>
			<th>Observaciones</th>
			<th>Entrega Soportes</th>
			<th>Acciones</th>
		</tr>
	</thead>
	<tbody>
		@foreach($novedades as $index=>$novedad)
		<tr>
			<td><b>{{$novedad->codigo}}</b></td>
			<td>{{$novedad->funcionario->obtenerNombreCompleto()}}</td>
			<td>{{$novedad->funcionario->numero_documento}}</td>
			<td><b>{{$novedad->tipoNovedad->name}}</b></td>
			<td>{{$novedad->funcionario->obtenerSede()}}</td>
			<td>{{$novedad->created_at}}</td>
			<td>{{$novedad->fecha_novedad}}</td>
			<td>{{$novedad->numero_dias}}</td>
			<td>{{$novedad->numero_horas}}</td>
			@if(empty($novedad->tipoHoraExtra))
			<td>{{$novedad->tipo_hora_extra_id}}</td>
			@else
			<td>{{$novedad->tipoHoraExtra->name}}</td>
			@endif
			<td>{{$novedad->hora_inicial}}</td>
			<td>{{$novedad->hora_final}}</td>
			@if(empty($novedad->nueva_sede))
			<td>{{$novedad->nueva_sede}}</td>
			@else
			<td>{{$novedad->obtenerSede()}}</td>
			@endif
			<td>{{$novedad->fecha_retiro}}</td>
			<td>{{$novedad->nuevo_salario}}</td>
			<td>{{$novedad->nuevo_horario}}</td>
			<td>{{$novedad->anio_vacaciones}}</td>
			<td>{{$novedad->dias_pendientes_vacaciones}}</td>
			<td>{{$novedad->dias_compensados}}</td>
			<td>{{$novedad->fecha_inicio_vacaciones}}</td>
			<td>{{$novedad->fecha_final_vacaciones}}</td>
			@if(empty($novedad->tipoDescuento))
			<td>{{$novedad->tipo_descuento_id}}</td>
			@else
			<td>{{$novedad->tipoDescuento->name}}</td>
			@endif
			<td>{{$novedad->valor_descuento}}</td>
			<td>{{$novedad->numero_cuotas_quincenales}}</td>
			<td>{{$novedad->numero_quincena}}</td>
			<td>{{$novedad->mes_descuento}}</td>
			<td>{{$novedad->anio_descuento}}</td>
			<td>{{$novedad->concepto_descuento}}</td>
			<td>{{$novedad->observaciones}}</td>
			<td>{{$novedad->entrega_soportes}}</td>
			<td>
				<button data-toggle="modal" title="Ver Novedad" type="button" data-target="#ver-{{ $novedad->id }}" class="btn btn-danger btn-outline btn-circle btn-md m-r-5"><i class="ti-eye"></i>
				</button>
				<button data-toggle="modal" title="Agregar Archivos" type="button" data-target="#agregar-archivos-{{ $novedad->id }}" class="btn btn-danger btn-outline btn-circle btn-md m-r-5"><i class="ti-cloud-up"></i>
				</button>
				@if($novedad->archivos->count() > 0)
				<button data-toggle="modal" title="Listado Archivos" type="button" data-target="#archivos-{{ $novedad->id }}" class="btn btn-danger btn-outline btn-circle btn-md m-r-5"><i class="ti-files"></i>
				</button>
				@endif
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
<script>
	$(document).ready(function() {
		$('#tabla_registros').DataTable({
			dom: 'Bfrtip',
			buttons: [
			{
				extend: 'excel',
				exportOptions: {
					columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 ]
				}
			},
			{
				extend: 'pdf',
				exportOptions: {
					columns: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 ]
				}
			}
			],
			"columnDefs": [
			{
				"targets": [ 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29 ],
				"visible": false
			}
			],
			"language": {
				"url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
			}
		});
	});
	</script>