@if(!empty($salarios))
	@if (count($salarios) > 1)
			<div class="form-group">
				<label class="col-xs-3 control-label">Tipo Salario</label>
				<div class="col-xs-5 input-group">
					<select name="salario" class="form-control" required>
						<option value="" disabled selected>Selecciona el periodo del salario</option>
						@foreach($salarios as $salario)
							<option value="{{ $salario->id }}">{{ $salario->periodo }}</option>
						@endforeach
					</select>
				</div>
			</div>
		@else
			@foreach($salarios as $salario)
				<input type="hidden" name="salario" value="{{ $salario->id }}">
			@endforeach
	@endif
@endif