@extends('layouts.dashboard')
@section('title')
<title>Cosmos - Administrar Permisos</title>
@endsection
@section('css')
<!-- Wizard CSS -->
<link href="../plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">
<link href="../plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="../plugins/bower_components/jquery-wizard-master/libs/formvalidation/formValidation.min.css" rel="stylesheet" >
<link href="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Administrar Permisos</h4> 
		</div>
		<!-- /.col-lg-12 -->
	</div>
    <div class="row">
		<div class="col-sm-12">
			<div class="white-box panel panel-default block3">
				<h3 class="box-title m-b-0">Formulario de creación</h3>
				<p class="text-muted m-b-30 font-13"> Completa totalmente el formulario para crear un nuevo rol de usuario.</p>
                <form id="formulario_permisos" class="form-horizontal" method="post" action="{{route('crear-rol')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Nombre del rol</label>
                        <div class="col-xs-5 input-group">
                            <input name="name" id="name" type="text" class="form-control">
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Crear rol</button>
                    </div>
                </form>
			</div>
		</div>
	</div>
	<!-- .row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box panel panel-default block3">
				<h3 class="box-title m-b-0">Formulario de administración</h3>
				<p class="text-muted m-b-30 font-13"> Completa totalmente el formulario segun los permisos que desea para los usuarios.</p>
                <form id="formulario_permisos" class="form-horizontal" method="post" action="{{route('administrar-rol')}}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Seleccione el rol</label>
                        <div class="col-xs-5 input-group">
                            <select name="tipo_usuario" id="tipo_usuario" required class="form-control">
                                <option value="" disabled="" selected="">Selecciona el rol</option>
                                @foreach($tipos_usuario as $tipo)
                                    <option value="{{$tipo->id}}">{{$tipo->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <h3 class="box-title m-t-40">Funcionarios</h3>
                    <hr>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Agregar funcionario</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="agregar_funcionario" id="agregar_funcionario" type="checkbox">
                            <label for="agregar_funcionario"> </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Listado de funcionarios</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="listado_funcionarios" id="listado_funcionarios" type="checkbox">
                            <label for="listado_funcionarios"> </label>
                            
                        </div>
                    </div>
                    <div class="form-group">

                        <label class="col-xs-1 col-xs-offset-2 control-label">Ver</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="funcionarios_ver" id="funcionarios_ver" type="checkbox">
                            <label for="funcionarios_ver"> </label>
              
                        </div>
                        <label class="col-xs-1 control-label">Editar</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="funcionarios_editar" id="funcionarios_editar" type="checkbox">
                            <label for="funcionarios_editar"> </label>
              
                        </div>
                        <label class="col-xs-1  control-label">Generar novedad</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="funcionarios_generarNovedad" id="funcionarios_generarNovedad" type="checkbox">
                            <label for="funcionarios_generarNovedad"> </label>
                            
                        </div>
                        <label class="col-xs-1  control-label">Asignar Jefe</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="funcionarios_asginarJefe" id="funcionarios_asginarJefe" type="checkbox">
                            <label for="funcionarios_asginarJefe"> </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Listado de novedades</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="listado_novedades" id="listado_novedades" type="checkbox">
                            <label for="listado_novedades"> </label>
                            
                        </div>
                    </div>
                    <div class="form-group">

                        <label class="col-xs-1 col-xs-offset-2 control-label">Ver</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="novedades_ver" id="novedades_ver" type="checkbox">
                            <label for="novedades_ver"> </label>
                            
                        </div>
                        <label class="col-xs-1 control-label">Editar</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="novedades_editar" id="novedades_editar" type="checkbox">
                            <label for="novedades_editar"> </label>
                            
                        </div>
                        <label class="col-xs-1 control-label">Adjuntar</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="novedades_adjuntar" id="novedades_adjuntar" type="checkbox">
                            <label for="novedades_adjuntar"> </label>
                            
                        </div>
                        <label class="col-xs-1 control-label">Ver archivos</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="novedades_verArchivos" id="novedades_verArchivos" type="checkbox">
                            <label for="novedades_verArchivos"> </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-3 control-label">Exportar funcionarios</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name=funcionarios_exportar id="funcionarios_exportar" type="checkbox">
                            <label for="funcionarios_exportar"> </label>
                            
                        </div>
                    </div>
                    <h3 class="box-title m-t-40">Nómina</h3>
                    <hr>
                    <div class="form-group">
                        <label class="col-xs-2 control-label">Configuración</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="configuracion_nomina" id="configuracion_nomina" type="checkbox">
                            <label for="configuracion_nomina"> </label>
                            
                        </div>
                        <label class="col-xs-3 control-label">Códigos contables </label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="codigos_contables" id="codigos_contables" type="checkbox">
                            <label for="codigos_contables"> </label>
                            
                        </div>
                        <label class="col-xs-3 control-label">Módulo salarial</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="modulo_salarial" id="modulo_salarial" type="checkbox">
                            <label for="modulo_salarial"> </label>
                            
                        </div>
                    </div>
                    <h3 class="box-title m-t-40">Administración</h3>
                    <hr>
                    <div class="form-group">
                        <label class="col-xs-2 control-label">Usuarios</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="usuarios" id="usuarios" type="checkbox">
                            <label for="usuarios"> </label>
                            
                        </div>
                        <label class="col-xs-3 control-label">AFP</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="afp" id="afp" type="checkbox">
                            <label for="afp"> </label>
                            
                        </div>
                        <label class="col-xs-3 control-label">ARL</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="arl" id="arl" type="checkbox">
                            <label for="arl"> </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-2 control-label">Bancos</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="bancos" id="bancos" type="checkbox">
                            <label for="bancos"> </label>
                            
                        </div>
                        <label class="col-xs-3 control-label">Cajas de compensación</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="cajas_compensacion" id="cajas_compensacion" type="checkbox">
                            <label for="cajas_compensacion"> </label>
                            
                        </div>
                        <label class="col-xs-3 control-label">Dotaciones</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="dotaciones" id="dotaciones" type="checkbox">
                            <label for="dotaciones"> </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-2 control-label">EPS</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="eps" id="eps" type="checkbox">
                            <label for="eps"> </label>
                            
                        </div>

                        <label class="col-xs-3 control-label">Escalafones</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="escalafones" id="escalafones" type="checkbox">
                            <label for="escalafones"> </label>
                            
                        </div>

                        <label class="col-xs-3 control-label">Estados civiles</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="estados_civiles" id="estados_civiles" type="checkbox">
                            <label for="estados_civiles"> </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-2 control-label">Fondos de cesantias</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="fondos_cesantias" id="fondos_cesantias" type="checkbox">
                            <label for="fondos_cesantias"> </label>
                            
                        </div>
                        <label class="col-xs-3 control-label">Tipos funcionarios</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="tipos_funcionarios" id="tipos_funcionarios" type="checkbox">
                            <label for="tipos_funcionarios"> </label>
                            
                        </div>
                        <label class="col-xs-3 control-label">Idiomas</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="idiomas" id="idiomas" type="checkbox">
                            <label for="idiomas"> </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-2 control-label">Niveles</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="niveles" id="niveles" type="checkbox">
                            <label for="niveles"> </label>
                            
                        </div>
                        <label class="col-xs-3 control-label">Niveles educativos</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="niveles_educativos" id="niveles_educativos" type="checkbox">
                            <label for="niveles_educativos"> </label>
                            
                        </div>
                        <label class="col-xs-3 control-label">Pruebas docentes</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="pruebas_docentes" id="pruebas_docentes" type="checkbox">
                            <label for="pruebas_docentes"> </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-2 control-label">Razas</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="razas" id="razas" type="checkbox">
                            <label for="razas"> </label>
                            
                        </div>
                        <label class="col-xs-3 control-label">Tallas</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="tallas" id="talla" type="checkbox">
                            <label for="tallas"> </label>
                            
                        </div>
                        <label class="col-xs-3 control-label">Tipos documentos</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="tipos_documentos" id="tipos_documentos" type="checkbox">
                            <label for="tipos_documentos"> </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-2 control-label">Turnos</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="turnos" id="turnos" type="checkbox">
                            <label for="turnos"> </label>
                            
                        </div>

                        <label class="col-xs-3 control-label">Visas</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="visas" id="visa" type="checkbox">
                            <label for="visas"> </label>
                            
                        </div>

                        <label class="col-xs-3 control-label">Tipos Contratos</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="contratos" id="contratos" type="checkbox">
                            <label for="contratos"> </label>
                            
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-2 control-label">Barrios</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="barrios" id="barrios" type="checkbox">
                            <label for="barrios"> </label>                            
                        </div>

                        <label class="col-xs-3 control-label">Cargos</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="cargos" id="cargos" type="checkbox">
                            <label for="cargos"> </label>                            
                        </div>
                        <label class="col-xs-3 control-label">Condiciones Contratos</label>
                        <div class="col-xs-1 checkbox checkbox-success">
                            <input name="condiciones" id="condiciones" type="checkbox">
                            <label for="condiciones"> </label>                            
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Modificar permisos</button>
                    </div>
                </form>
			</div>
		</div>
	</div>
<!-- /.row -->
@endsection
@section('scripts')

<!-- Form Wizard JavaScript -->
<script src="../plugins/bower_components/jquery-wizard-master/dist/jquery-wizard.min.js"></script>
<!-- FormValidation plugin and the class supports validating Bootstrap form -->
<script src="../plugins/bower_components/jquery-wizard-master/libs/formvalidation/formValidation.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.formvalidation/0.6.1/js/language/es_ES.js"></script>
<script src="../plugins/bower_components/jquery-wizard-master/libs/formvalidation/bootstrap.min.js"></script>
<script src="../plugins/bower_components/blockUI/jquery.blockUI.js"></script>
<!-- Sweet-Alert  -->
<script src="../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="../plugins/bower_components/bootstrap-datepicker/bootstrap-datetimepicker.es.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script>
$('#tipo_usuario').change(function () {
    $('input:checkbox').prop('checked', false);
    var id = $(this).find(':selected')[0].value;
    $.ajax({
        type: 'GET',
        url: "/permisos/obtener-permisos/" + id,
        success: function (data) {
            completarPermisos(data[0]);
        }
    });

});

function completarPermisos(data) {
    $.each( data, function( key, value ) {
        if(value){
            $('#' + key ).prop('checked', true);
        }
    });
}
</script>
@if (session('mensaje'))
<script type="text/javascript">
    swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
</script>
@endif
@endsection