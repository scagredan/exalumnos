@extends('layouts.dashboard')
@section('title')
<title>Cosmos - Listado de Salarios</title>
@endsection
@section('css')
<!-- Wizard CSS -->
<link href="../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="../plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="../plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="container-fluid">
	<!-- /row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Listado de Salarios</h3>
				<p class="text-muted m-b-30">
						<button data-toggle="modal" data-target="#crear" class="btn btn-info waves-effect waves-light" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Agregar nuevo salario</button>
				</p>
				<div class="table-responsive">
					<table id="tabla_registros" class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Código Contable</th>
								<th>Período</th>
								<th>Base</th>
								<th>Auxilio de Transporte</th>
								<th>Bono Alimentación</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($registros as $index=>$registro)
							<tr>
								<td>{{$index+1}}</td>
								<td>{{$registro->codigos->nombre}}</td>
								<td>{{$registro->periodo}}</td>
								<td>{{$registro->obtenerValorBase()}}</td>
								<td>{{$registro->obtenerValorAuxilioTransporte()}}</td>
								<td>{{$registro->obtenerValorBonoAlimentacion()}}</td>
								<td>
									<button data-toggle="modal" title="Editar salario" type="button" data-target="#editar-{{ $registro->id }}" class="btn btn-danger btn-outline btn-circle btn-md m-r-5"><i class="ti-pencil"></i>
									</button>
									@if($registro->activo == 1)
										<input type="checkbox" data-render="switchery" value="{{$registro->activo}}" checked class="js-switch js-check-change" data-color="#13dafe" data-salario-id="{{$registro->id}}" />
									@else
										<input type="checkbox" data-render="switchery" value="{{$registro->activo}}" class="js-switch js-check-change" data-color="#13dafe" data-salario-id="{{$registro->id}}" />
									@endif
								</td>
							</tr>
							@endforeach								
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Modal Creación -->
<div id="crear" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Nuevo salario</h4>
			</div>
			<div class="modal-body">
				<form role="form" method="POST" action="{{ url('/administracion/crear-salario') }}">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="recipient-name" class="control-label">Código Contable:</label>
						<select name="codigo_contable" required class="form-control">
							<option value="" disabled="" selected="" >Selecciona el código contable</option>
							@foreach($codigos_contables as $item)
								<option value="{{$item->id}}">{{$item->nombre}}</option>
							@endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label">Período:</label>
						<input type="text" class="form-control" name="periodo" required>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label">Base:</label>
						<input type="text" class="form-control precio" name="base" onchange="consultarAuxilioTransporte(this.value)" required>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label">Auxilio de Transporte:</label>
						<input type="number" class="form-control" name="c_auxilio_transporte" value="" required readonly>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label">Bono Alimentación:</label>
						<input type="text" class="form-control precio" name="bono_alimentacion" required>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-danger waves-effect waves-light">Crear</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function consultarAuxilioTransporte($salario){
			var token = $("input[name='_token']").val();
			$.ajax({
				url: '{{route("auxilio-transporte")}}',
				method: 'POST',
				data: {salario:$salario, _token:token},
				success: function(data) {
					$("input[name='c_auxilio_transporte']").val(data.auxilio_transporte);
				}
			});
		}
	</script>
</div> 
<!--Modal Edición -->
@foreach($registros as $registro)
<div id="editar-{{ $registro->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Edición salario</h4>
			</div>
			<div class="modal-body">
				<form role="form" method="POST" action="{{ url('/administracion/editar-salario') }}">
					{{ csrf_field() }}

					<div class="form-group">
						<label for="recipient-name" class="control-label">Código Contable:</label>
						<select name="codigo_contable" class="form-control" required >						
			              @foreach($codigos_contables as $codigo)
			                @if ($codigo->id == $registro->codigo_contable_id)
			                    <option selected value="{{ $codigo->id }}">{{ $codigo->nombre }}</option>
			                @else
			                    <option value="{{ $codigo->id }}">{{ $codigo->nombre }}</option>
			                @endif
			              @endforeach
						</select>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label">Período:</label>
						<input type="text" class="form-control" name="periodo" value="{{ $registro->periodo }}"  required>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label">Base:</label>
						<input type="text" class="form-control precio" name="base" value="{{ $registro->base }}" onchange="consultarAuxilioTransporteEdit(this.value)"  required>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label">Auxilio de Transporte:</label>
						<input type="number" class="form-control" name="e_auxilio_transporte" value="{{ $registro->auxilio_transporte }}" required readonly>
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label">Bono Alimentación:</label>
						<input type="text" class="form-control precio" name="bono_alimentacion" value="{{ $registro->bono_alimentacion }}" required>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-danger waves-effect waves-light">Editar</button>
					</div>
					<input type="hidden" name="id" value="{{ $registro->id }}">
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function consultarAuxilioTransporteEdit($salario){
			var token = $("input[name='_token']").val();
			$.ajax({
				url: '{{route("auxilio-transporte")}}',
				method: 'POST',
				data: {salario:$salario, _token:token},
				success: function(data) {
					$("input[name='e_auxilio_transporte']").val(data.auxilio_transporte);
				}
			});
		}
	</script>
</div>
@endforeach
@endsection
@section('scripts')
<script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="../plugins/bower_components/switchery/dist/switchery.min.js"></script>
<script src="../js/jquery.maskMoney.min.js"></script>
<script>
	$(document).ready(function() {
		$('#tabla_registros').DataTable({
			"language": {
				"url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
			}
		});
		$(".precio").maskMoney({prefix:'$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false, precision: 0});
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());
        });

		elems.forEach(function(el) {
			el.onchange = function(e) {
				if ($(this).is(':checked')) {
					cambiarEstadoSalario($(this).data("salario-id"), $(this).is(':checked'));
				} else {
					cambiarEstadoSalario($(this).data("salario-id"), $(this).is(':checked'));
				}
			}
		});
		function cambiarEstadoSalario($salario_id, $estado){
			var token = $("input[name='_token']").val();
			$.ajax({
				url: '{{route("cambiar-estado")}}',
				method: 'POST',
				data: {salario_id:$salario_id, _token:token, estado:$estado ? 1 : 0},
				success: function(data) {
					if(data.success) {
						swal("Buen Trabajo!", "Cambio de estado exitoso!", "success");
					}
					else{
						swal("Oops...", "Error cambiando el estado, por favor contacta al administrador", "error");
					}
				}
			});
		}
	});

</script>
@if (session('mensaje'))
<script type="text/javascript">
	swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
</script>
@endif
@endsection