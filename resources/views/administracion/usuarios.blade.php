@extends('layouts.dashboard')
@section('title')
<title>Cosmos - Listado de usuarios</title>
@endsection
@section('css')
<!-- Wizard CSS -->
<link href="../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="../plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<link href="../plugins/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" >
<link href="../plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="../plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="../plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
@endsection
@section('content')
<div class="container-fluid">
	<!-- /row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Listado de Usuarios</h3>
				<p class="text-muted m-b-30">
				<!--button data-toggle="modal" data-target="#crear" class="btn btn-info waves-effect waves-light" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Agregar Usuario</button-->
				</p>

				<div class="ui-widget">
					<input id="buscar_usuario" placeholder="Buscar...">
				</div>
				<div class="table-responsive">
					<table id="tabla_usuarios" class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th># Documento</th>
								<th>Nombre</th>
								<th>Correo</th>
								<th>Tipo</th>
								<th>Foto</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($usuarios as $index=>$usuario)
							<tr>
								<td>{{$index+1}}</td>
								<td>{{$usuario->funcionario->numero_documento}}</td>
								<td>{{$usuario->name}}</td>
								<td>{{$usuario->email}}</td>
								<td>{{$usuario->tipoUsuario->name}}</td>
								<td><img width="50px" src="/storage/usuarios/{{$usuario->imagen}}" alt="user-img" class="img-circle"></td>
								<td>
									<button data-toggle="modal" title="Editar Usuario" data-target="#editar-{{ $usuario->id }}" type="button" class="btn btn-danger btn-outline btn-circle btn-lg m-r-5"><i class="ti-pencil"></i>
									</button>
								</td>
							</tr>
							@endforeach								
						</tbody>
					</table>
				</div>
				{{ $usuarios->links() }}
			</div>
		</div>
	</div>
</div>

<!--Modal Edición Usuario -->
@foreach($usuarios as $usuario)
<div id="editar-{{ $usuario->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Editar usuario <b>{{$usuario->name}}</b></h4>
			</div>
			<div class="modal-body">
				<form role="form" method="POST" action="{{ url('/usuarios/editar-usuario') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="recipient-name" class="control-label">Tipo Usuario:</label>						
						<select class="form-control" name="tipo_usuario_id">
							@foreach($tipos as $tipo)
								@if($usuario->tipo_usuario_id == $tipo->id)
									<option selected value="{{$tipo->id}}">{{$tipo->name}}</option>
								@else
									<option value="{{$tipo->id}}">{{$tipo->name}}</option>
								@endif	
							@endforeach
						</select>						
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label">Contraseña:</label>
						<input type="password" class="form-control" name="password" value="">
					</div>					
					<div class="clearfix"></div>
					<br><br>
					<label for="" class="control-label">Foto actual</label>
					<div class="clearfix"></div>
					<div class="col-xs-6">
						@if(!is_null($usuario->imagen))
							<img width="50px" src="/storage/usuarios/{{$usuario->imagen}}" class="img-responsive" alt="Foto Perfil">
						@else
							No hay imagen
						@endif	
					</div>
					<div class="clearfix"></div>
					<br><br><br>
					<div class="form-group">
						<label class="col-sm-12">Cambiar imagen</label>
						<input name="foto_perfil" type="file" id="input-file-now" class="dropify" /> 
					</div>
					<input type="text" value="{{$usuario->id}}" hidden="" name="user_id">
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-danger waves-effect waves-light">Editar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endforeach
@endsection
@section('scripts')
<script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
<script>
	$(document).ready(function() {
		$('.dropify').dropify();
		// $('#tabla_usuarios').DataTable({
		// 	"language": {
		// 		"url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
		// 	}
		// });
		$(".select2").select2();

		$("#buscar_usuario").autocomplete({
			source: "{{route('buscar-usuario')}}",
			minLength: 3,
			select: function(event, ui) {
				$('#buscar_usuario').val(ui.item.value);
				var ruta = ui.item.id;
				window.location = ruta;
			}
		});
	});

</script>
@if (session('mensaje'))
<script type="text/javascript">
	swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
</script>
@endif
@endsection