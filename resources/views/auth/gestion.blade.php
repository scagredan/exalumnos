@extends('layouts.dashboard')

@section('title')
<title>Recursos Humanos - Cambio de contraseña</title>
@endsection

@section('content')
<div class="container-fluid">
		<!-- .row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box panel panel-default block3">
				<form class="form-horizontal form-material" role="form" method="POST" action="{{ route('gestion-contrasena') }}">
					{{ csrf_field() }}
					<a href="javascript:void(0)" class="text-center db"><img src="../plugins/images/admin-logo-dark.png" alt="Home" /><br/><img src="../plugins/images/admin-text-dark.png" alt="Home" /></a> 
					<h3 class="box-title m-t-40 m-b-0">Cambia tu contraseña</h3><small>Valida la siguiente información para continuar</small> 

					<div class="form-group ">
						<div class="col-xs-12">
							<input class="form-control" name="actual" type="password" required="" placeholder="Escriba la anterior contraseña">

							@if (session('error1'))
							<span class="help-block">
								<strong>La anterior contraseña es erronea</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group ">
						<div class="col-xs-12">
							<input class="form-control" name="nueva" type="password" required="" placeholder="Escriba la nueva contraseña">
							@if (session('error2'))
							<span class="help-block">
								<strong>Las contraseñas no coinciden</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<div class="col-xs-12">
							<input class="form-control" type="password" required="" name="confirmacion" placeholder="Confirm Password">
						</div>
					</div>

					<div class="form-group text-center m-t-20">
						<div class="col-xs-12">
							<button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Cambiar contraseña</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
	@endsection
@section('scripts')
@if(session('exito'))
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script>
	swal('Cotraseña cambiada con éxito!');
</script>
@endif
@endsection