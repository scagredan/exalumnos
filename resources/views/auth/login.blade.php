@extends('layouts.ample')
@section('title')
<title>Cosmos - Login</title>
<style>
  html{
    background: #000617 !important;
  }
</style>
@endsection
@section('content')
<section id="wrapper" class="login-register">
  <div class="login-box login-sidebar">
    <div class="white-box" >
      <form class="form-horizontal form-material" role="form" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <a href="javascript:void(0)" class="text-center db"><img src="/images/logo.png" alt="Home" style="width: 100px" /><br/><!-- <img src="../plugins/images/american-text-white.png" alt="Home" /> --></a>  
        <div class="form-group m-t-40">
          <div class="col-xs-12 {{ $errors->has('email') ? ' has-error' : '' }}"">
            <input class="form-control " name="email" type="email" required="" placeholder="Correo electrónico" value="{{ old('email') }}">
          </div>
          @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
          @endif
        </div>
        <div class="form-group">
          <div class="col-xs-12 {{ $errors->has('password') ? ' has-error' : '' }}"">
            <input class="form-control " name="password" type="password" required="" placeholder="Contraseña" value="{{ old('password') }}">
            @if ($errors->has('password'))
            <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
            @endif
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <div class="checkbox checkbox-primary pull-left p-t-0">
              <input id="checkbox-signup" type="checkbox">
              <label for="checkbox-signup" > Recordarme </label>
            </div>
            <div class="clearfix"></div>
           
          </div>
        </div>

        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" style=" type="submit">Iniciar Sesión</button>
          </div>
        </div>
        <a href="/register" id="" class=" " style="font-size: 18px; text-decoration: underline;"><i class="fa fa-lock m-r-5"></i> ¿Eres un exalumno? Ingresa aquí</a>
      </form>
      <form class="form-horizontal" id="recoverform" action="index.html">
        <div class="form-group ">
          <div class="col-xs-12">
            <h3>Recuperar contraseña</h3>
            <p class="text-muted">Ingresa tu email para enviar un correo con las instrucciones! </p>
          </div>
        </div>
        <div class="form-group ">
          <div class="col-xs-12">
            <input class="form-control" type="text" required="" placeholder="Email">
          </div>
        </div>
        <div class="form-group text-center m-t-20">
          <div class="col-xs-12">
            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Cambiar</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>
@endsection
