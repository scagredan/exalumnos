@extends('layouts.ample')

@section('title')
<title>Recursos Humanos - Registro</title>

@endsection

@section('content')
<section id="wrapper" class="login-register">
  <div class="login-box" style="width: 80vw; margin: 1% auto;">
    <div class="white-box">
      <form action="/register2" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="step" value="1">
        <a href="javascript:void(0)" class="text-center db"><img src="/images/logo.png" alt="Home" /></a> 
        <h3 class="box-title m-t-40 m-b-0">Registrate ahora</h3><small>Networking Bartolino es una plataforma exclusiva para exalumnos del colegio San Bartolome La Merced</small> 
        <div class="form-group m-t-20">
            <label for="">Primero ingresa tu generación</label>
            <input class="form-control" name="generacion" type="number" required="" placeholder="(AAAA) Ejemplo 1991">
        </div>
        <div class="form-group m-t-20">
            <label for="">Ingresa tu Nombre</label>
            <input class="form-control" name="nombre" type="text" required="" placeholder="Ingresa tu nombre">
        </div>  
        <div class="form-group m-t-20">
            <label for="">Ahora ingresa tu Apellido</label>
            <input class="form-control" name="apellido" type="text" required="" placeholder="Ingresa tu apellido">
        </div>         
        <div class="form-group text-center m-t-20">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Registrate</button>
        </div>
        <div class="form-group m-b-0">
            <p>¿Ya tienes cuenta? <a href="login" class="text-primary m-l-5"><b>Accede a tu cuenta</b></a></p>
        </div>
    </form>
    </div>
  </div>
</section>

<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

@endsection
@section ('scripts')
<script src="/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
@if (session('mensaje'))

<script type="text/javascript">
   alert("{{ session('titulo') }} {{ session('mensaje') }}");
</script>
@endif
@endsection