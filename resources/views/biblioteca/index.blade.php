@extends('layouts.dashboard')
@section('title')
<title>Cosmos - Listado de bibliotecas</title>
@endsection
@section('css')
<!-- Wizard CSS -->
<link href="/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" >
<link href="/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
@endsection
@section('content')
<div class="container-fluid">
	<!-- /row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Listado de Usuarios Antiguos</h3>
				<p class="text-muted m-b-30">
				<!--button data-toggle="modal" data-target="#crear" class="btn btn-info waves-effect waves-light" type="button"><span class="btn-label"><i class="fa fa-plus"></i></span>Agregar Usuario</button-->
				</p>

				<div class="ui-widget">
					{{$bibliotecas->links()}}
				</div>
				<div class="table-responsive">
					<table id="tabla_bibliotecas" class="table table-striped">
						<thead>
							<tr>
								<th>#</th>
								<th>Nombre</th>
								<th>Segundo Nombre</th>
								<th>Apellido</th>
								<th>Segundo Apellido</th>
								<th>Genero</th>
								<th>Año de Graduacion</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							@foreach($bibliotecas as $index=>$biblioteca)
							<tr>
								<td>{{$index+1}}</td>
								<td>{{$biblioteca->name}}</td>
								<td>{{$biblioteca->sname}}</td>
								
								<td>{{$biblioteca->fapellido}}</td>
								<td>{{$biblioteca->lapellido}}</td>
								<td>{{$biblioteca->genero}}</td>
								<td>{{$biblioteca->ano}}</td>
								<td>
									<a href="edit/{{$biblioteca->id}}" type="button" class="btn btn-danger btn-outline btn-circle btn-lg m-r-5"><i class="ti-pencil"></i>
									</a>
									<a href="view/{{$biblioteca->id}}" title="Ver" type="button" class="btn btn-danger btn-outline btn-circle btn-lg m-r-5"><i class="ti-eye"></i>
									</a>
									<form action="delete" method="post" style="display: inline;">
										<input type="hidden" value="{{$biblioteca->id}}" name="identificador">
										{{ csrf_field() }}
									<button  title="Editar Usuario"  type="submit" class="btn btn-danger btn-outline btn-circle btn-lg m-r-5"><i class="ti-trash"></i>
									</button>
									</form>
								</td>
							</tr>
							@endforeach								
						</tbody>
					</table>
				</div>
				
			</div>
		</div>
	</div>
</div>

<!--Modal Edición Usuario -->
@foreach($bibliotecas as $biblioteca)
<div id="editar-{{ $biblioteca->id }}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Editar biblioteca <b>{{$biblioteca->name}}</b></h4>
			</div>
			<div class="modal-body">
				<form role="form" method="POST" action="{{ url('/bibliotecas/editar-biblioteca') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="recipient-name" class="control-label">Tipo Usuario:</label>						
						<select class="form-control" name="tipo_biblioteca_id">
							
						</select>						
					</div>
					<div class="form-group">
						<label for="recipient-name" class="control-label">Contraseña:</label>
						<input type="password" class="form-control" name="password" value="">
					</div>					
					<div class="clearfix"></div>
					<br><br>
					<label for="" class="control-label">Foto actual</label>
					<div class="clearfix"></div>
					<div class="col-xs-6">
						@if(!is_null($biblioteca->imagen))
							<img width="50px" src="/storage/bibliotecas/{{$biblioteca->imagen}}" class="img-responsive" alt="Foto Perfil">
						@else
							No hay imagen
						@endif	
					</div>
					<div class="clearfix"></div>
					<br><br><br>
					<div class="form-group">
						<label class="col-sm-12">Cambiar imagen</label>
						<input name="foto_perfil" type="file" id="input-file-now" class="dropify" /> 
					</div>
					<input type="text" value="{{$biblioteca->id}}" hidden="" name="user_id">
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-danger waves-effect waves-light">Editar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@endforeach
@endsection
@section('scripts')
<script src="/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
<script>
	$(document).ready(function() {
		$('.dropify').dropify();
		// $('#tabla_bibliotecas').DataTable({
		// 	"language": {
		// 		"url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
		// 	}
		// });
		$(".select2").select2();

		
	});

</script>
@if (session('mensaje'))
<script type="text/javascript">
	swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
</script>
@endif
@endsection