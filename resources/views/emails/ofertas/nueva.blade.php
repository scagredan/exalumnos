@component('mail::message')
# Nueva oferta

Se ha publicado una nueva oferta que puede ser de tu interés:

# {{ $oferta->name }}
#  Para: {{ $oferta->ocupacion->name }}
 {{ $oferta->descripcion }}

@component('mail::button', ['url' => config('app.url')])
Ingresar a la plataforma
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
