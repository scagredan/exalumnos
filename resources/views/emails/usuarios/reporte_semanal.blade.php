@component('mail::message')
# Nuevos usuarios registrados esta semana:

@component('mail::table')
| Nombre       | Email         | Año de graduación  |
| ------------- |:-------------:| --------:|
@foreach($registros as $registro)
| {{ $registro->name . $registro->apellido }}    | {{ $registro->email }}      | {{ $registro->graduacion }}     |
@endforeach
@endcomponent

@component('mail::button', ['url' => config('app.url')])
Ingresar a la plataforma
@endcomponent

Gracias,<br>
{{ config('app.name') }}
@endcomponent
