<?php 
use App\Categoria;

?>

@extends('layouts.dashboard')
@section('title')
<title>Networking Bartolino - Listado de Empresas</title>
@endsection
@section('css')
<!-- Wizard CSS -->
<link href="/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" >
<link href="/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
@endsection
@section('content')

<div class="container-fluid">

	<!-- .row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Editar Empresa</h3>

				<form class="form-horizontal" action="../edit" method="post">
					<input type="hidden" name="identificador" value="{{$empresa->id}}">
					<div class="form-group">
						<label class="col-md-12">ID Usuario para crear empresa</label>
						<div class="col-md-12">
							<input type="number" required="" class="form-control" placeholder="Ingrese el id del usuario"  name="user_id" value="{{$empresa->user_id}}" />
						</div>
					</div>
					<div class="form-group">
						{{ csrf_field() }}
						<label class="col-md-12">Nombre de la Empresa</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese nombre"  name="name" value="{{$empresa->name}}"/>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-md-12">Descripción</label>
						<div class="col-md-12">
							<textarea name="descripcion" id="" class="form-control">{{$empresa->descripcion}}</textarea>
							
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Teléfono</label>
						<div class="col-md-12">
							<input type="tel" required="" class="form-control" placeholder="Ingrese el teléfono"  name="telefono" value="{{$empresa->telefono}}" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Celular</label>
						<div class="col-md-12">
							<input type="tel" required="" class="form-control" placeholder="Ingrese el celular"  name="celular" value="{{$empresa->celular}}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Nit</label>
						<div class="col-md-12">
							<input type="number"  class="form-control" placeholder="Ingrese el Nit"  name="nit" value="{{$empresa->nit}}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Dirección</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese la dirección"  name="direccion" value="{{$empresa->direccion}}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Tipo de Empresa</label>
						<div class="col-md-12">
							<input type="text"  class="form-control" placeholder="Ingrese el tipo de empresa"  name="tipo_empresa" value="{{$empresa->tipo_empresa}}"/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Correo electrónico</label>
						<div class="col-md-12">
							<input type="email" required="" class="form-control" placeholder="Ingrese el correo electrónico"  name="correo" value="{{$empresa->correo}}"/>
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-md-12">Categoria</label>
						<div class="col-md-12">
							<select name="categoria" id="" class="form-control">
								<option value="" disabled="" selected="">Selecciona la categoria</option>
								@foreach(Categoria::all() as $categoria)
								@if($empresa->categoria == $categoria->id)
								<option value="{{$categoria->id}}" selected="">{{$categoria->name}}</option>
								@else
								<option value="{{$categoria->id}}">{{$categoria->name}}</option>
								@endif
								@endforeach
							</select>
							
						</div>
					</div>
					<div class="form-group">
						<label for="" class="col-md-12">Activo</label>
						<div class="col-md-12">
							<select name="activo" id="" class="form-control">
								@if($empresa->active == 1)
								<option value="1" selected="">Si</option>
								<option value="0">No</option>	
								@else
								<option value="1" >Si</option>
								<option value="0" selected="">No</option>
								@endif
							</select>
							
						</div>
						

					</div>

					


					<button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Guardar</button>

				</form>
			</div>
		</div>
	</div>
</div>

<!-- /.row -->

</div>
@endsection
@section('scripts')
<script src="/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
<script>
	$(document).ready(function() {
		$('.dropify').dropify();
        // $('#tabla_ofertas').DataTable({
        //  "language": {
        //      "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
        //  }
        // });
        $(".select2").select2();

        
    });

</script>
@if (session('mensaje'))
<script type="text/javascript">
	swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
</script>
@endif
@endsection