@extends('layouts.dashboard')
@section('title')
<title>Cosmos - Agregar Registro</title>
@endsection
@section('css')
<!-- Wizard CSS -->
<link href="../plugins/bower_components/jquery-wizard-master/css/wizard.css" rel="stylesheet">
<link href="../plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="../plugins/bower_components/jquery-wizard-master/libs/formvalidation/formValidation.min.css" rel="stylesheet" >
<link href="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Agregar Funcionario</h4> 
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- .row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box panel panel-default block3">
				<h3 class="box-title m-b-0">Formulario de registro</h3>
				<p class="text-muted m-b-30 font-13"> Completa totalmente el formulario para agregar el funcionario.</p>
				<div id="div_wizard" class="wizard">
					<ul class="wizard-steps" role="tablist">
						<li class="active" role="tab">
							<h4><span><i class="ti-user"></i></span>Información Personal</h4>
						</li>
						<li role="tab">
							<h4><span><i class="ti-credit-card"></i></span>Información Laboral</h4>
						</li>
						<li role="tab">
							<h4><span><i class="ti-check"></i></span>Administrativo</h4>
						</li>
					</ul>
					<form id="formulario_registro" class="form-horizontal" method="post" action="{{url('post-registro')}}">
						{{ csrf_field() }}
						<div class="wizard-content">
							<div class="wizard-pane active" role="tabpanel">
								<small>Datos Básicos</small>
								<div class="form-group">
									<label class="col-xs-3 control-label">Nombres</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control" name="nombres" required/> 
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Apellidos</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control" name="apellidos" required/> 
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Tipo de documento</label>
									<div class="col-xs-5 input-group">
										<select name="tipo_documento"  required class="form-control">
											<option value="" disabled="" selected="">Selecciona el tipo de documento</option>
											@foreach($tipos_documento as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Número de documento</label>
									<div class="col-xs-5 input-group">
										<input type="number" class="form-control" name="numero_documento" onchange="verificarExistencia(this.value)" required/> 
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Fecha de nacimiento</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control datepicker-autoclose" name="fecha_nacimiento" placeholder="yyyy-mm-dd" required/> 
										<span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Fecha de expedición documento</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control datepicker-autoclose" name="fecha_expedicion_documento" placeholder="yyyy-mm-dd" required/>
										<span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Lugar de expedición documento</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control" name="lugar_expedicion_documento" required/> 
									</div>
								</div>
								<hr class="m-t-40">
								<small>Datos Demográficos</small>
								<div class="form-group">
									<label class="col-xs-3 control-label">Nacionalidad</label>
									<div class="col-xs-5 input-group">
										<select name="nacionalidad" id="nacionalidad" required class="form-control">
											<option value="" disabled="" selected="">Selecciona la nacionalidad</option>
											@foreach($paises as $item)
											<option value="{{$item->id}}">{{$item->nombre}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Género</label>
									<div class="col-xs-5 input-group">
										<select name="genero"  required = "" class="form-control">
											<option value="" disabled="" selected="">Selecciona el género</option>
											<option value="Masculino">Masculino</option>
											<option value="Femenino">Femenino</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Raza</label>
									<div class="col-xs-5 input-group">
										<select name="raza"  required class="form-control">
											<option value="" disabled="" selected="">Selecciona la raza</option>
											@foreach($razas as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Correo Electrónico</label>
									<div class="col-xs-5 input-group">
										<input type="email" class="form-control" name="correo_electronico" required />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Ciudad residencia</label>
									<div class="col-xs-5 input-group">
										<select name="ciudad"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona la ciudad</option>
											@foreach($ciudades as $item)
											<option value="{{$item->id}}">{{$item->nombre}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Dirección de residencia</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control" name="direccion_residencia" required />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Barrio residencia</label>
									<div class="col-xs-5 input-group">
										<select name="barrio_residencia" id="barrios" required class="form-control">
											<option value="" disabled="" selected="" >Selecciona el barrio</option>
											@foreach($barrios as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Estrato</label>
									<div class="col-xs-5 input-group">
										<select name="estrato"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona el estrato</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Teléfono Fijo</label>
									<div class="col-xs-5 input-group">
										<input type="phone" class="form-control" name="telefono_fijo" required />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Celular</label>
									<div class="col-xs-5 input-group">
										<input type="phone" class="form-control" name="celular" required />
									</div>
								</div>
								<hr class="m-t-40">
								<small>Datos Familiares</small>
								<div class="form-group">
									<label class="col-xs-3 control-label">Contacto de Emergencia</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control" name="contacto_emergencia" required />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Teléfono Contacto Emergencia</label>
									<div class="col-xs-5 input-group">
										<input type="phone" class="form-control" name="telefono_contacto_emergencia" required />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Estado Civil</label>
									<div class="col-xs-5 input-group">
										<select name="estado_civil"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona el estado</option>
											@foreach($estados_civiles as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Nombre de la pareja</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control" name="nombre_pareja" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Número de hijos</label>
									<div class="col-xs-5 input-group">
										<input id="numero_hijos_id" type="number" class="form-control" name="numero_hijos" />
									</div>
								</div>
								<div id="div-hijos">
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">¿Tiene Subsidio?</label>
									<div class="col-xs-5 input-group">
										<select name="subsidio" class="form-control" required>
											<option value="" disabled="" selected="" >Selecciona la respuesta</option>
											<option value="Si">Si</option>
											<option value="No">No</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Personas a Cargo</label>
									<div class="col-xs-5 input-group">
										<input type="number" class="form-control" name="personas_cargo" required />
									</div>
								</div>
							</div>
							<!-- Laboral -->
							<div class="wizard-pane" role="tabpanel">
								<div class="form-group">
									<label class="col-xs-3 control-label">Nivel Educativo</label>
									<div class="col-xs-5 input-group">
										<select name="nivel_educativo"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona el nivel</option>
											@foreach($niveles_educativos as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Cargo</label>
									<div class="col-xs-5 input-group">
										<select name="cargo"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona el cargo</option>
											@foreach($cargos as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Turno de trabajo</label>
									<div class="col-xs-5 input-group">
										<select name="turno_trabajo"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona el turno</option>
											@foreach($turnos as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Fecha de ingreso</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control datepicker-autoclose" name="fecha_ingreso" placeholder="yyyy-mm-dd"/> 
										<span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">EPS</label>
									<div class="col-xs-5 input-group">
										<select name="eps" id="eps" required class="form-control">
											<option value="" disabled="" selected="" >Selecciona la EPS</option>
											@foreach($eps as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">AFP</label>
									<div class="col-xs-5 input-group">
										<select name="afp"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona la AFP</option>
											@foreach($afps as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Caja de compensación</label>
									<div class="col-xs-5 input-group">
										<select name="caja_compensacion"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona la entidad</option>
											@foreach($cajas_compensacion as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">ARL</label>
									<div class="col-xs-5 input-group">
										<select name="arl" class="form-control" required>
											<option value="" disabled="" selected="" >Selecciona la ARL</option>
											@foreach($arls as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Fondo de cesantias</label>
									<div class="col-xs-5 input-group">
										<select name="fondo_cesantias" class="form-control" required>
											<option value="" disabled selected>Selecciona el fondo</option>
											@foreach($fondos_cesantias as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Número de contratos con la empresa</label>
									<div class="col-xs-5 input-group">
										<input type="number" class="form-control" name="numero_contratos" required />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Tipo Contrato</label>
									<div class="col-xs-5 input-group">
										<select name="tipo_contrato"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona el tipo contrato</option>
											@foreach($tipos_contrato as $item)
												<option value="{{$item->id}}">{{$item->nombre}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Condición de Contrato</label>
									<div class="col-xs-5 input-group">
										<select name="condicion"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona la condición de contrato</option>
											@foreach($condiciones as $item)
												<option value="{{$item->id}}">{{$item->nombre}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Código Contable</label>
									<div class="col-xs-5 input-group">
										<select name="codigo_contable" required class="form-control">
											<option value="" disabled="" selected="" >Selecciona el código contable</option>
											@foreach($codigos_contables as $item)
												<option value="{{$item->id}}">{{$item->nombre}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div id="salario_div">
									
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Tipo de la cuenta</label>
									<div class="col-xs-5 input-group">
										<select name="tipo_cuenta"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona el tipo</option>
											@foreach($tipos_cuenta as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Número de cuenta</label>
									<div class="col-xs-5 input-group">
										<input type="number" class="form-control" name="numero_cuenta" required />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Banco</label>
									<div class="col-xs-5 input-group">
										<select name="banco"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona el banco</option>
											@foreach($bancos as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<!-- Administrativo -->
							<div class="wizard-pane" role="tabpanel">
								<div class="form-group">
									<label class="col-xs-3 control-label">Tipo funcionario</label>
									<div class="col-xs-5 input-group">
										<select name="tipo_funcionario"  required class="form-control" id="tipo_funcionario_id">
											<option value="" disabled="" selected="" >Selecciona el funcionario</option>
											@foreach($tipos_funcionarios as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div id="div-docente" style="display: none">
									<div class="form-group">
										<label class="col-xs-3 control-label">¿Aplica Visa?</label>
										<div class="col-xs-5 input-group">
											<select id="visa_trigger" name="visa_seleccion"  required class="form-control">
												<option value="" disabled="" selected="" >Selecciona si aplica</option>
												<option value="1">Si</option>
												<option value="0">No</option>
											</select>
										</div>
									</div>
									<div id="visa_div" style="display: none">
										<div class="form-group">
											<label class="col-xs-3 control-label">Tipo de Visa</label>
											<div class="col-xs-5 input-group">
												<select name="visa" class="form-control">
													<option value="" disabled="" selected>Selecciona tipo de visa</option>
													@foreach($visas as $item)
														<option value="{{$item->id}}">{{$item->name}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-3 control-label">Fecha vigencia Visa</label>
											<div class="col-xs-5 input-group">
												<input type="text" class="form-control datepicker-autoclose" name="fecha_visa" placeholder="yyyy-mm-dd" />
												<span class="input-group-addon"><i class="icon-calender"></i></span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Escalafón</label>
										<div class="col-xs-5 input-group">
											<select name="escalafon" class="form-control" required>
												<option value="" disabled="" selected>Selecciona el escalafon</option>
												@foreach($escalafones as $item)
												<option value="{{$item->id}}">{{$item->name}}</option>
												@endforeach 
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Registro SIRE</label>
										<div class="col-xs-5 input-group">
											<input type="text" class="form-control" name="registro_sire" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Observación compromiso</label>
										<div class="col-xs-5 input-group">
											<textarea name="observacion_compromiso"  cols="30" rows="10" class="form-control"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Fecha Compromiso</label>
										<div class="col-xs-5 input-group">
											<input type="text" class="form-control datepicker-autoclose" name="fecha_compromiso" placeholder="yyyy-mm-dd"/>
											<span class="input-group-addon"><i class="icon-calender"></i></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Fecha límite cumplimiento compromiso</label>
										<div class="col-xs-5 input-group">
											<input type="text" class="form-control datepicker-autoclose" name="fecha_limite_compromiso" placeholder="yyyy-mm-dd"/>
											<span class="input-group-addon"><i class="icon-calender"></i></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Certificado nivel idioma</label>
										<div class="col-xs-5 input-group">
											<input type="text" class="form-control" name="certificado_nivel_idioma" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Examen pago por ASW</label>
										<div class="col-xs-5 input-group">
											<select name="examen_pago" class="form-control" required>
												<option value="" disabled="" selected>Selecciona si tiene Examen pago</option>
												<option value="Si">Si</option>
												<option value="No">No</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Observación</label>
										<div class="col-xs-5 input-group">
											<textarea name="observacion_docente"  cols="30" rows="10" class="form-control"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Firma anexo al contrato de certificaciones laborales</label>
										<div class="col-xs-5 input-group">
											<select name="firma_anexo" class="form-control" required>
												<option value="" disabled="" selected>Selecciona si firma</option>
												<option value="Si">Si</option>
												<option value="No">No</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Cumple el perfil</label>
										<div class="col-xs-5 input-group">
											<select name="cumple_perfil" class="form-control" required>
												<option value="" disabled="" selected>Selecciona si cumple</option>
												<option value="Si">Si</option>
												<option value="No">No</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Pruebas internas docentes</label>
										<div class="col-xs-5 input-group">
											<select name="pruebas_docentes" class="form-control" required>
												<option value="" disabled="" selected>Selecciona la prueba</option> @foreach($pruebas_docentes as $item)
												<option value="{{$item->id}}">{{$item->name}}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Sede</label>
									<div class="col-xs-5 input-group">
										<select name="sede"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona la sede</option>
											@foreach($sedes as $item)
											<option value="{{$item->id}}">{{$item->nombre}}</option>
											@endforeach
										</select>
									</div>
								</div>
								 <div class="form-group">
									<label class="col-xs-3 control-label">¿Aplica Dotación?</label>
									<div class="col-xs-5 input-group">
										<select id="dotacion_trigger" name="dotacion_seleccion" required class="form-control">
											<option value="" disabled="" selected="" >Selecciona si aplica</option>
											<option value="1">Si</option>
											<option value="0">No</option>
										</select>
									</div>
								</div>
								<div class="form-group" id="dotacion_div">
								
								</div>
								<div class="form-group" id="entrega_dotacion_div">
								
								</div>							
								<div class="form-group">
									<label class="col-xs-3 control-label">Idioma</label>
									<div class="col-xs-5 input-group">
										<select name="idioma"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona el idioma</option>
											@foreach($idiomas as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Nivel Idioma</label>
									<div class="col-xs-5 input-group">
										<select name="nivel_idioma"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona el nivel</option>
											@foreach($niveles as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Fecha terminación periodo de prueba</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control datepicker-autoclose" name="fecha_terminacion" placeholder="yyyy-mm-dd"/>
										<span class="input-group-addon"><i class="icon-calender"></i></span> 
									</div>
								</div>
								 <div class="form-group">
									<label class="col-xs-3 control-label">Tiene Carné</label>
									<div class="col-xs-5 input-group">
										<select name="tiene_carne"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona si tiene Carné</option>
											<option value="Si">Si</option>
											<option value="No">No</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Estado Beca</label>
									<div class="col-xs-5 input-group">
										<select name="estado_beca"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona el estado</option>
											<option value="Activo">Activo</option>
											<option value="Inactivo">Inactivo</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Fecha Beca</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control datepicker-autoclose" name="fecha_beca" placeholder="yyyy-mm-dd"/>
										<span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Idioma de la Beca</label>
									<div class="col-xs-5 input-group">
										<select name="idioma_beca"  required class="form-control">
											<option value="" disabled="" selected="" >Selecciona el idioma</option>
											@foreach($idiomas as $item)
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">¿Retirado?</label>
									<div class="col-xs-5 input-group">
										<select id="retirado_trigger" name="retirado" class="form-control" required>
											<option value="" disabled="" selected=>Selecciona si está retirado</option>
											<option value="Si">Si</option>
											<option value="No">No</option>
										</select>
									</div>
								</div>
								<div id="div_retirado" style="display: none">
									<div class="form-group">
										<label class="col-xs-3 control-label">Motivo del retiro</label>
										<div class="col-xs-5 input-group">
											<textarea name="motivo_retiro" cols="30" rows="10" class="form-control"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Fecha del retiro</label>
										<div class="col-xs-5 input-group">
											<input type="text" class="form-control datepicker-autoclose" name="fecha_retiro" placeholder="yyyy-mm-dd"/>
											<span class="input-group-addon"><i class="icon-calender"></i></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">¿Bloqueado?</label>
										<div class="col-xs-5 input-group">
											<select name="bloqueado" class="form-control">
												<option value="" disabled="" selected=>Selecciona si está bloqueado</option>
												<option value="Si">Si</option>
												<option value="No">No</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
<!-- /.row -->
@endsection
@section('scripts')

<!-- Form Wizard JavaScript -->
<script src="../plugins/bower_components/jquery-wizard-master/dist/jquery-wizard.min.js"></script>
<!-- FormValidation plugin and the class supports validating Bootstrap form -->
<script src="../plugins/bower_components/jquery-wizard-master/libs/formvalidation/formValidation.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.formvalidation/0.6.1/js/language/es_ES.js"></script>
<script src="../plugins/bower_components/jquery-wizard-master/libs/formvalidation/bootstrap.min.js"></script>
<script src="../plugins/bower_components/blockUI/jquery.blockUI.js"></script>
<!-- Sweet-Alert  -->
<script src="../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="../plugins/bower_components/bootstrap-datepicker/bootstrap-datetimepicker.es.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<!-- Custom Theme JavaScript -->
<script type="text/javascript">
	$(document).ready(function() {
		$("input[type='phone']").keyup(function() {
			$(this).val($(this).val().replace(/[^\d]/g,''));
		});
		$("#nacionalidad").select2({
			placeholder: "Selecciona un país",
			allowClear: true
		});
		$("#barrios").select2({
			placeholder: "Selecciona un barrio",
			allowClear: true
		});
		$("#eps").select2({
			placeholder: "Selecciona una EPS",
			allowClear: true
		});
		$(window).keydown(function(event){
			if(event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});
	});
	(function() {			
		$('#div_wizard').wizard({
			onInit: function() {
				$('#formulario_registro').formValidation({
					locale: 'es_ES',
					framework: 'bootstrap',
				});
			},
			validator: function() {
				var fv = $('#formulario_registro').data('formValidation');
				var $this = $(this);
				fv.validateContainer($this);
				var isValidStep = fv.isValidContainer($this);
				if (isValidStep === false || isValidStep === null) {
					return false;
				}
				return true;
			},
			onFinish: function() {
				var dataForm = $('#formulario_registro').serialize();
				$.ajax({
					url: '{{route("post-funcionario")}}',
					type: 'POST',			
					datatype: 'JSON',
					data: dataForm,
					beforeSend: function (){
						$('div.block3').block({
							message: '<h3><img src="../plugins/images/busy.gif"/> Guardando Información...</h3>',
							centerY: false,
							css: {
								border: '1px solid #fff',
								top: '920px'
							}
						});
					},
					success: function(data){
						if(data.success){
							$('div.block3').unblock();
							$('#formulario_registro')[0].reset();
							swal("Perfecto!", "La información se ha registrado con éxito!", "success");
						}				
						else{
							$('div.block3').unblock();
							swal("Ups!", "Hubo un error al registrar la información por favor verifica los datos!", "error");
						}
					},
					error:function(){
						$('div.block3').unblock();
						swal("Ups!", "Hubo un error al registrar la información por favor verifica los datos!", "error");
					}
				});
			}
		});

	})();
</script>

<script>
$('#dotacion_trigger').change(function(){
	if($('#dotacion_trigger').val() == 1){
		$('#dotacion_div').empty();
		$('#dotacion_div').append('<label class="col-xs-3 control-label">Dotación</label> <div class="col-xs-5 input-group"> <select name="dotacion"  required class="form-control"> <option value="" disabled="" selected="" >Selecciona la dotación</option> @foreach($dotaciones as $item) @if($item->id != 0) <option value="{{$item->id}}">{{$item->name}}</option> @endif @endforeach </select> </div> <div class="clearfix"></div><br><br> <label class="col-xs-3 control-label">Talla</label> <div class="col-xs-5 input-group"> <select name="talla"  required class="form-control"> <option value="" disabled="" selected="" >Selecciona la talla</option> @foreach($tallas as $item) <option value="{{$item->id}}">{{$item->name}}</option> @endforeach </select> </div>');
		$('#entrega_dotacion_div').empty();
		$('#entrega_dotacion_div').append('<label class="col-xs-3 control-label">¿Dotación Entregada?</label><div class="col-xs-5 input-group"><select  name="entrega_dotacion"  required class="form-control"><option value="" disabled="" selected="" >Selecciona una opción</option><option value="Si">Si</option><option value="No">No</option></select></div>');
	}else{
		$('#dotacion_div').empty();
		$('#entrega_dotacion_div').empty();
	}
});

$('#numero_hijos_id').change(function(){
	var count = $('#numero_hijos_id').val();
	$('#div-hijos').empty();
	for (var i = 1; i <= count; i++) {
		$('#div-hijos').append('<div class="form-group"><label class="col-xs-3 control-label">Género Hijo '+i+'</label><div class="col-xs-5 input-group"><select name="hijos_genero[]" required class="form-control"><option value="" disabled selected>Selecciona el género</option><option value="Masculino">Masculino</option><option value="Femenino">Femenino</option></select></div></div><div class="form-group"><label class="col-xs-3 control-label">Fecha de nacimiento hijo '+i+'</label><div class="col-xs-5 input-group"><input type="text" class="form-control datepicker-autoclose-hijos" name="hijos_nacimiento[]" placeholder="yyyy-mm-dd" required/><span class="input-group-addon"><i class="icon-calender"></i></span></div></div><hr></hr>');
	}
	$('.datepicker-autoclose-hijos').datepicker({
		autoclose: true,
		todayHighlight: true,
		format: 'yyyy-mm-dd',
		language: 'es'
	});
});

$("select[name='codigo_contable']").change(function(){
	var codigo_contable_id = $(this).val();
	var token = $("input[name='_token']").val();
	$.ajax({
		url: '{{route("consultar-salarios")}}',
		method: 'POST',
		data: {codigo_contable_id:codigo_contable_id, _token:token},
		success: function(data) {
			$("#salario_div").html('');
			$("#salario_div").html(data.options);
		}
	});
});

$('#tipo_funcionario_id').change(function(){
	if ($('#tipo_funcionario_id').val() == '3')
		$('#div-docente').show();
	else
		$('#div-docente').hide();
});
$('#visa_trigger').change(function(){
	if($('#visa_trigger').val() == 1)
		$('#visa_div').show();
	else
		$('#visa_div').hide();
});
$('#retirado_trigger').change(function(){
	if($('#retirado_trigger').val()=="Si")
		$('#div_retirado').show();
	else
		$('#div_retirado').hide();
});
$('.datepicker-autoclose').datepicker({
    autoclose: true,
    todayHighlight: true,
    format: 'yyyy-mm-dd',
    language: 'es'
});
function verificarExistencia($numero_documento){
	var tipo_documento_id = $("select[name='tipo_documento']").val();
	var token = $("input[name='_token']").val();
	$.ajax({
		url: '{{route("consultar-existencia")}}',
		method: 'POST',
		data: {numero_documento:$numero_documento, tipo_documento_id:tipo_documento_id, _token:token},
		beforeSend: function (){
			$('div.block3').block({
				message: '<h3><img src="../plugins/images/busy.gif"/> Verificando Número Documento...</h3>',
				centerY: false,
				css: {
					border: '1px solid #fff',
             		top: '500px'
				}
			});
		},
		success: function(data) {
			$('div.block3').unblock();
			if(data.existe){
				swal("Atención!", "Este número de documento ya existe!", "error");
				$("input[name='numero_documento']").val('');
			}
		}
	});
}
</script>
@endsection