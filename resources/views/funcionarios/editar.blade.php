@extends('layouts.dashboard')
@section('title')
<title>Cosmos - Agregar Registro</title>
@endsection
@section('css')
<!-- Wizard CSS -->
<link href="{{ URL::asset('plugins/bower_components/jquery-wizard-master/css/wizard.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('plugins/bower_components/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
<link href="{{ URL::asset('plugins/bower_components/jquery-wizard-master/libs/formvalidation/formValidation.min.css') }}" rel="stylesheet" >
<link href="{{ URL::asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<?php   
use App\User;
use App\Salario;

    const ADMINISTRADOR_TIPO = 1;
    const USUARIO_TIPO = 0;
    const USUARIO = 1;
    const USUARIO_IMG = 2;
    const ADMIN = 3;
    const ADMIN_IMG = 4;
 ?>
<div class="container-fluid">
	<div class="row bg-title">
		<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
			<h4 class="page-title">Editar Funcionario</h4> 
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- .row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box panel panel-default block3">
				<h3 class="box-title m-b-0">Formulario de registro</h3>
				<p class="text-muted m-b-30 font-13"> Completa totalmente el formulario para agregar el funcionario.</p>
				<div id="div_wizard" class="wizard">
					<ul class="wizard-steps" role="tablist">
						<li class="active" role="tab">
							<h4><span><i class="ti-user"></i></span>Información Personal</h4>
						</li>
						<li role="tab">
							<h4><span><i class="ti-credit-card"></i></span>Información Laboral</h4>
						</li>
						<li role="tab">
							<h4><span><i class="ti-check"></i></span>Administrativo</h4>
						</li>
					</ul>
					<form id="formulario_registro" class="form-horizontal" method="post" action="{{url('post-funcionario')}}">
						{{ csrf_field() }}
						<input type="text" hidden="" value="{{$registro->id}}" name="identificador">
						<div class="wizard-content">
							<div class="wizard-pane active" role="tabpanel">
								<small>Datos Básicos</small>
								<div class="form-group">
									<label class="col-xs-3 control-label">Nombres</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control" value="{{$registro->nombres}}" name="nombres" required/> 
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Apellidos</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control" value="{{$registro->apellidos}}" name="apellidos" required/> 
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Tipo de documento</label>
									<div class="col-xs-5 input-group">
										<select name="tipo_documento" id="" required class="form-control">
											
											@foreach($tipos_documento as $item)
											@if($item->id==$registro->tipo_documento_id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Número de documento</label>
									<div class="col-xs-5 input-group">
										<input type="number" value="{{$registro->numero_documento}}" class="form-control" name="numero_documento" onchange="verificarExistencia(this.value)" required/> 
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Fecha de nacimiento</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control datepicker-autoclose" value="{{$registro->fecha_nacimiento}}" name="fecha_nacimiento" required />
										<span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Fecha de expedición documento</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control datepicker-autoclose" name="fecha_expedicion_documento" value="{{$registro->fecha_expedicion}}" placeholder="yyyy-mm-dd" required/>
										<span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Lugar de expedición documento</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control" name="lugar_expedicion_documento" value="{{$registro->lugar_expedicion}}" required/> 
									</div>
								</div>
								<hr class="m-t-40">
								<small>Datos Demográficos</small>
								<div class="form-group">
									<label class="col-xs-3 control-label">Nacionalidad</label>
									<div class="col-xs-5 input-group">
										<select name="nacionalidad" id="nacionalidad" required class="form-control">
											
											@foreach($paises as $item)
											@if($registro->nacionalidad==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->nombre}}</option>		
											@else
											<option value="{{$item->id}}">{{$item->nombre}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Género</label>
									<div class="col-xs-5 input-group">
										<select name="genero" id="" required = "" class="form-control">
											@if($registro->genero=="MASCULINO" || $registro->genero=="Masculino")
											<option value="Masculino" selected="">Masculino</option>
											<option value="Femenino">Femenino</option>
											@endif
											@if($registro->genero == "Femenino" ||$registro->genero=="Femenino")
											<option value="Masculino">Masculino</option>
											<option value="Femenino" selected="">Femenino</option>
											@endif
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Raza</label>
									<div class="col-xs-5 input-group">
										<select name="raza" id="" required class="form-control">
											
											@foreach($razas as $item)
											@if($registro->raza_id==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Correo Electrónico</label>
									<div class="col-xs-5 input-group">
										<input type="email" value="{{$registro->email}}" class="form-control" name="correo_electronico" required />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Ciudad residencia</label>
									<div class="col-xs-5 input-group">
										<select name="ciudad" id="" required class="form-control">
											@foreach($ciudades as $item)
											@if($registro->ciudades_id == $item->id)
											<option selected="" value="{{$item->id}}">{{$item->nombre}}</option>
											@else
											<option value="{{$item->id}}">{{$item->nombre}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Dirección de residencia</label>
									<div class="col-xs-5 input-group">
										<input type="text" value="{{$registro->dirreccion}}" class="form-control" name="direccion_residencia" required />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Barrio residencia</label>
									<div class="col-xs-5 input-group">
										<select name="barrio_residencia" id="barrios" required class="form-control">
											@foreach($barrios as $item)
											@if($registro->barrios_id==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Estrato</label>
									<div class="col-xs-5 input-group">
										<select name="estrato" id="" required class="form-control">
											@if($registro->estrato=="1")
											<option selected="" value="1">1</option>
											@else
											<option value="1">1</option>
											@endif
											@if($registro->estrato=="2")
											<option selected="" value="2">2</option>
											@else
											<option value="2">2</option>
											@endif
											@if($registro->estrato=="3")
											<option selected="" value="3">3</option>
											@else
											<option value="3">3</option>
											@endif
											@if($registro->estrato=="4")
											<option selected="" value="1">4</option>
											@else
											<option value="4">4</option>
											@endif
											@if($registro->estrato=="5")
											<option selected="" value="5">5</option>
											@else
											<option value="5">5</option>
											@endif
											@if($registro->estrato=="6")
											<option selected="" value="6">6</option>
											@else
											<option value="6">6</option>
											@endif
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Teléfono Fijo</label>
									<div class="col-xs-5 input-group">
										<input type="phone" value="{{$registro->telefono_fijo}}" class="form-control" name="telefono_fijo" required />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Celular</label>
									<div class="col-xs-5 input-group">
										<input type="phone" value="{{$registro->celular}}" class="form-control" name="celular" required />
									</div>
								</div>
								<hr class="m-t-40">
								<small>Datos Familiares</small>
								<div class="form-group">
									<label class="col-xs-3 control-label">Contacto de Emergencia</label>
									<div class="col-xs-5 input-group">
										<input type="text" value="{{$registro->contacto_emergencia}}" class="form-control" name="contacto_emergencia" required />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Teléfono Contacto Emergencia</label>
									<div class="col-xs-5 input-group">
										<input type="phone" value="{{$registro->telefono_emergencia}}" class="form-control" name="telefono_contacto_emergencia" required />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Estado Civil</label>
									<div class="col-xs-5 input-group">
										<select name="estado_civil" id="" required class="form-control">
											@foreach($estados_civiles as $item)
											@if($registro->estado_civil_id == $item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Nombre de la pareja</label>
									<div class="col-xs-5 input-group">
										<input type="text" value="{{$registro->nombre_pareja}}" class="form-control" name="nombre_pareja" />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Número de hijos</label>
									<div class="col-xs-5 input-group">
										<input id="numero_hijos_id" value="{{$registro->numero_hijos}}" type="number" class="form-control" name="numero_hijos" />
									</div>
								</div>
								@if($registro->numero_hijos > 0)
									<div class="form-group" id="div-hijos">
										@foreach($registro->hijos as $index=>$hijo)
										<div class="form-group">
										<label class="col-xs-3 control-label">Género Hijo {{$index+1}}</label>
										<div class="col-xs-5 input-group">
											<select name="hijos_genero[]" required class="form-control">
												<option value="" disabled="" selected="" >Selecciona el género</option>
												@if($hijo->genero == 'Masculino')
												<option value="{{$hijo->genero}}" selected>{{$hijo->genero}}</option>
												<option value="Femenino">Femenino</option>
												@elseif($hijo->genero == 'Femenino')
												<option value="{{$hijo->genero}}" selected>{{$hijo->genero}}</option>
												<option value="Masculino">Masculino</option>
												@endif
											</select>
										</div>
										</div>
										<div class="form-group">
											<label class="col-xs-3 control-label">Fecha de nacimiento hijo {{$index+1}}</label>
											<div class="col-xs-5 input-group">
												<input type="text" class="form-control datepicker-autoclose" name="hijos_nacimiento[]" placeholder="yyyy-mm-dd" value="{{$hijo->fecha_nacimiento}}" required/>
												<span class="input-group-addon"><i class="icon-calender"></i></span>
											</div>
										</div>
										<hr></hr>
										@endforeach
									</div>
								@else
									<div id="div-hijos">
									</div>
								@endif
								<div class="form-group">
									<label class="col-xs-3 control-label">Tiene Subsidio</label>
									<div class="col-xs-5 input-group">
										<select name="subsidio" class="form-control" required>
											@if($registro->subsidio ==1)
											<option selected="" value="1">Si</option>
											<option value="0">No</option>
											@else
											<option value="1">Si</option>
											<option selected="" value="0">No</option>
											@endif
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Personas a Cargo</label>
									<div class="col-xs-5 input-group">
										<input type="number" value="{{$registro->personas_cargo}}" class="form-control" name="personas_cargo" required />
									</div>
								</div>
							</div>
							<!-- Laboral -->
							<div class="wizard-pane" role="tabpanel">
								<div class="form-group">
									<label class="col-xs-3 control-label">Nivel Educativo</label>
									<div class="col-xs-5 input-group">
										<select name="nivel_educativo" class="form-control" required>
											@foreach($niveles_educativos as $item)
											@if($registro->nivel_educativo_id == $item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Cargo</label>
									<div class="col-xs-5 input-group">
										<select name="cargo" class="form-control" required>
											
											@foreach($cargos as $item)
											@if($registro->cargos_id == $item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Turno de trabajo</label>
									<div class="col-xs-5 input-group">
										<select name="turno_trabajo" id="" required class="form-control">
											
											@foreach($turnos as $item)
											@if($registro->turnos_id ==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Fecha de ingreso</label>
									<div class="col-xs-5 input-group">
										<input type="text" class="form-control datepicker-autoclose" value="{{$registro->fecha_ingreso}}" name="fecha_ingreso" />
										<span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">EPS</label>
									<div class="col-xs-5 input-group">
										<select name="eps" id="eps" class="form-control" required>
											@foreach($eps as $item)
											@if($registro->eps_id ==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">AFP</label>
									<div class="col-xs-5 input-group">
										<select name="afp" class="form-control" required>
											
											@foreach($afps as $item)
											@if($registro->afps_id ==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Caja de compensación</label>
									<div class="col-xs-5 input-group">
										<select name="caja_compensacion" class="form-control" required>
											
											@foreach($cajas_compensacion as $item)
											@if($registro->cajas_compensacion_id ==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">ARL</label>
									<div class="col-xs-5 input-group">
										<select name="arl" class="form-control" required>
											@foreach($arls as $item)
											@if($registro->arls_id ==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Fondo de cesantias</label>
									<div class="col-xs-5 input-group">
										<select name="fondo_cesantias" class="form-control" required>
											
											@foreach($fondos_cesantias as $item)
											@if($registro->fondos_cesantias_id ==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Número de contratos con la empresa</label>
									<div class="col-xs-5 input-group">
										<input type="number" value="{{$registro->numero_contratos_empresa}}" class="form-control" name="numero_contratos" required />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Tipo Contrato</label>
									<div class="col-xs-5 input-group">
										<select name="tipo_contrato"  required class="form-control">
											@foreach($tipos_contrato as $item)
											@if($registro->tipo_contrato_id ==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->nombre}}</option>
											@else
											<option value="{{$item->id}}">{{$item->nombre}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Condición de Contrato</label>
									<div class="col-xs-5 input-group">
										<select name="condicion"  required class="form-control">
											<option value="">Selecciona la condición del contrato</option>
											@foreach($condiciones as $item)
											@if($registro->id_condicion ==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->nombre}}</option>
											@else
											<option value="{{$item->id}}">{{$item->nombre}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<?php 
								$salario = Salario::find($registro->salario_id);
								if (count($salario->codigos->get()) > 1) {
									$hidden = false;
								}
								else{
									$hidden = true;
								}
								$codigos_nuevos = $salario->codigos;
								?>
								<?php
								$auxiliar = Salario::find($registro->salario_id)->codigo_contable_id;
								?>
								<div class="form-group">
									<label class="col-xs-3 control-label">Código Contable</label>
									<div class="col-xs-5 input-group">
										<select name="codigo_contable" required class="form-control">
											@foreach($codigos_nuevos->get() as $item)
												@if($auxiliar == $item->id)
											<option selected="" value="{{$item->id}}">{{$item->nombre}}</option>
											@else
											<option value="{{$item->id}}">{{$item->nombre}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div id="salario_div">
								@if($hidden)
									<input hidden="" type="text" name="salario" value="{{$salario->id}}">
								@else
								<div  class="form-group">
									<label class="col-xs-3 control-label">Tipo Salario</label>
									<div class="col-xs-5 input-group">
									<?php 	 ?>
										<select name="salario" id="" required class="form-control">
											@foreach(Salario::where('codigo_contable_id', '=', Salario::find($registro->salario_id)->codigo_contable_id)->get() as $item)
											@if($salario->id ==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->periodo}}</option>
											@else
											<option value="{{$item->id}}">{{$item->periodo}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								@endif									
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Tipo de la cuenta</label>
									<div class="col-xs-5 input-group">
										<select name="tipo_cuenta" id="" required class="form-control">
											@foreach($tipos_cuenta as $item)
											@if($registro->tipos_cuenta_id ==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Número de cuenta</label>
									<div class="col-xs-5 input-group">
										<input type="number" value="{{$registro->numero_cuenta}}" class="form-control" name="numero_cuenta" required />
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Banco</label>
									<div class="col-xs-5 input-group">
										<select name="banco" id="" required class="form-control">
											
											@foreach($bancos as $item)
											@if($registro->bancos_id ==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
							</div>
							<!-- Administrativo -->
							<div class="wizard-pane" role="tabpanel">
								<div class="form-group">
									<label class="col-xs-3 control-label">Tipo funcionario</label>
									<div class="col-xs-5 input-group">
										<select name="tipo_funcionario"  required class="form-control" id="tipo_funcionario_id">											
											@foreach($tipos_funcionarios as $item)
												@if($registro->tipos_funcionario_id ==$item->id)
													<option selected="" value="{{$item->id}}">{{$item->name}}</option>
												@else
													<option value="{{$item->id}}">{{$item->name}}</option>
												@endif
											@endforeach
										</select>
									</div>
								</div>
								@if($registro->tipoFuncionario->getEsDocente())
								<div id="div-docente">
									<div class="form-group">
										<label class="col-xs-3 control-label">¿Aplica Visa?</label>
										<div class="col-xs-5 input-group">
											<select id="visa_trigger" class="form-control" name="visa_seleccion" required >
												<option value="" disabled>Selecciona si aplica</option>
												@if($registro->visas_id > 0)
													<option value="1" selected>Si</option>
													<option value="0">No</option>
												@else
													<option value="0" selected>No</option>
													<option value="1">Si</option>
												@endif
											</select>
										</div>
									</div>
									@if($registro->visas_id > 0)
									<div id="visa_div">
										<div class="form-group">
											<label class="col-xs-3 control-label">Tipo de Visa</label>
											<div class="col-xs-5 input-group">
												<select name="visa" class="form-control">
													<option value="" disabled selected>Selecciona tipo de visa</option>
													@foreach($visas as $item)
														@if($registro->visas_id == $item->id)
															<option value="{{$item->id}}" selected>{{$item->name}}</option>
														@else
															<option value="{{$item->id}}">{{$item->name}}</option>
														@endif
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-3 control-label">Fecha vigencia Visa</label>
											<div class="col-xs-5 input-group">
												<input type="text" class="form-control datepicker-autoclose" name="fecha_visa" placeholder="yyyy-mm-dd" value="{{$registro->fecha_visa}}" />
												<span class="input-group-addon"><i class="icon-calender"></i></span>
											</div>
										</div>
									</div>
									@endif
									<div class="form-group">
										<label class="col-xs-3 control-label">Escalafón</label>
										<div class="col-xs-5 input-group">
											<select name="escalafon" class="form-control" required>
												<option value="" disabled="" selected>Selecciona el escalafon</option>
												@foreach($escalafones as $item)
													@if($registro->escalafones_id == $item->id)
														<option value="{{$item->id}}" selected>{{$item->name}}</option>
													@else
														<option value="{{$item->id}}">{{$item->name}}</option>
													@endif
												@endforeach 
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Registro SIRE</label>
										<div class="col-xs-5 input-group">
											<input type="text" class="form-control" name="registro_sire" value="{{$registro->registro_sire}}" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Observación compromiso</label>
										<div class="col-xs-5 input-group">
											<textarea name="observacion_compromiso" cols="30" rows="10" class="form-control">{{$registro->observacion_compromiso}}</textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Fecha Compromiso</label>
										<div class="col-xs-5 input-group">
											<input type="text" class="form-control datepicker-autoclose" name="fecha_compromiso" placeholder="yyyy-mm-dd" value="{{$registro->fecha_compromiso}}"/>
											<span class="input-group-addon"><i class="icon-calender"></i></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Fecha límite cumplimiento compromiso</label>
										<div class="col-xs-5 input-group">
											<input type="text" class="form-control datepicker-autoclose" name="fecha_limite_compromiso" placeholder="yyyy-mm-dd" value="{{$registro->fecha_limite_compromiso}}"/>
											<span class="input-group-addon"><i class="icon-calender"></i></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Certificado nivel idioma</label>
										<div class="col-xs-5 input-group">
											<input type="text" class="form-control" name="certificado_nivel_idioma" value="{{$registro->certificado_nivel_norma}}" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Examen pago por ASW</label>
										<div class="col-xs-5 input-group">
											<select name="examen_pago" class="form-control" required>
												<option value="" disabled>Selecciona si tiene Examen pago</option>
												@if($registro->examen_pago == 'Si')
													<option value="Si" selected>Si</option>
													<option value="No">No</option>
												@else
													<option value="No" selected>No</option>
													<option value="Si">Si</option>
												@endif
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Observación</label>
										<div class="col-xs-5 input-group">
											<textarea name="observacion_docente" cols="30" rows="10" class="form-control">{{$registro->observacion}}</textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Firma anexo al contrato de certificaciones laborales</label>
										<div class="col-xs-5 input-group">
											<select name="firma_anexo" class="form-control" required>
												<option value="" disabled>Selecciona si firma</option>
												@if($registro->firma_anexo == 'Si')
													<option value="Si" selected>Si</option>
													<option value="No">No</option>
												@else
													<option value="No" selected>No</option>
													<option value="Si">Si</option>
												@endif
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Cumple el perfil</label>
										<div class="col-xs-5 input-group">
											<select name="cumple_perfil" class="form-control" required>
												<option value="" disabled>Selecciona si cumple</option>
												@if($registro->cumple_perfil == 'Si')
													<option value="Si" selected>Si</option>
													<option value="No">No</option>
												@else
													<option value="No" selected>No</option>
													<option value="Si">Si</option>
												@endif
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Pruebas internas docentes</label>
										<div class="col-xs-5 input-group">
											<select name="pruebas_docentes" class="form-control" required>
												<option value="" disabled="" selected>Selecciona la prueba</option> @foreach($pruebas_docentes as $item)
													@if($registro->pruebas_docente_id == $item->id)
														<option value="{{$item->id}}" selected>{{$item->name}}</option>
														@else
														<option value="{{$item->id}}">{{$item->name}}</option>
														@endif
												@endforeach
											</select>
										</div>
									</div>
								</div>
								@endif						
								<div id="div-docente" style="display: none">
									<div class="form-group">
										<label class="col-xs-3 control-label">¿Aplica Visa?</label>
										<div class="col-xs-5 input-group">
											<select id="visa_trigger" name="visa_seleccion"  required class="form-control">
												<option value="" disabled="" selected="" >Selecciona si aplica</option>
												<option value="1">Si</option>
												<option value="0">No</option>
											</select>
										</div>
									</div>
									<div id="visa_div" style="display: none">
										<div class="form-group">
											<label class="col-xs-3 control-label">Tipo de Visa</label>
											<div class="col-xs-5 input-group">
												<select name="visa" class="form-control">
													<option value="" disabled="" selected>Selecciona tipo de visa</option>
													@foreach($visas as $item)
														<option value="{{$item->id}}">{{$item->name}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-3 control-label">Fecha vigencia Visa</label>
											<div class="col-xs-5 input-group">
												<input type="text" class="form-control datepicker-autoclose" name="fecha_visa" placeholder="yyyy-mm-dd" />
												<span class="input-group-addon"><i class="icon-calender"></i></span>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Escalafón</label>
										<div class="col-xs-5 input-group">
											<select name="escalafon" class="form-control" required>
												<option value="" disabled="" selected>Selecciona el escalafon</option>
												@foreach($escalafones as $item)
												<option value="{{$item->id}}">{{$item->name}}</option>
												@endforeach 
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Registro SIRE</label>
										<div class="col-xs-5 input-group">
											<input type="text" class="form-control" name="registro_sire" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Observación compromiso</label>
										<div class="col-xs-5 input-group">
											<textarea name="observacion_compromiso"  cols="30" rows="10" class="form-control"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Fecha Compromiso</label>
										<div class="col-xs-5 input-group">
											<input type="text" class="form-control datepicker-autoclose" name="fecha_compromiso" placeholder="yyyy-mm-dd"/>
											<span class="input-group-addon"><i class="icon-calender"></i></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Fecha límite cumplimiento compromiso</label>
										<div class="col-xs-5 input-group">
											<input type="text" class="form-control datepicker-autoclose" name="fecha_limite_compromiso" placeholder="yyyy-mm-dd"/>
											<span class="input-group-addon"><i class="icon-calender"></i></span>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Certificado nivel idioma</label>
										<div class="col-xs-5 input-group">
											<input type="text" class="form-control" name="certificado_nivel_idioma" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Examen pago por ASW</label>
										<div class="col-xs-5 input-group">
											<select name="examen_pago" class="form-control" required>
												<option value="" disabled="" selected>Selecciona si tiene Examen pago</option>
												<option value="Si">Si</option>
												<option value="No">No</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Observación</label>
										<div class="col-xs-5 input-group">
											<textarea name="observacion_docente" cols="30" rows="10" class="form-control"></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Firma anexo al contrato de certificaciones laborales</label>
										<div class="col-xs-5 input-group">
											<select name="firma_anexo" class="form-control" required>
												<option value="" disabled="" selected>Selecciona si firma</option>
												<option value="Si">Si</option>
												<option value="No">No</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Cumple el perfil</label>
										<div class="col-xs-5 input-group">
											<select name="cumple_perfil" class="form-control" required>
												<option value="" disabled="" selected>Selecciona si cumple</option>
												<option value="Si">Si</option>
												<option value="No">No</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label class="col-xs-3 control-label">Pruebas internas docentes</label>
										<div class="col-xs-5 input-group">
											<select name="pruebas_docentes" class="form-control" required>
												<option value="" disabled="" selected>Selecciona la prueba</option> @foreach($pruebas_docentes as $item)
													<option value="{{$item->id}}">{{$item->name}}</option>
												@endforeach
											</select>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Sede</label>
									<div class="col-xs-5 input-group">
										<select name="sede" id="" required class="form-control">
											@foreach($sedes as $item)
											@if($registro->sedes_id ==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->nombre}}</option>
											@else
											<option value="{{$item->id}}">{{$item->nombre}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<!-- NUEVOS CAMPOS -->
								<div class="form-group">
									<label class="col-xs-3 control-label">Aplica Dotación</label>
									<div class="col-xs-5 input-group">
										<select id="dotacion_trigger" name="dotacion_seleccion" class="form-control" required>
											@if($registro->dotacion_id != 0)
											<option value="1" selected="">Si</option>
											<option value="0">No</option>
											@else
											<option value="1">Si</option>
											<option value="0" selected="">No</option>
											@endif
										</select>
									</div>
								</div>
								<div class="form-group" id="dotacion_div">
									@if($registro->dotacion_id != 0)
									<label class="col-xs-3 control-label">Dotación</label>
									<div class="col-xs-5 input-group">
										<select name="dotacion" class="form-control" required>
											@foreach($dotaciones as $item) 
											@if($item->id != 0)
											<option value="{{$item->id}}" @if($registro->dotacion_id == $item->id) selected="" @endif>{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
									<div class="clearfix"></div><br><br>
									<label class="col-xs-3 control-label">Talla</label>
									<div class="col-xs-5 input-group">
										<select name="talla" class="form-control" required>
											@foreach($tallas as $item)
											@if($item->id != 0)
											<option value="{{$item->id}}" @if($registro->talla_id == $item->id) selected="" @endif>{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
									@endif					
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Entrega Dotación</label>
									<div class="col-xs-5 input-group">
										<select  name="entrega_dotacion" class="form-control" required>
											@if($registro->entrega_dotacion == 'Si')
											<option value="1" selected="">Si</option>
											<option value="0">No</option>
											@else
											<option value="1">Si</option>
											<option value="0" selected="">No</option>
											@endif
										</select>
									</div>
								</div>
								<!-- FIN NUEVOS CAMPOS -->
								<div class="form-group">
									<label class="col-xs-3 control-label">Idioma</label>
									<div class="col-xs-5 input-group">
										<select name="idioma" class="form-control" required>
											@foreach($idiomas as $item)
											@if($registro->idiomas_id ==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Nivel Idioma</label>
									<div class="col-xs-5 input-group">
										<select name="nivel_idioma" class="form-control" required>
											@foreach($niveles as $item)
											@if($registro->niveles_id ==$item->id)
											<option selected="" value="{{$item->id}}">{{$item->name}}</option>
											@else
											<option value="{{$item->id}}">{{$item->name}}</option>
											@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Fecha terminación periodo de prueba</label>
									<div class="col-xs-5 input-group">
										<input type="text" value="{{$registro->fecha_terminacion_prueba}}" class="form-control datepicker-autoclose" name="fecha_terminacion" placeholder="yyyy-mm-dd" />
										<span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Tiene Carné</label>
									<div class="col-xs-5 input-group">
										<select name="tiene_carne" id="" required class="form-control">
											@if($registro->carnet == 'Si')
											<option selected="" value="Si">Si</option>
											<option value="No">No</option>
											@else
											<option value="Si">Si</option>
											<option selected="" value="No">No</option>
											@endif
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Estado Beca</label>
									<div class="col-xs-5 input-group">
										<select name="estado_beca" id="" required class="form-control">
											@if($registro->beca == "Activo")
											<option value="Activo" selected>Activo</option>
											<option value="Inactivo">Inactivo</option>
											@else
											<option value="Activo">Activo</option>
											<option value="Inactivo" selected>Inactivo</option>
											@endif
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Fecha Beca</label>
									<div class="col-xs-5 input-group">
										<input type="text" value="{{$registro->fecha_beca}}" class="form-control datepicker-autoclose" name="fecha_beca" placeholder="yyyy-mm-dd" />
										<span class="input-group-addon"><i class="icon-calender"></i></span>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Idioma de la Beca</label>
									<div class="col-xs-5 input-group">
										<select name="idioma_beca" id="" required class="form-control">
											@foreach($idiomas as $item)
												@if($registro->idiomas_beca == $item->id)
													<option selected="" value="{{$item->id}}">{{$item->name}}</option>
												@else
													<option value="{{$item->id}}">{{$item->name}}</option>
												@endif
											@endforeach
										</select>
									</div>
								</div>
								<div class="form-group">
									<label class="col-xs-3 control-label">Retirado</label>
									<div class="col-xs-5 input-group">
										<select id="retirado_trigger" name="retirado" class="form-control" required>
											@if($registro->retirado == 'Si')
												<option value="Si" selected>Si</option>
												<option value="No">No</option>
											@else
												<option value="No" selected>No</option>
												<option value="Si">Si</option>
											@endif
										</select>
									</div>
								</div>
								@if($registro->retirado == 'Si')
									<div id="div_retirado">
										<div class="form-group" >
											<label class="col-xs-3 control-label">Motivo del retiro</label>
											<div class="col-xs-5 input-group"> 
												<textarea name="motivo_retiro" cols="30" rows="10" class="form-control">{{$registro->motivo_retiro}}</textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-3 control-label">Fecha del retiro</label>
											<div class="col-xs-5 input-group">
												<input type="text" value="{{$registro->fecha_retiro}}" class="form-control datepicker-autoclose" name="fecha_retiro" placeholder="yyyy-mm-dd" />
												<span class="input-group-addon"><i class="icon-calender"></i></span>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-3 control-label">¿Bloqueado?</label>
											<div class="col-xs-5 input-group">
												<select name="bloqueado" class="form-control">
													<option value="" disabled selected=>Selecciona si está bloqueado</option>
													@if($registro->bloqueado == 'Si')
														<option value="Si" selected>Si</option>
														<option value="No">No</option>
													@else
														<option value="No" selected>No</option>
														<option value="Si">Si</option>
													@endif
												</select>
											</div>
										</div>
									</div>
								@else								
									<div id="div_retirado" style="display: none">
										<div class="form-group" >
											<label class="col-xs-3 control-label">Motivo del retiro</label>
											<div class="col-xs-5 input-group"> 
												<textarea name="motivo_retiro" cols="30" rows="10" class="form-control"></textarea>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-3 control-label">Fecha del retiro</label>
											<div class="col-xs-5 input-group">
												<input type="text" value="" class="form-control datepicker-autoclose" name="fecha_retiro" placeholder="yyyy-mm-dd" />
												<span class="input-group-addon"><i class="icon-calender"></i></span>
											</div>
										</div>
										<div class="form-group">
											<label class="col-xs-3 control-label">¿Bloqueado?</label>
											<div class="col-xs-5 input-group">
												<select name="bloqueado" class="form-control">
													<option value="" disabled selected>Selecciona si está bloqueado</option>
													<option value="Si">Si</option>
													<option value="No">No</option>
												</select>
											</div>
										</div>
									</div>
								@endif
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
<!-- /.row -->
@endsection
@section('scripts')

<!-- Form Wizard JavaScript -->
<script src="{{ URL::asset('plugins/bower_components/jquery-wizard-master/dist/jquery-wizard.min.js') }}"></script>
<!-- FormValidation plugin and the class supports validating Bootstrap form -->
<script src="{{ URL::asset('plugins/bower_components/jquery-wizard-master/libs/formvalidation/formValidation.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/jquery.formvalidation/0.6.1/js/language/es_ES.js"></script>
<script src="{{ URL::asset('plugins/bower_components/jquery-wizard-master/libs/formvalidation/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('plugins/bower_components/blockUI/jquery.blockUI.js') }}"></script>
<!-- Sweet-Alert  -->
<script src="{{ URL::asset('plugins/bower_components/sweetalert/sweetalert.min.js') }}"></script>
<script src="{{ URL::asset('plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js') }}"></script>
<script src="{{ URL::asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('plugins/bower_components/bootstrap-datepicker/bootstrap-datetimepicker.es.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<!-- Custom Theme JavaScript -->
<script type="text/javascript">

$('#dotacion_trigger').change(
			function(){
				if($('#dotacion_trigger').val() == 1){
					$('#dotacion_div').empty();
					$('#dotacion_div').append('<label class="col-xs-3 control-label">Dotación</label> <div class="col-xs-5 input-group"> <select name="dotacion"  required class="form-control"> <option value="" disabled="" selected="" >Selecciona la dotación</option> @foreach($dotaciones as $item) @if($item->id != 0) <option value="{{$item->id}}">{{$item->name}}</option> @endif @endforeach </select> </div> <div class="clearfix"></div><br><br> <label class="col-xs-3 control-label">Talla</label> <div class="col-xs-5 input-group"> <select name="talla"  required class="form-control"> <option value="" disabled="" selected="" >Selecciona la talla</option> @foreach($tallas as $item) <option value="{{$item->id}}">{{$item->name}}</option> @endforeach </select> </div>');
				}else{
					$('#dotacion_div').empty();
				}
			}
		);
$('#tipo_funcionario_id').change(function(){
	if ($('#tipo_funcionario_id').val() == '3')
		$('#div-docente').show();
	else
		$('#div-docente').hide();
});
$('#visa_trigger').change(function(){
	if($('#visa_trigger').val() == 1)
		$('#visa_div').show();
	else
		$('#visa_div').hide();
});
$('#retirado_trigger').change(function(){
	if($('#retirado_trigger').val()=="Si")
		$('#div_retirado').show();
	else
		$('#div_retirado').hide();
});
$('#numero_hijos_id').change(function(){
	var count = $('#numero_hijos_id').val();
	$('#div-hijos').empty();
	for (var i = 1; i <= count; i++) {
		$('#div-hijos').append('<div class="form-group"><label class="col-xs-3 control-label">Género Hijo '+i+'</label><div class="col-xs-5 input-group"><select name="hijos_genero[]" required class="form-control"><option value="" disabled selected>Selecciona el género</option><option value="Masculino">Masculino</option><option value="Femenino">Femenino</option></select></div></div><div class="form-group"><label class="col-xs-3 control-label">Fecha de nacimiento hijo '+i+'</label><div class="col-xs-5 input-group"><input type="text" class="form-control datepicker-autoclose-hijos" name="hijos_nacimiento[]" placeholder="yyyy-mm-dd" required/><span class="input-group-addon"><i class="icon-calender"></i></span></div></div><hr></hr>');
	}
	$('.datepicker-autoclose-hijos').datepicker({
		autoclose: true,
		todayHighlight: true,
		format: 'yyyy-mm-dd',
		language: 'es'
	});
});
	$(document).ready(function() {
		$("input[type='phone']").keyup(function() {
			$(this).val($(this).val().replace(/[^\d]/g,''));
		});
	    $('.datepicker-autoclose').datepicker({
		    autoclose: true,
		    todayHighlight: true,
		    format: 'yyyy-mm-dd',
		    language: 'es'
	    });
		$("#nacionalidad").select2({
			placeholder: "Selecciona un país",
			allowClear: true
		});
		$("#barrios").select2({
			placeholder: "Selecciona un barrio",
			allowClear: true
		});
		$("#eps").select2({
			placeholder: "Selecciona una EPS",
			allowClear: true
		});
		$(window).keydown(function(event){
			if(event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});
	});
	(function() {			
		$('#div_wizard').wizard({
			onInit: function() {
				$('#formulario_registro').formValidation({
					locale: 'es_ES',
					framework: 'bootstrap',
				});
			},
			validator: function() {
				var fv = $('#formulario_registro').data('formValidation');
				var $this = $(this);
				fv.validateContainer($this);
				var isValidStep = fv.isValidContainer($this);
				if (isValidStep === false || isValidStep === null) {
					return false;
				}
				return true;
			},
			onFinish: function() {
				var dataForm = $('#formulario_registro').serialize();
				$.ajax({
					url: '{{route("post-edit-funcionario")}}',
					type: 'POST',			
					datatype: 'JSON',
					data: dataForm,
					beforeSend: function (){
						$('div.block3').block({
							message: '<h3><img src="../plugins/images/busy.gif"/> Guardando Información...</h3>',
							css: {
								border: '1px solid #fff'
							}
						});
					},
					success: function(data){
						if(data.success){
							$('div.block3').unblock();
							$('#formulario_registro')[0].reset();
							swal("Perfecto!", "La información se ha editado con éxito!", "success");
						}				
						else{
							$('div.block3').unblock();
							swal("Ups!", "Hubo un error al editar la información por favor verifica los Datos!", "error");
						}
					},
					error:function(){
						$('div.block3').unblock();
						swal("Ups!", "Hubo un error al editar la información por favor verifica los Datos!", "error");
					}
				});
			}
		});

	})();

$("select[name='codigo_contable']").change(function(){
	var codigo_contable_id = $(this).val();
	var token = $("input[name='_token']").val();
	$.ajax({
		url: '{{route("consultar-salarios")}}',
		method: 'POST',
		data: {codigo_contable_id:codigo_contable_id, _token:token},
		success: function(data) {
			$("#salario_div").html('');
			$("#salario_div").html(data.options);
		}
	});
});

function verificarExistencia($numero_documento){
	var tipo_documento_id = $("select[name='tipo_documento']").val();
	var token = $("input[name='_token']").val();
	$.ajax({
		url: '{{route("consultar-existencia")}}',
		method: 'POST',
		data: {numero_documento:$numero_documento, tipo_documento_id:tipo_documento_id, _token:token},
		beforeSend: function (){
			$('div.block3').block({
				message: '<h3><img src="../plugins/images/busy.gif"/> Verificando Número Documento...</h3>',
				centerY: false,
				css: {
					border: '1px solid #fff',
             		top: '500px'
				}
			});
		},
		success: function(data) {
			$('div.block3').unblock();
			if(data.existe){
				swal("Atención!", "Este número de documento ya existe!", "error");
				$("input[name='numero_documento']").val('');
			}
		}
	});
}
</script>
@endsection