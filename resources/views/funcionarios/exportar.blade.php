<!DOCTYPE html>
<html>
<head>
<title>Exportar funcionarios</title>
<link href="../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />

</head>
<body>
<table id="funcionarios" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Tipo Documento</th>
                <th>Número Documento</th>
                <th>Fecha Nacimiento</th>
                <th>Fecha Expedición</th>
                <th>Lugar Expedición</th>
                <th>Nacionalidad</th>
                <th>Género</th>
                <th>Raza</th>
                <th>Email</th>
                <th>Ciudad</th>
                <th>Dirección</th>
                <th>Barrio</th>
                <th>Estrato</th>
                <th>Teléfono Fijo</th>
                <th>Celular</th>
                <th>Contácto Emergencia</th>
                <th>Teléfono Emergencia</th>
                <th>Estado Civil</th>
                <th>Nombre Pareja</th>
                <th>Número Hijos</th>
                <th>Personas a cargo</th>
                <th>Nivel Educativo</th>
                <th>Cargo</th>
                <th>Turno</th>
                <th>Fecha Ingreso</th>
                <th>EPS</th>
                <th>AFP</th>
                <th>Caja de Compensación</th>
                <th>ARL</th>
                <th>Fondo Cesantías</th>
                <th>Número Contratos</th>
                <th>Tipo Contrato</th>
                <th>Salario</th>
                <th>Tipo Cuenta</th>
                <th>Número de Cuenta</th>
                <th>Banco</th>
                <th>Tipo Funcionario</th>
                <th>Sede</th>
                <th>Idioma</th>
                <th>Nivel</th>
                <th>Fecha Terminación Prueba</th>
                <th>Carnet</th>
                <th>Beca</th>
                <th>Fecha Beca</th>
                <th>Idiomas Beca</th>
                <th>Registro sire</th>
                <th>Escalafones</th>
                <th>Observación Compromiso</th>
                <th>Fecha Compromiso</th>
                <th>Fecha Limite Compromiso</th>
                <th>Certificado Nivel Norma</th>
                <th>Examen Pago</th>
                <th>Observación</th>
                <th>Firma Anexo</th>
                <th>Cumple Perfil</th>
                <th>Prueba Docente</th>
                <th>Retirado</th>
                <th>Motivo Retiro</th>
                <th>Fecha Retiro</th>
                <th>Bloqueado</th>
                <th>Subsidio</th>
                <th>Visa</th>
                <th>Talla</th>
                <th>Dotación</th>
                <th>Fecha Visa</th>
                <th>Entrega Dotación</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Nombres</th>
                <th>Apellidos</th>
                <th>Tipo Documento</th>
                <th>Número Documento</th>
                <th>Fecha Nacimiento</th>
                <th>Fecha Expedición</th>
                <th>Lugar Expedición</th>
                <th>Nacionalidad</th>
                <th>Género</th>
                <th>Raza</th>
                <th>Email</th>
                <th>Ciudad</th>
                <th>Dirección</th>
                <th>Barrio</th>
                <th>Estrato</th>
                <th>Teléfono Fijo</th>
                <th>Celular</th>
                <th>Contácto Emergencia</th>
                <th>Teléfono Emergencia</th>
                <th>Estado Civil</th>
                <th>Nombre Pareja</th>
                <th>Número Hijos</th>
                <th>Personas a cargo</th>
                <th>Nivel Educativo</th>
                <th>Cargo</th>
                <th>Turno</th>
                <th>Fecha Ingreso</th>
                <th>EPS</th>
                <th>AFP</th>
                <th>Caja de Compensación</th>
                <th>ARL</th>
                <th>Fondo Cesantías</th>
                <th>Número Contratos</th>
                <th>Tipo Contrato</th>
                <th>Salario</th>
                <th>Tipo Cuenta</th>
                <th>Número de Cuenta</th>
                <th>Banco</th>
                <th>Tipo Funcionario</th>
                <th>Sede</th>
                <th>Idioma</th>
                <th>Nivel</th>
                <th>Fecha Terminación Prueba</th>
                <th>Carnet</th>
                <th>Beca</th>
                <th>Fecha Beca</th>
                <th>Idiomas Beca</th>
                <th>Registro sire</th>
                <th>Escalafones</th>
                <th>Observación Compromiso</th>
                <th>Fecha Compromiso</th>
                <th>Fecha Limite Compromiso</th>
                <th>Certificado Nivel Norma</th>
                <th>Examen Pago</th>
                <th>Observación</th>
                <th>Firma Anexo</th>
                <th>Cumple Perfil</th>
                <th>Prueba Docente</th>
                <th>Retirado</th>
                <th>Motivo Retiro</th>
                <th>Fecha Retiro</th>
                <th>Bloqueado</th>
                <th>Subsidio</th>
                <th>Visa</th>
                <th>Talla</th>
                <th>Dotación</th>
                <th>Fecha Visa</th>
                <th>Entrega Dotación</th>
            </tr>
        </tfoot>
        <tbody>
            @foreach($funcionarios as $funcionario)
                <tr>
                    @if($funcionario->nombres)
                    <td>{{$funcionario->nombres}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->apellidos)
                    <td>{{$funcionario->apellidos}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->tipoDocumento->name)
                    <td>{{$funcionario->tipoDocumento->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->numero_documento)
                    <td>{{$funcionario->numero_documento}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->fecha_nacimiento)
                    <td>{{$funcionario->fecha_nacimiento}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->fecha_expedicion)
                    <td>{{$funcionario->fecha_expedicion}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->lugar_expedicion)
                    <td>{{$funcionario->lugar_expedicion}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->nacionalidad)
                    <td>{{$funcionario->pais->nombre}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->genero)
                    <td>{{$funcionario->genero}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->raza->name))
                    <td>{{$funcionario->raza->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->email)
                    <td>{{$funcionario->email}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->ciudad->nombre))
                    <td>{{$funcionario->ciudad->nombre}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->direccion)
                    <td>{{$funcionario->direccion}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->barrio->name))
                    <td>{{$funcionario->barrio->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->estrato)
                    <td>{{$funcionario->estrato}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->telefono_fijo)
                    <td>{{$funcionario->telefono_fijo}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->celular)
                    <td>{{$funcionario->celular}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->contacto_emergencia)
                    <td>{{$funcionario->contacto_emergencia}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->telefono_emergencia)
                    <td>{{$funcionario->telefono_emergencia}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->estadoCivil->name))
                    <td>{{$funcionario->estadoCivil->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->nombre_pareja)
                    <td>{{$funcionario->nombre_pareja}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->numero_hijos)
                    <td>{{$funcionario->numero_hijos}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->personas_cargo)
                    <td>{{$funcionario->personas_cargo}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->nivelEducativo->name))
                    <td>{{$funcionario->nivelEducativo->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->cargo->name))
                    <td>{{$funcionario->cargo->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->turno->name))
                    <td>{{$funcionario->turno->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->fecha_ingreso)
                    <td>{{$funcionario->fecha_ingreso}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->eps->name))
                    <td>{{$funcionario->eps->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->afps->name))
                    <td>{{$funcionario->afps->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->cajaCompensacion)
                    <td>{{$funcionario->cajaCompensacion->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->arl->name))
                    <td>{{$funcionario->arl->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->fondoCesantias->name))
                    <td>{{$funcionario->fondoCesantias->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->numero_contratos_empresa)
                    <td>{{$funcionario->numero_contratos_empresa}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->tipoContrato->nombre))
                    <td>{{$funcionario->tipoContrato->nombre}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->salario->base))
                    <td>{{$funcionario->salario->base}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->tipoCuenta->name))
                    <td>{{$funcionario->tipoCuenta->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->numero_cuenta)
                    <td>{{$funcionario->numero_cuenta}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->banco->name))
                    <td>{{$funcionario->banco->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->tipoFuncionario->name))
                    <td>{{$funcionario->tipoFuncionario->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->obtenerSede())
                    <td>{{$funcionario->obtenerSede()}}</td> 
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->idioma->name))
                    <td>{{$funcionario->idioma->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->nivel->name))
                    <td>{{$funcionario->nivel->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->fecha_terminacion_prueba)
                    <td>{{$funcionario->fecha_terminacion_prueba}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->carnet)
                    <td>{{$funcionario->carnet}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->beca)
                    <td>{{$funcionario->beca}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->fecha_beca)
                    <td>{{$funcionario->fecha_beca}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->idiomaBeca)
                    <td>{{$funcionario->idiomaBeca->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->registro_sire)
                    <td>{{$funcionario->registro_sire}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->escalafon->name))
                    <td>{{$funcionario->escalafon->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->observacion_compromiso)
                    <td>{{$funcionario->observacion_compromiso}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->fecha_compromiso)
                    <td>{{$funcionario->fecha_compromiso}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->fecha_limite_compromiso)
                    <td>{{$funcionario->fecha_limite_compromiso}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->certificado_nivel_norma)
                    <td>{{$funcionario->certificado_nivel_norma}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->examen_pago)
                    <td>{{$funcionario->examen_pago}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->observacion)
                    <td>{{$funcionario->observacion}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->firma_anexo)
                    <td>{{$funcionario->firma_anexo}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->cumple_perfil)
                    <td>{{$funcionario->cumple_perfil}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->pruebaDocente->name))
                    <td>{{$funcionario->pruebaDocente->name}}</td>

                    @else
                        <td></td>
                    @endif
                    @if($funcionario->retirado)
                    <td>{{$funcionario->retirado}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->motivo_retiro)
                    <td>{{$funcionario->motivo_retiro}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->fecha_retiro)
                    <td>{{$funcionario->fecha_retiro}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->bloqueado)
                    <td>{{$funcionario->bloqueado}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->subsidio)
                    <td>{{$funcionario->subsidio}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->visas->name))
                    <td>{{$funcionario->visas->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->tallas->name))
                    <td>{{$funcionario->tallas->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if(isset($funcionario->dotacion->name))
                    <td>{{$funcionario->dotacion->name}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->fecha_visa)
                    <td>{{$funcionario->fecha_visa}}</td>
                    @else
                        <td></td>
                    @endif
                    @if($funcionario->entrega_dotacion)
                    <td>{{$funcionario->entrega_dotacion}}</td>
                    @else
                        <td></td>
                    @endif
                </tr>
            @endforeach
        </tbody>
    </table>

<script src="/plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js"></script>

<script>
    $(document).ready(function() {
        $('#funcionarios').DataTable({
            dom: 'Bfrtip',
			buttons: [
			'copy', 'excel', 'pdf', 'print'
			],
			"language": {
				"url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
			}
        });
    } );
</script>
</body>
</html>