@extends('layouts.dashboard')
@section('title')
<title>Cosmos - Funcionario {{$registro->nombres." ".$registro->apellidos}}</title>
@endsection
@section('css')
<style> 
    .separador{
        border-bottom: 2px solid #f7fafc;
        padding-top: 15px;
    }
</style>
@endsection
@section('content')
<div class="container-fluid">
    <!-- /row -->
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div class="white-box">
                <ul class="nav nav-tabs tabs customtab">
                    <li class="active tab">
                        <a href="#personal" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Información Personal</span> </a>
                    </li>
                    <li class="tab">
                        <a href="#laboral" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Información Laboral</span> </a>
                    </li>
                    <li class="tab">
                        <a href="#administrativo" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Administrativo</span> </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="personal">
                        <div class="row ">
                            <p><i class="ti-angle-right"></i> Datos Básicos</p>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Nombres</strong>
                                <br>
                                <p class="text-muted">{{$registro->nombres}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Apellidos</strong>
                                <br>
                                <p class="text-muted">{{$registro->apellidos}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Tipo Documento</strong>
                                <br>
                                <p class="text-muted">{{$registro->tipoDocumento->name}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6"> <strong>Número Documento</strong>
                                <br>
                                <p class="text-muted">{{$registro->numero_documento}}</p>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Fecha Nacimiento</strong>
                                <br>
                                <p class="text-muted">{{$registro->fecha_nacimiento}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Fecha Expedición Documento</strong>
                                <br>
                                <p class="text-muted">{{$registro->fecha_expedicion}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Lugar Expediciónn</strong>
                                <br>
                                <p class="text-muted">{{$registro->lugar_expedicion}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 m-t-30 ">
                                <hr> </div>
                            </div>
                            <div class="row clearfix">
                                <p><i class="ti-angle-right"></i> Datos Demograficos</p>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Nacionalidad</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->pais->nombre}}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Género</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->genero}}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Raza</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->raza->name}}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Edad</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->getEdad()}}</p>
                                </div>
                            </div>
                            <div class="row">                                    
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->email}}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Ciudad Residencia</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->ciudad->nombre}}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Dirección</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->dirreccion}}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Barrio</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->barrio->name}}</p>
                                </div>
                            </div>
                            <div class="row">                                    
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Estrato</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->estrato}}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Teléfono Fijo</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->telefono_fijo}}</p>
                                </div>
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Celular</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->celular}}</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 m-t-30 ">
                                    <hr> </div>
                                </div>
                                <div class="row clearfix">
                                    <p><i class="ti-angle-right"></i> Datos Familiares</p>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Contacto de Emergencia</strong>
                                        <br>
                                        <p class="text-muted">{{$registro->contacto_emergencia}}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Teléfono Emergencia</strong>
                                        <br>
                                        <p class="text-muted">{{$registro->telefono_emergencia}}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Estado Civil</strong>
                                        <br>
                                        <p class="text-muted">{{$registro->estadoCivil->name}}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Nombre Pareja</strong>
                                        <br>
                                        @if($registro->nombre_pareja)
                                        <p class="text-muted">{{$registro->nombre_pareja}}</p>
                                        @else
                                        <p class="text-muted">No Aplica</p>
                                        @endif
                                    </div>
                                    
                                </div>
                                <div class="row">                                    
                                    <div class="col-md-3 col-xs-6 b-r"> <strong>Número Hijos</strong>
                                        <br>
                                        <p class="text-muted">{{$registro->numero_hijos}}
                                            @if($registro->hijos->count() > 0)
                                            <a href="#" data-toggle="modal" data-target="#hijos"> Ver</a>
                                            <!-- Modal -->
                                            <div id="hijos" class="modal fade" role="dialog">
                                              <div class="modal-dialog">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    <h4 class="modal-title">Hijos</h4>
                                                </div>
                                                <div class="modal-body">
                                                @foreach($registro->hijos as $hijo)
                                                    <p>{{$hijo->genero}} - Edad: {{$hijo->getEdad()}} </p>
                                                @endforeach
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    @endif
                                </p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>¿Tiene Subsidio?</strong>
                                <br>
                                <p class="text-muted">{{$registro->subsidio}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Personas a cargo</strong>
                                <br>
                                <p class="text-muted">{{$registro->personas_cargo}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="laboral">
                        <div class="row clearfix">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Nivel Educativo</strong>
                                <br>
                                <p class="text-muted">{{$registro->nivelEducativo->name}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Cargo</strong>
                                <br>
                                <p class="text-muted">{{$registro->cargo->name}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Turno</strong>
                                <br>
                                <p class="text-muted">{{$registro->turno->name}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Fecha de Ingreso</strong>
                                <br>
                                <p class="text-muted">{{$registro->fecha_ingreso}}</p>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>EPS</strong>
                                <br>
                                <p class="text-muted">{{$registro->eps->name}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>AFP</strong>
                                <br>
                                <p class="text-muted">{{$registro->afp->name}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Caja Compensación</strong>
                                <br>
                                <p class="text-muted">{{$registro->cajaCompensacion->name}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>ARL</strong>
                                <br>
                                <p class="text-muted">{{$registro->arl->name}}</p>
                            </div>
                        </div>
                        <div class="row clearfix">                                    
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Fondo Cesantías</strong>
                                <br>
                                <p class="text-muted">{{$registro->fondoCesantias->name}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Contratos con la Empresa</strong>
                                <br>
                                <p class="text-muted">{{$registro->numero_contratos_empresa}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Tipo de contrato</strong>
                                <br>
                                <p class="text-muted">{{$registro->tipoContrato->nombre}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Condición contrato</strong>
                                <br>
                                
                                @if(is_null($registro->id_condicion))
                                <p class="text-muted"></p>
                                @else
                                <p class="text-muted">{{$registro->condicionContrato->nombre}}</p>
                                @endif
                                
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Base Salarial</strong>
                                <br>
                                <p class="text-muted">{{$registro->salario->base}}</p>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Bono</strong>
                                <br>
                                <p class="text-muted">{{$registro->salario->bono_alimentacion}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Código contable</strong>
                                <br>
                                <p class="text-muted">{{$registro->salario->codigos->nombre}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Tipo de Cuenta</strong>
                                <br>
                                <p class="text-muted">{{$registro->tipoCuenta->name}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Número de Cuenta</strong>
                                <br>
                                <p class="text-muted">{{$registro->numero_cuenta}}</p>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Banco</strong>
                                <br>
                                <p class="text-muted">{{$registro->banco->name}}</p>
                            </div>
                            @if($registro->fecha_ingreso)
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Días disponibles vacaciones</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->calcularDiasVacaciones()}}</p>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="tab-pane" id="administrativo">
                        <div class="row clearfix">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Tipo Funcionario</strong>
                                <br>
                                <p class="text-muted">{{$registro->tipoFuncionario->name}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Sede</strong>
                                <br>
                                <p class="text-muted">{{$registro->obtenerSede()}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Idioma</strong>
                                <br>
                                <p class="text-muted">{{$registro->idioma->name}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Nivel Idioma</strong>
                                <br>
                                <p class="text-muted">{{$registro->nivel->name}}</p>
                            </div>
                        </div>
                        @if($registro->esDocente())
                        <div class="row clearfix">                            
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Registro SIRE</strong>
                                <br>
                                <p class="text-muted">{{$registro->registro_sire}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Escalafón</strong>
                                <br>
                                <p class="text-muted">{{$registro->escalafon->name}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Observación compromiso</strong>
                                <br>
                                <p class="text-muted">{{$registro->observacion_compromiso}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Fecha Compromiso</strong>
                                <br>
                                <p class="text-muted">{{$registro->fecha_compromiso}}</p>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Fecha Límite de Compromiso</strong>
                                <br>
                                <p class="text-muted">{{$registro->fecha_limite_compromiso}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Certificado nivel idioma</strong>
                                <br>
                                <p class="text-muted">{{$registro->certificado_nivel_norma}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>¿Examen Pago?</strong>
                                <br>
                                <p class="text-muted">{{$registro->examen_pago}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Observación</strong>
                                <br>
                                <p class="text-muted">{{$registro->observacion}}</p>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Firma anexo el contrato de certificaciones internacionales</strong>
                                <br>
                                <p class="text-muted">{{$registro->firma_anexo}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>¿Cumple con el perfil?</strong>
                                <br>
                                <p class="text-muted">{{$registro->cumple_perfil}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Pruebas internas docente</strong>
                                <br>
                                <p class="text-muted">{{$registro->pruebaDocente->name}}</p>
                            </div>                            
                        </div>
                            @if($registro->visas_id != 0)
                            <div class="row clearfix">
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Visa</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->visas->name}}</p>
                                </div> 
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Fecha terminación de la Visa</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->fecha_visa}}</p>
                                </div>                            
                            </div>
                            @endif
                        @endif
                        @if($registro->dotacion_id != 0)
                        <div class="row clearfix">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Dotación</strong>
                                <br>
                                <p class="text-muted">{{$registro->dotacion->name}}</p>
                            </div>
                            @if($registro->tallas_id != '')
                                <div class="col-md-3 col-xs-6 b-r"> <strong>Talla</strong>
                                    <br>
                                    <p class="text-muted">{{$registro->tallas->name}}</p>
                                </div>
                            @endif
                            <div class="col-md-3 col-xs-6 b-r"> <strong>¿Dotación Entregada?</strong>
                                <br>
                                <p class="text-muted">{{$registro->entrega_dotacion}}</p>
                            </div>
                        </div>
                        @endif
                        <div class="row clearfix">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Fecha terminación prueba</strong>
                                <br>
                                <p class="text-muted">{{$registro->fecha_terminacion_prueba}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>¿Tiene Carné?</strong>
                                <br>
                                <p class="text-muted">{{$registro->carnet}}</p>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Estado Beca</strong>
                                <br>
                                <p class="text-muted">{{$registro->beca}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Idioma de la beca</strong>
                                <br>
                                <p class="text-muted">{{$registro->idiomaBeca->name}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Fecha Beca</strong>
                                <br>
                                <p class="text-muted">{{$registro->fecha_beca}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>¿Retirado?</strong>
                                <br>
                                <p class="text-muted">{{$registro->retirado}}</p>
                            </div>
                        </div>
                        @if($registro->retirado == 'Si')
                        <div class="row clearfix">
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Motivo del Retiro</strong>
                                <br>
                                <p class="text-muted">{{$registro->motivo_retiro}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>Fecha del Retiro</strong>
                                <br>
                                <p class="text-muted">{{$registro->fecha_retiro}}</p>
                            </div>
                            <div class="col-md-3 col-xs-6 b-r"> <strong>¿Bloqueado?</strong>
                                <br>
                                <p class="text-muted">{{$registro->bloqueado}}</p>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@endsection