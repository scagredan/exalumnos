<?php 
use App\Cargo;
?>
@extends('layouts.dashboard')
@section('title')
<title>Cosmos - Listado de registros</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('css')
<!-- Wizard CSS -->
<link href="../plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.9/css/fileinput.css" rel="stylesheet" >
<link href="../plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="../plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="../plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="container-fluid">
	<!-- /row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Listado de Funcionarios</h3>
				<p class="text-muted m-b-30"></p>
				<!-- <div class="ui-widget">
					<input id="buscar_funcionario" placeholder="Buscar...">
				</div> -->
				<div class="table-responsive">
					<table id="tabla_registros" class="table table-striped">
						<thead>
							<tr>
								<th>Nombre</th>
								<th>Apellido</th>
								<th>No de Identificación</th>
								<th>Estado</th>
								<th>Tipo Contrato</th>
								<th>Sede</th>
								<th>Acciones</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<!--Modal Asignar Jefe Directo -->
<div id="asignar-jefe" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Asignar Jefe</h4>
			</div>
			<div class="modal-body">
				<form role="form" method="POST" action="{{ url('/funcionarios/asignar-jefe') }}">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="recipient-name" class="control-label"># Identificación Jefe Directo:</label>
						<select name="funcionario_padre_id" id="funcionario_padre_id" class="form-control select2" required >
							<option value="0" disabled="" selected="" >Selecciona el # identificación</option>
							@foreach($funcionarios as $funcionario)
								<option value="{{$funcionario->id}}">{{$funcionario->numero_documento}}</option>
							@endforeach
						</select>
					</div>
					<input type="hidden" name="funcionario_id" id="funcionario_id" value="">
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-danger waves-effect waves-light">Asignar Jefe</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--Fin Modal Asignar Jefe Directo -->

<!--Modal Novedad -->
<div id="novedad-agregar" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title">Ingreso Novedad <b id="funcionario_nombre_novedad"></b></h4>
			</div>
			<div class="modal-body">
				<form role="form" method="POST" action="{{ url('/novedades/crear-novedad') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					<div class="form-group">
						<label for="recipient-name" class="control-label">Tipo Novedad:</label>
						<select name="tipo_novedad" class="form-control" required >
							<option value="" disabled="" selected="">Selecciona el tipo de novedad</option>
							@foreach($novedades as $novedad)
							<option value="{{$novedad->id}}">{{$novedad->name}}</option>
							@endforeach
						</select>
					</div>
					<div id="div_fecha_novedad" class="form-group horas_extras general" style="display: none">
						<label class="control-label">Fecha Novedad</label>
						<div class="input-group">
						<input type="text" class="form-control datepicker-autoclose" name="fecha_novedad" placeholder="yyyy-mm-dd">
							<span class="input-group-addon"><i class="icon-calender"></i></span>
						</div>
					</div>
					<div id="div_modalidad" class="form-group custom-1" style="display: none">
						<label for="recipient-name" class="control-label">Modalidad:</label>
						<select name="modalidad" class="form-control" >
							<option value="" disabled="" selected="">Selecciona la modalidad de la licencia</option>
								<option value="Remunerado">Remunerado</option>
								<option value="Por descuento">Por descuento</option>
								<option value="Por compensar">Por compensar</option>
						</select>
					</div>
                    <div id="div_dias_horas" class="row general" style="display: none">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label># Días</label>
								<input type="radio" id="dias" name="dias_horas" value="dias">
                                <input type="text" id="input_numero_dias" class="form-control" name="numero_dias" readonly>
                             </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label># Horas</label>
								<input type="radio" id="horas" name="dias_horas" value="horas">
                                <input type="text" id="input_numero_horas" class="form-control" name="numero_horas" readonly>
                            </div>
                        </div>
                    </div>
					<div id="div_tipo_hora" class="form-group horas_extras" style="display: none">
						<label for="recipient-name" class="control-label">Tipo:</label>
						<select name="tipo_hora_extra" class="form-control" >
							<option value="" disabled="" selected="">Selecciona tipo de hora extra</option>
							@foreach($tipos_horas_extra as $tipo_hora_extra)
								<option value="{{$tipo_hora_extra->id}}">{{$tipo_hora_extra->name}}</option>
							@endforeach
						</select>
					</div>
					<div id="div_horas_extras_inicial" class="form-group horas_extras" style="display: none">
						<label class="control-label">Hora Inicial:</label>
						<div class="input-group clockpicker" data-placement="bottom" data-align="top" data-autoclose="true">
							<input type="time" class="form-control" value="" name="hora_inicial"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
						</div>
					</div>
					<div id="div_horas_extras_final" class="form-group horas_extras" style="display: none">
						<label class="control-label">Hora Final:</label>
						<div class="input-group clockpicker" data-placement="bottom" data-align="top" data-autoclose="true">
							<input type="time" class="form-control" value="" name="hora_final"> <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
						</div>
					</div>
					<div id="div_nueva_sede" class="form-group cambio_sede" style="display: none">
						<label for="recipient-name" class="control-label">Nueva Sede:</label>
						<select name="nueva_sede" class="form-control" >
							<option value="" disabled="" selected="">Selecciona la sede</option>
							@foreach($sedes as $sede)
								<option value="{{$sede->id}}">{{$sede->nombre}}</option>
							@endforeach
						</select>
					</div>
					<div id="div_fecha_retiro" class="form-group retiro" style="display: none">
						<label class="control-label">Fecha Retiro</label>
						<div class="input-group">
							<input type="text" class="form-control datepicker-autoclose" name="fecha_retiro" placeholder="yyyy-mm-dd">
							<span class="input-group-addon"><i class="icon-calender"></i></span>
						</div>
					</div>
					<div id="div_nuevo_salario" class="form-group cambio_salario" style="display: none">
						<label class="control-label">Nuevo Salario:</label>
						<div class="input-group">
							<input type="text" class="form-control precio" name="nuevo_salario">
							<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
						</div>
					</div>
					<div id="div_nuevo_horario" class="form-group cambio_horario" style="display: none">
						<label class="control-label">Nuevo Horario:</label>
						<input type="text" class="form-control" name="nuevo_horario">
					</div>
					<div id="div_periodo_vacaciones_cumplido" class="form-group vacaciones" style="display: none">
						<label class="control-label">Periodo de vacaciones cumplido:</label>
					</div>
					<div id="div_anio_vacaciones" class="form-group vacaciones" style="display: none">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Año</label>
                                <input type="number" class="form-control" name="anio_vacaciones">
                             </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label># Días Pendientes</label>
                                <input type="number" class="form-control" name="dias_pendientes_vacaciones">
                            </div>
                        </div>
					</div>
					<div id="id_dias_compensados" class="form-group vacaciones" style="display: none">
						<label for="recipient-name" class="control-label">Deseo que 7 días hábiles de mis vacaciones sean compensados en dinero. (No serán tomados para descanso)</label>
						<select name="dias_compensados" class="form-control" >
						<option value="" disabled="" selected="">Selecciona una opción</option>
							<option value="Si">Si</option>
							<option value="No">No</option>
						</select>
					</div>
					<div id="div_fecha_inicio_vacaciones" class="form-group vacaciones" style="display: none">
						<div class="form-group">
							<label>Fecha inicio de comienzo:</label>
							<div class="input-group">
								<input type="text" class="form-control datepicker-autoclose" name="fecha_inicio_vacaciones" placeholder="yyyy-mm-dd">
								<span class="input-group-addon"><i class="icon-calender"></i></span>
							</div>
						</div>
					</div>
					<div id="div_fecha_final_vacaciones" class="form-group vacaciones" style="display: none">
						<div class="form-group">
							<label>Fecha final de regreso:</label>
							<div class="input-group">
								<input type="text" class="form-control datepicker-autoclose" name="fecha_final_vacaciones" placeholder="yyyy-mm-dd">
								<span class="input-group-addon"><i class="icon-calender"></i></span>
							</div>
						</div>
					</div>
					<div id="div_tipo_descuento" class="form-group descuento" style="display: none">
						<label for="recipient-name" class="control-label">Tipo Descuento:</label>
						<select name="tipo_descuento" class="form-control" >
							<option value="" disabled="" selected="">Selecciona el tipo de descuento</option>
							@foreach($tipos_descuentos as $tipo_descuento)
								<option value="{{$tipo_descuento->id}}">{{$tipo_descuento->name}}</option>
							@endforeach
						</select>
					</div>
					<div id="div_valor_descuento" class="form-group descuento" style="display: none">
						<label class="control-label">Valor del descuento en pesos:</label>
						<div class="input-group">
							<input type="text" class="form-control precio" name="valor_descuento">
							<span class="input-group-addon"><i class="fa fa-dollar"></i></span>
						</div>
					</div>
                    <div id="div_numero_cuotas_quincenales" class="row descuento" style="display: none">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label># Cuotas Quincenales</label>
                                <input type="number" class="form-control" name="numero_cuotas_quincenales">
                             </div>
                        </div>
                    </div>
                    <div class="row descuento" id="div_numero_quincena" style="display: none;">
                        <div id="" class="col-md-12">
                            <div class="form-group">
                                <label># Quincena</label>
                                <input type="number" class="form-control" name="numero_quincena">
                            </div>
                        </div>
                    </div>
                    <div id="div_mes_descuento" class="row descuento" style="display: none">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Mes</label>
                                <input type="text" class="form-control" name="mes_descuento">
                             </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Año</label>
                                <input type="number" class="form-control" name="anio_descuento">
                            </div>
                        </div>
                    </div>
                    <div id="div_nombre_cargo" class="form-group custom-1" style="display: none">
						<label for="recipient-name" class="control-label">Nombre del Cargo:</label>
						<select name="nombre_cargo" class="form-control" >
							<option value="" disabled="" selected="">Selecciona el cargo</option>
							@foreach(Cargo::all() as $cargo)
								<option value="{{$cargo->name}}">{{$cargo->name}}</option>
							@endforeach
						</select>


						<!-- <input type="text" class="form-control" name="nombre_cargo"> -->
						</div>
                    <div id="div_fecha_cambio" class="form-group custom-1" style="display: none">
						<div class="form-group">
							<label>Fecha del cambio:</label>
							<div class="input-group">
								<input type="text" class="form-control datepicker-autoclose" name="fecha_cambio" placeholder="yyyy-mm-dd">
								<span class="input-group-addon"><i class="icon-calender"></i></span>
							</div>
						</div>
					</div>

					<div id="div_concepto_descuento" class="form-group descuento" style="display: none">
						<label for="recipient-name" class="control-label">Concepto:</label>
						<input type="text" class="form-control" name="concepto_descuento">
						</div>
					<div id="div_observaciones" class="form-group">
						<label for="recipient-name" class="control-label">Observaciones:</label>					
						<textarea class="form-control" name="observaciones"></textarea>
					</div>
					<div id="div_entrega_soportes" class="form-group horas_extras general" style="display: none">
						<label for="recipient-name" class="control-label">¿Entrega Soportes?</label>
						<select name="entrega_soportes" class="form-control" >
							<option value="" disabled="" selected="">Selecciona una opción</option>
							<option value="Si">Si</option>
							<option value="No">No</option>
							<option value="N/A">N/A</option>
						</select>
					</div>
					<div id="div_soportes_archivos" class="form-group">
						<label for="recipient-name" class="control-label">Soportes:</label>
						<input class="file-input soportes" multiple type="file" name="soportes[]" accept="image/*, application/pdf,.doc,.docx,.xls,.xlsx,.ppt,.pptx, application/pdf,application/ms-word">
					</div>
					<input type="hidden" name="funcionario_id" id="funcionario_id_novedad"  value="">
					<div class="modal-footer">
						<button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Cerrar</button>
						<button type="submit" class="btn btn-danger waves-effect waves-light">Crear Novedad</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!--Fin Modal Usuario Creación -->




@endsection
@section('scripts')
<script src="../plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.9/js/fileinput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/4.3.9/js/locales/es.js"></script>
<script src="https://fastcdn.org/FileSaver.js/1.1.20151003/FileSaver.min.js"></script>
<script src="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="../plugins/bower_components/bootstrap-datepicker/bootstrap-datetimepicker.es.js"></script>
<script src="../plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="../plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="../plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="../plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="../js/jquery.maskMoney.min.js"></script>

<script>

	function modalJefe(id, funcionarioPadre){
		$('#funcionario_id').val(id);
		if(funcionarioPadre){
			$('#funcionario_padre_id').val(funcionarioPadre).trigger('change');
		}else{
			$('#funcionario_padre_id').val(0).trigger('change');
		}
	}

	function modalNovedad(id, nombre){
		$('#funcionario_id_novedad').val(id);
		$('#funcionario_nombre_novedad').text(nombre);
	}

	$(document).ready(function() {

		$( 'input[name="dias_horas"]:radio' ).change(function () {
			if(this.value == 'dias'){
				$('#input_numero_horas').val('');
				$('#input_numero_horas').prop('readonly', true);
				$('#input_numero_dias').prop('readonly', false);
			}else{
				$('#input_numero_dias').val('');
				$('#input_numero_dias').prop('readonly', true);
				$('#input_numero_horas').prop('readonly', false);
			}
		});

		$('#tabla_registros').DataTable( {
			lengthMenu: [
				[ 10, 25, 50, -1 ],
				[ '10', '25', '50', 'Todos' ]
			],
			"processing": true,
			"serverSide": true,
			"ajax": "/funcionariosDatatable",
			// "ajax": {
			// 	"url": "/funcionariosDatatable",
			// 	"type": "POST",
			// 	"headers": {
			// 		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			// 	}
			// },
			"language": {
				"url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
			},
			columns: [
				{ data: 'nombres' , name: 'nombres' },
				{ data: 'apellidos', name: 'apellidos' },
				{ data: 'numero_documento', name: 'numero_documento' },
				{ data: 'estado', name: 'estado' },
				{ data: 'tipo_contrato_nombre', name: 'tipo_contrato_nombre' },
				{ data: 'sede_nombre', name: 'sedes_nombre', orderable: false },
				{ data: 'action', name: 'action', orderable: false, searchable: false, "width": 165 }
			]
		} );

		$(".precio").maskMoney({prefix:'$ ', allowNegative: true, thousands:'.', decimal:',', affixesStay: false, precision: 0});

		$('.datepicker-autoclose').datepicker({
			autoclose: true,
			todayHighlight: true,
			format: 'yyyy-mm-dd',
			language: 'es'
		});
		$("select[name='tipo_novedad']").change(function(){

			if($(this).val() == 1){
				var clasesAOcultar = ['vacaciones','descuento','horas_extras','retiro','cambio_sede','cambio_salario','cambio_horario', 'custom-1'];
				var claseAMostrar = 'general';
				mostrarYOcultarDiv(clasesAOcultar, claseAMostrar);
				setTimeout(function(){ $('#div_fecha_final_vacaciones').fadeIn(); }, 1000);
				
			}
			if($(this).val() == 2){
				var clasesAOcultar = ['vacaciones','descuento','horas_extras','retiro','cambio_sede','cambio_salario','cambio_horario', 'custom-1'];
				var claseAMostrar = 'general';
				mostrarYOcultarDiv(clasesAOcultar, claseAMostrar);
				setTimeout(function(){ $('#div_fecha_final_vacaciones').fadeIn(); $('#div_modalidad').fadeIn();  }, 1000);
				
			}
			if($(this).val() == 4){
				var clasesAOcultar = ['horas_extras','general','descuento','retiro','cambio_sede','cambio_salario','cambio_horario, custom-1'];
				var claseAMostrar = 'vacaciones';
				mostrarYOcultarDiv(clasesAOcultar, claseAMostrar);
			}else
			if($(this).val() == 5){
				var clasesAOcultar = ['vacaciones','general','descuento','retiro','cambio_sede','cambio_salario','cambio_horario', 'custom-1'];
				var claseAMostrar = 'horas_extras';
				mostrarYOcultarDiv(clasesAOcultar, claseAMostrar);
			}else
			if($(this).val() == 7){
				var clasesAOcultar = ['vacaciones','general','descuento','horas_extras','cambio_sede','cambio_salario','cambio_horario', 'custom-1'];
				var claseAMostrar = 'retiro';
				mostrarYOcultarDiv(clasesAOcultar, claseAMostrar);
			}else
			if($(this).val() == 8){
				var clasesAOcultar = ['vacaciones','general','descuento','horas_extras','retiro','cambio_salario','cambio_horario', 'custom-1'];
				var claseAMostrar = 'cambio_sede';
				mostrarYOcultarDiv(clasesAOcultar, claseAMostrar);
				setTimeout(function(){ $('#div_fecha_cambio').fadeIn(); }, 1000);
				
			}else
			if($(this).val() == 9){
				var clasesAOcultar = ['vacaciones','general','descuento','horas_extras','retiro','cambio_sede','cambio_horario', 'custom-1'];
				var claseAMostrar = 'cambio_salario';
				mostrarYOcultarDiv(clasesAOcultar, claseAMostrar);
				$('#div_numero_quincena').fadeIn();
			}else
			if($(this).val() == 10){
				var clasesAOcultar = ['vacaciones','general','descuento','horas_extras','retiro','cambio_sede','cambio_salario', 'custom-1'];
				var claseAMostrar = 'cambio_horario';
				mostrarYOcultarDiv(clasesAOcultar, claseAMostrar);
			}else
			if($(this).val() == 11){
				var clasesAOcultar = ['vacaciones','general','horas_extras','retiro','cambio_sede','cambio_salario','cambio_horario', 'custom-1'];
				var claseAMostrar = 'descuento';
				mostrarYOcultarDiv(clasesAOcultar, claseAMostrar);
			}else
			if($(this).val() == 12){
				$('.general').fadeOut();
				$('.horas_extras').fadeOut();
				$('.vacaciones').fadeOut();
				$('.retiro').fadeOut();
				$('.cambio_sede').fadeOut();
				$('.cambio_salario').fadeOut();
				$('.cambio_horario').fadeOut();
				$('.descuento').fadeOut();
				$('.custom-1').fadeOut();
				$('#div_fecha_inicio_vacaciones').fadeIn();
				$('#div_fecha_final_vacaciones').fadeIn();
			}else
			if($(this).val() == 13){
				$('.general').fadeOut();
				$('.horas_extras').fadeOut();
				$('.vacaciones').fadeOut();
				$('.retiro').fadeOut();
				$('.cambio_sede').fadeOut();
				$('.cambio_salario').fadeOut();
				$('.cambio_horario').fadeOut();
				$('.descuento').fadeOut();
				$('.custom-1').fadeOut();
				$('#div_fecha_inicio_vacaciones').fadeIn();
				$('#div_fecha_final_vacaciones').fadeIn();
			}else
			if($(this).val() == 14){
				$('.general').fadeOut();
				$('.horas_extras').fadeOut();
				$('.vacaciones').fadeOut();
				$('.retiro').fadeOut();
				$('.cambio_sede').fadeOut();
				$('.cambio_salario').fadeOut();
				$('.cambio_horario').fadeOut();
				$('.descuento').fadeOut();
				$('#div_fecha_inicio_vacaciones').fadeIn();
				$('#div_fecha_final_vacaciones').fadeIn();
			}else
			if($(this).val() == 15){
				$('.general').fadeOut();
				$('.horas_extras').fadeOut();
				$('.vacaciones').fadeOut();
				$('.retiro').fadeOut();
				$('.cambio_sede').fadeOut();
				$('.cambio_salario').fadeOut();
				$('.cambio_horario').fadeOut();
				$('.descuento').fadeOut();
				$('.custom-1').fadeOut();
				$('#div_nombre_cargo').fadeIn();
				$('#div_fecha_cambio').fadeIn();
				$('#div_numero_quincena').fadeIn();

				// $('#div_fecha_final_vacaciones').fadeIn();
			}
			else{
				var clasesAOcultar = ['vacaciones','descuento','horas_extras','retiro','cambio_sede','cambio_salario','cambio_horario', 'custom-1'];
				var claseAMostrar = 'general';
				mostrarYOcultarDiv(clasesAOcultar, claseAMostrar);
			}
		});

		$(".soportes").fileinput({
			showUpload:false,
			language: 'es'
		});
		$(".select2").select2();

		function mostrarYOcultarDiv(clasesAOcultar, claseAMostrar){
			for(var i=0; i < clasesAOcultar.length; i++){
				$('.'+clasesAOcultar[i]).hide();
			}
			$('.'+claseAMostrar).fadeIn();
		}


		// $("#buscar_funcionario").autocomplete({
		// 	source: "{{route('buscar-funcionario')}}",
		// 	minLength: 3,
		// 	select: function(event, ui) {
		// 		$('#buscar_funcionario').val(ui.item.value);
		// 		var ruta = ui.item.id;
		// 		window.location = ruta;
		// 	}
		// });

	});

</script>
@if (session('mensaje'))
<script type="text/javascript">
	swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
</script>
@endif
@endsection