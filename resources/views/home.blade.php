@extends('layouts.dashboard')

@section('title')
<title>Cosmos - Inicio</title>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    Has ingresado al sistema!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
