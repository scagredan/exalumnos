
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<div class="navbar-default sidebar" role="navigation" style="padding-top: 20px">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
            <div class="user-profile">
                <div class="dropdown user-pro-body">
                    <div><img src="https://www.mastermagazine.info/termino/wp-content/uploads/Usuario-Icono.jpg" alt="user-img" class="img-circle"></div>
                    <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{Auth::user()->name}} <span class="caret"></span></a>
                    <ul class="dropdown-menu animated flipInY">
                        <li><a href="<?php echo URL::to('logout'); ?>"><i class="fa fa-power-off"></i> Cerrar Sesión</a></li>
                    </ul>
                </div>
            </div>
            <ul class="nav" id="side-menu">
                <li> <a href="/admin/usuarios/index" class="waves-effect"><i class="mdi mdi-account-multiple fa-fw"></i><span class="hide-menu">Usuarios</span></a> </li>
                <!-- <li> <a href="/admin/empresas/index" class="waves-effect"><i class="mdi mdi-view-module fa-fw"></i><span class="hide-menu">Empresas</span></a> </li> -->
                
                <li> <a href="/admin/ofertas/index" class="waves-effect"><i class="mdi mdi-comment-check fa-fw"></i><span class="hide-menu">Ofertas</span></a> </li>
                <!-- <li> <a href="/admin/ocupaciones/index" class="waves-effect"><i class="mdi mdi-account-star-variant fa-fw"></i><span class="hide-menu">Ocupaciones</span></a> </li> -->
                <!-- <li> <a href="/admin/universidades/index" class="waves-effect"><i class="mdi mdi-school fa-fw"></i><span class="hide-menu">Universidades</span></a> </li> -->
                <!-- <li> <a href="/admin/categorias/index" class="waves-effect"><i class="mdi mdi-server fa-fw"></i><span class="hide-menu">Categorias</span></a> </li> -->
                <li> <a href="/admin/categorias/index" class="waves-effect"><i class="mdi mdi-server fa-fw"></i><span class="hide-menu">Categorias</span></a> 
                    <ul class="nav nav-second-level collapse in">
                        <li> <a href="/admin/categorias/index" ><i class=" fa-fw"></i><span class="hide-menu">Areas</span></a> </li>
                        <li> <a href="/admin/empresas/index" ><i class=" fa-fw"></i><span class="hide-menu">Empresas</span></a> </li>
                        <li> <a href="/admin/universidades/index"><i class=" fa-fw"></i><span class="hide-menu">Universidades</span></a> </li>
                        <li> <a href="/admin/ocupaciones/index"><i class=" fa-fw"></i><span class="hide-menu">Ocupaciones</span></a> </li>
                        <li> <a href="/admin/hobbies/index"><i class=" fa-fw"></i><span class="hide-menu">Hobbies</span></a> </li>

                    </ul>
                </li>

                <!-- <li> <a href="/admin/bibliotecas/index" class="waves-effect"><i class="mdi mdi-library-books fa-fw"></i><span class="hide-menu">Biblioteca</span></a> </li> -->
            </ul>

        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Left Sidebar -->
        <!-- ==============================================================