@extends('layouts.dashboard')
@section('title')
<title>Cosmos - Listado de usuarios</title>
@endsection
@section('css')
<!-- Wizard CSS -->
<link href="/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" >
<link href="/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
@endsection
@section('content')

<div class="container-fluid">

	<!-- .row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Editar Usuario</h3>

				<form class="form-horizontal">
					<div class="form-group">
						<label class="col-md-12">Nombre</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese nombre" value="{{$usuario->name}}" name="name" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Primer Apellido</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese el apellido" value="{{$usuario->apellido}}" name="apellido" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Segundo Apellido</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese el apellido" value="{{$usuario->sapellido}}" name="sapellido" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Email</label>
						<div class="col-md-12">
							<input type="email" required="" class="form-control" placeholder="Ingrese el email" value="{{$usuario->email}}" name="correo" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de Graduación</label>
						<div class="col-md-12">
							<input type="number" class="form-control" placeholder="Ingrese el Año" value="{{$usuario->graduacion}}" name="graduacion" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Sexo</label>
						<div class="col-md-12">
							<select name="genero" class="form-control">
								@if($usuario->genero == "M")
								<option value="M" selected="">Masculino</option>
								@else
								<option value="M">Masculino</option>
								@endif
								@if($usuario->genero == "F")
								<option value="F" selected="">Femenino</option>
								@else
								<option value="F">Femenino</option>
								@endif
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Cumpleaños</label>
						<div class="col-md-12">
							<input type="date" class="form-control" placeholder="Ingrese su cumpleaños" value="{{$usuario->cumpleanos}}" name="cumpleanos" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Pais de Nacimiento</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese el apellido" value="{{$usuario->pais_nacimiento}}" name="pais_nacimiento" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Ciudad</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese la ciudad" value="{{$usuario->ciudad}}" name="ciudad" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Celular</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese el celular" value="{{$usuario->celular}}" name="celular" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Teléfono</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese el teléfono" value="{{$usuario->telefono}}" name="telefono" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Dirección</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese la dirección" value="{{$usuario->direccion}}" name="direccion" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Universidad</label>
						<div class="col-md-12">
							<select name="universidad" class="form-control">
								<option value="Selecciona la necesidad" disabled="" selected=""></option>
								@if($usuario->genero == "M")
								<option value="M" selected="">Masculino</option>
								@else
								<option value="M">Masculino</option>
								@endif
								@if($usuario->genero == "F")
								<option value="F" selected="">Femenino</option>
								@else
								<option value="F">Femenino</option>
								@endif
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Descripción</label>
						<div class="col-md-12">
							<textarea type="text" class="form-control" placeholder="Por favor ingrese la descripción"></textarea> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Ocupación </label>
						<div class="col-md-12">
							<select >
								<option value="1">Volvo</option>
								<option value="2">Saab</option>
								<option value="3">Mercedes</option>
								<option value="4">Audi</option>
							</select> </div>
						</div>


						<button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Guardar</button>
						<button type="submit" class="btn btn-primary waves-effect waves-light">Cancelar</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- /.row -->

</div>
@endsection
@section('scripts')
<script src="/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
<script>
	$(document).ready(function() {
		$('.dropify').dropify();
        // $('#tabla_usuarios').DataTable({
        //  "language": {
        //      "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
        //  }
        // });
        $(".select2").select2();

        
    });

</script>
@if (session('mensaje'))
<script type="text/javascript">
	swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
</script>
@endif
@endsection