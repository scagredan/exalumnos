<?php 
use App\Universidad;
use App\Ocupacion; ?>
@extends('layouts.normaldash')
@section('title')
<title>Bienvenido a Networking Bartolino</title>
@endsection
@section('content')
<style>
	.b-r{
		min-height: 52px;
	}
</style>
<div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Bienvenido al  tu perfil</h4> </div>
                    
                </div>
                <!-- /.row -->
                <!-- .row -->
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg">
							@if($usuario->genero == "F")
                             <img width="100%" alt="user" src="../plugins/images/large/img1.jpg">
                             @else
                             <img width="100%" alt="user" src="../plugins/images/users/ritesh.jpg">
                             @endif
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)">
                                        	@if($usuario->genero == "F")
                                        	<img src="../plugins/images/users/genu.jpg" class="thumb-lg img-circle" alt="img"></a>
                                        	@else
                                        	<img src="../plugins/images/users/ritesh.jpg" class="thumb-lg img-circle" alt="img"></a>
                                        	@endif
                                        <h4 class="text-white">{{$usuario->name}} {{$usuario->apellido}}</h4>
                                        <h5 class="text-white">{{$usuario->email}}</h5> </div>
                                </div>
                            </div>
                            <div class="user-btm-box">
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-purple">Generación</p>
                                    <h1>{{$usuario->graduacion}}</h1> </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-blue">Compañeros</p>
                                    <h1>{{$companeros}}</h1> </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-danger">Colegas</p>
                                    <h1>{{$colegas}}</h1> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="white-box">
                            <ul class="nav nav-tabs tabs customtab">
                                <li class="active tab">
                                    <a href="#home" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-home"></i></span> <span class="hidden-xs">Actividad</span> </a>
                                </li>
                                <li class="tab">
                                    <a href="#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Mi Perfil</span> </a>
                                </li>
                                <li class="tab">
                                    <a href="#messages" data-toggle="tab" aria-expanded="true"> <span class="visible-xs"><i class="fa fa-envelope-o"></i></span> <span class="hidden-xs">Mis Ofertas</span> </a>
                                </li>
                                <li class="tab">
                                    <a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Editar</span> </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="home">
                                    <div class="steamline">
                                        @foreach($ofertas as $oferta)
                                        <div class="sl-item">
                                            <div class="sl-left">
												@if($oferta->usuario->genero == "F")
                                            	<img src="/plugins/images/users/genu.jpg" alt="user" class="img-circle" />
												@else
												<img src="/plugins/images/users/ritesh.jpg" alt="user" class="img-circle" />
												@endif
                                            	 </div>
                                            <div class="sl-right">
                                                <div class="m-l-40"><a href="/user/perfil/{{$oferta->usuario->id}}" class="text-info">{{$oferta->usuario->name}} {{$oferta->usuario->apellido}}</a> <span class="sl-date">{{$oferta->created_at}}</span>
                                                    <p class="m-t-10"> <strong>{{$oferta->name}} para {{$oferta->ocupacion->name}}</strong> <br>{{$oferta->descripcion}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        
                                    </div>
                                </div>
                                <div class="tab-pane" id="profile">
                                    <div class="row">
                                    	<div class="col-md-12">
                                    		<p class="m-t-30">Para poder publicar empresas, es necesario tener completo el 100% de la información en Networking Bartolino</p>
                                    <h4 class="font-bold m-t-30">Perfil</h4>
                                    <hr>
                                    <h5>Estado del perfil <span class="pull-right">{{$usuario->completo()}}%</span></h5>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="{{$usuario->completo()}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$usuario->completo()}}%;"> <span class="sr-only">{{$usuario->completo()}}% Completo</span> </div>
                                    </div>
                                    <hr>
                                    	</div>
                                    	
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Nombres y Apellidos</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->name}} {{$usuario->apellido}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Teléfono</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->telefono}}</p>
                                        </div>
                                         <div class="col-md-3 col-xs-6 b-r"> <strong>Celular</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->celular}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->email}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Genero</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->genero}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Cumpleaños</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->cumpleanos}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Pais Nacimiento</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->pais_nacimiento}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Ciudad</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->ciudad}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Dirección</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->direccion}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Profesión Registrada</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->profesion_label}}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Universidad</strong>
                                            <br>
                                             @if(is_null($usuario->universidad))
                                            <p class="text-muted">No se ha registrado</p>
                                            @else
                                            <p class="text-muted">{{$usuario->universidad->name}}</p>
                                            @endif
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Carrera</strong>
                                            <br>
                                            @if(is_null($usuario->ocupacion))
                                            <p class="text-muted">No se ha registrado</p>
                                            @else
                                            <p class="text-muted">{{$usuario->ocupacion->name}}</p>
                                            @endif
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Año de graduación Universidad</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->ano_graduacion}}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Especialización</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->especializacion}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Universidad Especialización</strong>
                                            <br>
                                             @if(is_null($usuario->especializacion))
                                            <p class="text-muted">No se ha registrado</p>
                                            @else
                                            <p class="text-muted">{{$usuario->especializacion->name}}</p>
                                            @endif
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Año graduación Especialización</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->ano_graduacion_especializacion}}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Otro Curso</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->otro_curso}}</p>
                                        </div> 
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Año de graduación otro curso</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->graduacion_otro_curso}}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Empresa o Compañia donde trabaja</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->nombre_compania}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Teléfono (Empresa / Compañia)</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->telefono_compania}}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Estado Civil</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->estado_civil}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Hijos</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->hijos}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Hijos Estudiando</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->hijos_estudiando}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Hijos Graduados</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->hijos_graduados}}</p>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                                <div class="tab-pane" id="messages">
                                    <div class="steamline">
                                        @foreach($misofertas as $oferta)
                                        <div class="sl-item">
                                            <div class="sl-left">
												@if($oferta->usuario->genero == "F")
                                            	<img src="/plugins/images/users/genu.jpg" alt="user" class="img-circle" />
												@else
												<img src="/plugins/images/users/ritesh.jpg" alt="user" class="img-circle" />
												@endif
                                            	 </div>
                                            <div class="sl-right">
                                                <div class="m-l-40"><a href="/perfil/{{$oferta->usuario->id}}" class="text-info">{{$oferta->usuario->name}} {{$oferta->usuario->apellido}}</a> <span class="sl-date">{{$oferta->created_at}}</span>
                                                    <p class="m-t-10"> <strong>{{$oferta->name}} para {{$oferta->ocupacion->name}}</strong> <br>{{$oferta->descripcion}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @if(count($misofertas) == 0)
                                        <p>Todavía no has creado ninguna oferta</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="tab-pane" id="settings">
                                    <form class="form-horizontal" method="post" action="/admin/usuarios/edit">
					<input type="hidden" name="identificador" value="{{$usuario->id}}">
					{{ csrf_field()}}
					<div class="form-group">
						<label class="col-md-12">Nombre</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese nombre" value="{{$usuario->name}}" name="name" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Primer Apellido</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese el apellido" value="{{$usuario->apellido}}" name="apellido" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Segundo Apellido</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese el apellido" value="{{$usuario->sapellido}}" name="sapellido" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Email</label>
						<div class="col-md-12">
							<input type="email" required="" class="form-control" placeholder="Ingrese el email" value="{{$usuario->email}}" name="correo" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de Graduación</label>
						<div class="col-md-12">
							<input type="number" class="form-control" placeholder="Ingrese el Año" value="{{$usuario->graduacion}}" name="graduacion" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Sexo</label>
						<div class="col-md-12">
							<select name="genero" class="form-control">
								@if($usuario->genero == "M")
								<option value="M" selected="">Masculino</option>
								@else
								<option value="M">Masculino</option>
								@endif
								@if($usuario->genero == "F")
								<option value="F" selected="">Femenino</option>
								@else
								<option value="F">Femenino</option>
								@endif
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Cumpleaños</label>
						<div class="col-md-12">
							<input type="date" class="form-control" placeholder="Ingrese su cumpleaños" value="{{$usuario->cumpleanos}}" name="cumpleanos" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Pais de Nacimiento</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese el apellido" value="{{$usuario->pais_nacimiento}}" name="pais_nacimiento" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Ciudad</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese la ciudad" value="{{$usuario->ciudad}}" name="ciudad" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Celular</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese el celular" value="{{$usuario->celular}}" name="celular" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Teléfono</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese el teléfono" value="{{$usuario->telefono}}" name="telefono" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Dirección</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese la dirección" value="{{$usuario->direccion}}" name="direccion" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Universidad</label>
						<div class="col-md-12">
							<select name="universidad_id" class="form-control">
								<option  disabled="" selected="">Selecciona la universidad</option>
								@foreach(Universidad::all() as $universidad)
								@if($usuario->universidad_id == $universidad->id)
								<option value="{{$universidad->id}}" selected="">{{$universidad->name}}</option>
								@else
								<option value="{{$universidad->id}}">{{$universidad->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Ocupación</label>
						<div class="col-md-12">
							<select name="ocupacion_id" class="form-control">
								<option  disabled="" selected="">Selecciona la ocupación</option>
								@foreach(Ocupacion::all() as $ocupacion)
								@if($usuario->ocupacion_id == $ocupacion->id)
								<option value="{{$ocupacion->id}}" selected="">{{$ocupacion->name}}</option>
								@else
								<option value="{{$ocupacion->id}}">{{$ocupacion->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-12">Año de graduación</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese el año de graduación" value="{{$usuario->ano_graduacion}}" name="ano_graduacion" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Especialización</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese la Especialización" value="{{$usuario->especializacion}}" name="especializacion" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Universidad de la Especialización</label>
						<div class="col-md-12">
							<select name="especializacion_id" class="form-control">
								<option  disabled="" selected="">Selecciona la universidad</option>
								@foreach(Universidad::all() as $universidad)
								@if($usuario->especializacion_id == $universidad->id)
								<option value="{{$universidad->id}}" selected="">{{$universidad->name}}</option>
								@else
								<option value="{{$universidad->id}}">{{$universidad->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de graduación Especialización</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese el año de graduación" value="{{$usuario->ano_graduacion_especializacion}}" name="ano_graduacion_especializacion" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Otro curso</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese el nombre del otro curso" value="{{$usuario->otro_curso}}" name="otro_curso" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de graduación Otro Curso</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese el año de graduación" value="{{$usuario->ano_graduacion_especializacion}}" name="ano_graduacion_especializacion" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-12">Nombre Compañia / Empresa</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese el nombre de la compañia" value="{{$usuario->nombre_compania}}" name="nombre_compania" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Teléfono Compañia / Empresa</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese el nombre de la compañia" value="{{$usuario->telefono_compania}}" name="telefono_compania" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Estado Civil</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese el estado civil" value="{{$usuario->estado_civil}}" required="" name="estado_civil" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Hijos</label>
						<div class="col-md-12">
							<input type="number" class="form-control" placeholder="Ingrese los hijos" value="{{$usuario->hijos}}" name="hijos" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Hijos graduados</label>
						<div class="col-md-12">
							<input type="number" class="form-control" placeholder="Ingrese los hijos" value="{{$usuario->hijos_graduados}}" name="hijos_graduados" />
						</div>
					</div>

						<button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Guardar</button>
					</form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="gray" class="yellow-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                                <li><b>With Dark sidebar</b></li>
                                <br/>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="gray-dark" class="yellow-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme working">12</a></li>
                            </ul>
                            <ul class="m-t-20 all-demos">
                                <li><b>Choose other demos</b></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="../plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
@endsection
