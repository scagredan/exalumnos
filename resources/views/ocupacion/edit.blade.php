@extends('layouts.dashboard')
@section('title')
<title>Networking Bartolino - Listado de ocupacions</title>
@endsection
@section('css')
<!-- Wizard CSS -->
<link href="/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" >
<link href="/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
@endsection
@section('content')

<div class="container-fluid">

	<!-- .row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Editar Ocupacion</h3>

				<form class="form-horizontal" action="../edit" method="post">
					<div class="form-group">
						<input type="hidden" name="identificador" value="{{$ocupacion->id}}">
						{{ csrf_field() }}
						<label class="col-md-12">Nombre</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese nombre" value="{{$ocupacion->name}}" name="name" />
						</div>
					</div>
					


						<button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Guardar</button>
						
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- /.row -->

</div>
@endsection
@section('scripts')
<script src="/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
<script>
	$(document).ready(function() {
		$('.dropify').dropify();
        // $('#tabla_ocupacions').DataTable({
        //  "language": {
        //      "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
        //  }
        // });
        $(".select2").select2();

        
    });

</script>
@if (session('mensaje'))
<script type="text/javascript">
	swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
</script>
@endif
@endsection