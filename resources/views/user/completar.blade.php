<?php 
use App\Universidad;
use App\Carrera;
use App\Ocupacion;
use App\Hobbie;

 ?>
@extends('layouts.ample')

@section('title')
<title>Recursos Humanos - Registro</title>

@endsection
@section('css')
<link href="/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css">
@endsection
@section('content')
<section id="wrapper" class="login-register" style="overflow-y: scroll; ">
  <div class="login-box" style="width: 80vw; margin: 1% auto; overflow: visible;">
    <div class="white-box">
      <form action="/admin/usuarios/completar" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="step" value="1">
        <a href="javascript:void(0)" class="text-center db"><img src="/images/logo.png" alt="Home" /><br/></a> 
        <h3 class="box-title m-t-40 m-b-0">Registrate ahora</h3><small>Networking Bartolino es una plataforma exclusiva para exalumnos del colegio Asia Bartolina</small> 
        <div class="form-group m-t-20">
            <label for="">¿Quien eres tú?</label> <br>
            <select name="identificador" id="completar" class="form-control">
                @foreach($alumnos as $alumno)
                <option value="{{$alumno->id}}">{{$alumno->name}} {{$alumno->apellido}}</option>
                @endforeach
            </select>
        </div>
                    <div class="form-group">
                        <label class="col-md-12">Número de documento de identificación</label>
                        <div class="col-md-11">
                            <input type="text" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required="" class="form-control" placeholder="Ingrese el número de documento" name="numero_documento" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_numero_documento" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Email</label>
                        <div class="col-md-12">
                            <input type="email" required="" class="form-control" placeholder="Ingrese el email" name="correo" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Contraseña</label>
                        <div class="col-md-12">
                            <input type="text" required="" class="form-control" placeholder="Ingrese la contraseña" name="password" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Año de Graduación Colegio</label>
                        <div class="col-md-11">
                            <input type="number" class="form-control" placeholder="Ingrese el Año" name="graduacion" required />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_graduacion" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="example-email">Sexo</label>
                        <div class="col-md-11">
                            <select name="genero" class="form-control">
                                <option value="M">Masculino</option>
                                <option value="F" selected="">Femenino</option>
                            </select> 
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_genero" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Cumpleaños</label>
                        <div class="col-md-11">
                            <input type="date" class="form-control" placeholder="Ingrese su cumpleaños"  name="cumpleanos" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_date" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Pais de Nacimiento</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" placeholder="Ingrese el apellido"  name="pais_nacimiento" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_pais_nacimiento" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Ciudad</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" placeholder="Ingrese la ciudad"  name="ciudad" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ciudad" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Celular</label>
                        <div class="col-md-11">
                            <input type="tel" class="form-control" placeholder="Ingrese el celular"  name="celular" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_celular" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Teléfono</label>
                        <div class="col-md-11">
                            <input type="tel" class="form-control" placeholder="Ingrese el teléfono" name="telefono" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_telefono" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Dirección</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" placeholder="Ingrese la dirección" name="direccion" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_direccion" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="example-email">Universidad</label>
                        <div class="col-md-11">
                            <select name="universidad" class="form-control">
                                <option  disabled="" selected="">Selecciona la universidad</option>
                                @foreach(Universidad::all() as $universidad)
                                <option value="{{$universidad->id}}">{{$universidad->name}}</option>
                                @endforeach
                            </select> 
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_universidad" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="example-email">Ocupación</label>
                        <div class="col-md-11">
                            <select name="ocupacion_id" class="form-control">
                                <option  disabled="" selected="">Selecciona la ocupación</option>
                                @foreach(Ocupacion::all() as $ocupacion)
                                <option value="{{$ocupacion->id}}">{{$ocupacion->name}}</option>
                                @endforeach
                            </select> 
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ocupacion_id" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Año de graduación Universidad</label>
                        <div class="col-md-11">
                            <input type="number" class="form-control" placeholder="Ingrese el año de graduación"  name="ano_graduacion" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ano_graduacion" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Especialización</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" placeholder="Ingrese la Especialización"  name="especializacion" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_especializacion" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="example-email">Universidad de la Especialización</label>
                        <div class="col-md-11">
                            <select name="especializacion_id" class="form-control">
                                <option  disabled="" selected="">Selecciona la universidad</option>
                                @foreach(Universidad::all() as $universidad)
                                <option value="{{$universidad->id}}">{{$universidad->name}}</option>
                                @endforeach
                            </select> 
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_especializacion_id" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Año de graduación Especialización</label>
                        <div class="col-md-11">
                            <input type="number" class="form-control" placeholder="Ingrese el año de graduación" name="ano_graduacion_especializacion" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ano_graduacion_especializacion" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Otro curso</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" placeholder="Ingrese el nombre del otro curso" name="otro_curso" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_otro_curso" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Año de graduación Otro Curso</label>
                        <div class="col-md-11">
                            <input type="number" class="form-control" placeholder="Ingrese el año de graduación" name="ano_graduacion_especializacion" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ano_graduacion_especializacion" />
                        </div>
                    </div>
                    <div class="form-group">
						<label class="col-md-12" for="example-email">Hobbie 1</label>
						<div class="col-md-11">
							<select name="hobbie_id" class="form-control">
								<option  disabled="" selected="">Selecciona el hobbie</option>
								@foreach(Hobbie::all() as $hobbie)
								<option value="{{$hobbie->id}}">{{$hobbie->name}}</option>
								@endforeach
							</select> 
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="" />
                        </div>
					</div>
                    <div class="form-group">
						<label class="col-md-12" for="example-email">Hobbie 2</label>
						<div class="col-md-11">
							<select name="hobbie_id_2" class="form-control">
								<option  disabled="" selected="">Selecciona el hobbie</option>
								@foreach(Hobbie::all() as $hobbie)
								<option value="{{$hobbie->id}}">{{$hobbie->name}}</option>
								@endforeach
							</select> 
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="" />
                        </div>
					</div>
                    <div class="form-group">
                        <label class="col-md-12">Nombre Compañia / Empresa</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" placeholder="Ingrese el nombre de la compañia" name="nombre_compania" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_nombre_compania" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Teléfono Compañia / Empresa</label>
                        <div class="col-md-11">
                            <input type="text" class="form-control" placeholder="Ingrese el telefono de la compañia" name="telefono_compania" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_telefono_compania" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Estado Civil</label>
                        <div class="col-md-11">
                            <input type="text" required="" class="form-control" placeholder="Ingrese el estado civil" name="estado_civil" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_estado_civil" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Hijos</label>
                        <div class="col-md-11">
                            <input type="number" class="form-control" placeholder="Ingrese los hijos"  name="hijos" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_hijos" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Hijos graduados</label>
                        <div class="col-md-11">
                            <input type="number" class="form-control" placeholder="Ingrese los hijos" name="hijos_graduados" />
                        </div>
                        <div class="col-md-1 text-center">
                            <label class="">Privado</label>
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_hijos_graduados" />
                        </div>
                    </div>  
                    <br><br><br>  
                    <div class="form-group">
                        <input type="checkbox" required="" name="check">Esta de acuerdo con la <a href="http://www.asiabartolina.org.co/politicas-de-tratamiento-de-informacion/" target="_blank">politica de datos.</a> 
                    </div>
        <div class="form-group text-center m-t-20">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Registrate</button>
        </div>
        <div class="form-group m-b-0">
            <p>¿Ya tienes cuenta? <a href="login" class="text-primary m-l-5"><b>Accede a tu cuenta</b></a></p>
        </div>
    </form>
    </div>
  </div>
</section>

<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

@endsection
@section ('scripts')
<script src="/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>

<script>
    $(document).ready(function() {
    $('#completar').select2();
});
</script>
@if (session('mensaje'))

<script type="text/javascript">
   alert("{{ session('titulo') }} {{ session('mensaje') }}");
</script>
@endif
@endsection