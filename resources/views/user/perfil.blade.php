<?php 
use App\Universidad;
use App\Ocupacion; ?>
@extends('layouts.normaldash')
@section('title')
<title>Bienvenido a Networking Bartolino</title>
@endsection
@section('content')
<style>
	.b-r{
		min-height: 52px;
	}
</style>
<div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Bienvenido al perfil de {{$usuario->name}}</h4> </div>
                    
                </div>
                <!-- /.row -->
                <!-- .row -->
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg">
							@if($usuario->genero == "F")
                             <img width="100%" alt="user" src="/plugins/images/large/img1.jpg">
                             @else
                             <img width="100%" alt="user" src="/plugins/images/users/ritesh.jpg">
                             @endif
                                <div class="overlay-box">
                                    <div class="user-content">
                                        <a href="javascript:void(0)">
                                        	@if($usuario->genero == "F")
                                        	<img src="/plugins/images/users/genu.jpg" class="thumb-lg img-circle" alt="img"></a>
                                        	@else
                                        	<img src="/plugins/images/users/ritesh.jpg" class="thumb-lg img-circle" alt="img"></a>
                                        	@endif
                                        <h4 class="text-white">{{$usuario->name}} {{$usuario->apellido}}</h4>
                                        <h5 class="text-white">{{$usuario->email}}</h5> </div>
                                </div>
                            </div>
                            <div class="user-btm-box">
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-purple">Generación</p>
                                    <h1>{{$usuario->graduacion}}</h1> </div>
                                <div class="col-md-4 col-sm-4 text-center">
                                    <p class="text-blue">Perfil</p>
                                    <h1>{{$usuario->completo()}}%</h1> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12">
                        <div class="white-box" style="padding-bottom: 100px">
                            <ul class="nav nav-tabs tabs customtab">
                                <li class="tab">
                                    <a href="#profile" data-toggle="tab"> <span class="visible-xs"><i class="fa fa-user"></i></span> <span class="hidden-xs">Perfil</span> </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="profile">
                                    <div class="steamline">
                                       <div class="col-md-3 col-xs-6 b-r"> <strong>Nombres y Apellidos</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->name}} {{$usuario->apellido}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Teléfono</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->telefono}}</p>
                                        </div>
                                         <div class="col-md-3 col-xs-6 b-r"> <strong>Celular</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->celular}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->email}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Número de documento</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->numero_documento}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Genero</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->genero}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Cumpleaños</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->cumpleanos}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Pais Nacimiento</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->pais_nacimiento}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Ciudad</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->ciudad}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Dirección</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->direccion}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Profesión Registrada</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->profesion_label}}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Universidad</strong>
                                            <br>
                                             @if(is_null($usuario->universidad))
                                            <p class="text-muted">No se ha registrado</p>
                                            @else
                                            <p class="text-muted">{{$usuario->universidad->name}}</p>
                                            @endif
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Carrera</strong>
                                            <br>
                                            @if(is_null($usuario->ocupacion))
                                            <p class="text-muted">No se ha registrado</p>
                                            @else
                                            <p class="text-muted">{{$usuario->ocupacion->name}}</p>
                                            @endif
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Año de graduación Universidad</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->ano_graduacion}}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Especialización</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->especializacion}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Universidad Especialización</strong>
                                            <br>
                                             @if(is_null($usuario->especializacion))
                                            <p class="text-muted">No se ha registrado</p>
                                            @else
                                            <p class="text-muted">{{$usuario->especializacion->name}}</p>
                                            @endif
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Año graduación Especialización</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->ano_graduacion_especializacion}}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Otro Curso</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->otro_curso}}</p>
                                        </div> 
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Año de graduación otro curso</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->graduacion_otro_curso}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Hobbie</strong>
                                            <br>
                                            @if(is_null($usuario->hobbie_id))
                                            <p class="text-muted">No tiene hobbie registrado</p>
                                            @else
                                            <p class="text-muted">{{$usuario->hobbie->name}}</p>
                                            @endif
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Empresa o Compañia donde trabaja</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->nombre_compania}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Teléfono (Empresa / Compañia)</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->telefono_compania}}</p>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Estado Civil</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->estado_civil}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Hijos</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->hijos}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Hijos Estudiando</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->hijos_estudiando}}</p>
                                        </div>
                                        <div class="col-md-3 col-xs-6 b-r"> <strong>Hijos Graduados</strong>
                                            <br>
                                            <p class="text-muted">{{$usuario->hijos_graduados}}</p>
                                        </div>
                                    </div>
                                </div>
                               
                                
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" data-theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" data-theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" data-theme="gray" class="yellow-theme">3</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue" class="blue-theme">4</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna" class="megna-theme">6</a></li>
                                <li><b>With Dark sidebar</b></li>
                                <br/>
                                <li><a href="javascript:void(0)" data-theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" data-theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" data-theme="gray-dark" class="yellow-dark-theme">9</a></li>
                                <li><a href="javascript:void(0)" data-theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" data-theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" data-theme="megna-dark" class="megna-dark-theme working">12</a></li>
                            </ul>
                            <ul class="m-t-20 all-demos">
                                <li><b>Choose other demos</b></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="/plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="/plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="/plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="/plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="/plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="/plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="/plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="/plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
@endsection
