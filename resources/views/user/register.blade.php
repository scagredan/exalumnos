@extends('layouts.ample')

@section('title')
<title>Recursos Humanos - Registro</title>
@endsection

@section('content')
<section id="wrapper" class="login-register">
  <div class="login-box" style="width: 80vw; margin: 1% auto;">
    <div class="white-box">
      <form action="/register2" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="step" value="2">
        <input type="hidden" name="generacion" value="{{$graduacion}}">
        <input type="hidden" name="nombre" value="{{$nombre}}">
        <input type="hidden" name="apellido" value="{{$apellido}}">
        <a href="javascript:void(0)" class="text-center db"><img src="/images/logo.png" alt="Home" /><br/></a> 
        <h3 class="box-title m-t-40 m-b-0">Registrate ahora</h3><small>Networking Bartolino es una plataforma exclusiva para exalumnos del colegio San Bartolome La Merced</small> 
        <div class="form-group m-t-20">
            <label for="">Cual de estos alumnos es de tu generación</label>
            <div class="row">
            <?php

             shuffle($alumnos); ?>

            @foreach($alumnos as $alumno)

            <div class="col-md-12">
            <input type="radio" required="" name="opcion" value="{{$alumno->id}}"> {{$alumno->name}} {{$alumno->apellido}}    
            </div>
            
            @endforeach
            </div>
        </div>        
        <div class="form-group text-center m-t-20">
            <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Registrate</button>
        </div>
        <div class="form-group m-b-0">
            <p>¿Ya tienes cuenta? <a href="login" class="text-primary m-l-5"><b>Accede a tu cuenta</b></a></p>
        </div>
    </form>
    </div>
  </div>
</section>

<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection
@if (session('mensaje'))
<script type="text/javascript">
    swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
</script>
@endif
