<?php 
use App\Universidad;
use App\Carrera;
use App\Ocupacion;
use App\Hobbie;

 ?>
@extends('layouts.dashboard')
@section('title')
<title>Editar usuarios - Listado de usuarios</title>
@endsection
@section('css')
<!-- Wizard CSS -->
<link href="/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" >
<link href="/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
@endsection
@section('content')
@if(isset($usuario->campos))
<div class="container-fluid">

	<!-- .row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Editar Usuario</h3>

				<form class="form-horizontal" method="post" action="../edit">
					<input type="hidden" name="identificador" value="{{$usuario->id}}">
					{{ csrf_field()}}
					<div class="form-group">
						<label class="col-md-12">Nombre</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese nombre" value="{{$usuario->name}}" name="name" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Primer Apellido</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese el apellido" value="{{$usuario->apellido}}" name="apellido" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Segundo Apellido</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese el apellido" value="{{$usuario->sapellido}}" name="sapellido" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Email</label>
						<div class="col-md-12">
							<input type="email" required="" class="form-control" placeholder="Ingrese el email" value="{{$usuario->email}}" name="correo" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Número de documento</label>
						<div class="col-md-11">
							<input type="text" required="" class="form-control" placeholder="Ingrese el documento" value="{{$usuario->numero_documento}}" name="numero_documento" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->numero_documento)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_numero_documento" checked/>
							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_numero_documento" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de Graduación</label>
						<div class="col-md-11">
							<input type="number" class="form-control" placeholder="Ingrese el Año" value="{{$usuario->graduacion}}" name="graduacion" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->graduacion)
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_graduacion" checked/>
							@else
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_graduacion" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Sexo</label>
						<div class="col-md-11">
							<select name="genero" class="form-control">
								@if($usuario->genero == "M")
								<option value="M" selected="">Masculino</option>
								@else
								<option value="M">Masculino</option>
								@endif
								@if($usuario->genero == "F")
								<option value="F" selected="">Femenino</option>
								@else
								<option value="F">Femenino</option>
								@endif
							</select> 
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->genero)
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_genero" checked/>
							@else
                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_genero" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Cumpleaños</label>
						<div class="col-md-11">
							<input type="date" class="form-control" placeholder="Ingrese su cumpleaños" value="{{$usuario->cumpleanos}}" name="cumpleanos" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->cumpleanos)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_date" checked/>

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_date" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Pais de Nacimiento</label>
						<div class="col-md-11">
							<input type="text" class="form-control" placeholder="Ingrese el apellido" value="{{$usuario->pais_nacimiento}}" name="pais_nacimiento" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->pais_nacimiento)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_pais_nacimiento" checked />

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_pais_nacimiento" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Ciudad</label>
						<div class="col-md-11">
							<input type="text" class="form-control" placeholder="Ingrese la ciudad" value="{{$usuario->ciudad}}" name="ciudad" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->ciudad)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ciudad" checked/>

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ciudad" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Celular</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el celular" value="{{$usuario->celular}}" name="celular" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->celular)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_celular" checked/>

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_celular" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Teléfono</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el teléfono" value="{{$usuario->telefono}}" name="telefono" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->telefono)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_telefono" checked/>

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_telefono" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Dirección</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese la dirección" value="{{$usuario->direccion}}" name="direccion" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->direccion)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_direccion" checked/>

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_direccion" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Universidad</label>
						<div class="col-md-11">
							<select name="universidad_id" class="form-control">
								<option  disabled="" selected="">Selecciona la universidad</option>
								@foreach(Universidad::all() as $universidad)
								@if($usuario->universidad_id == $universidad->id)
								<option value="{{$universidad->id}}" selected="">{{$universidad->name}}</option>
								@else
								<option value="{{$universidad->id}}">{{$universidad->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->universidad_id)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_universidad"  checked/>

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_universidad" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Ocupación</label>
						<div class="col-md-11">
							<select name="ocupacion_id" class="form-control">
								<option  disabled="" selected="">Selecciona la ocupación</option>
								@foreach(Ocupacion::all() as $ocupacion)
								@if($usuario->ocupacion_id == $ocupacion->id)
								<option value="{{$ocupacion->id}}" selected="">{{$ocupacion->name}}</option>
								@else
								<option value="{{$ocupacion->id}}">{{$ocupacion->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->ocupacion_id)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ocupacion_id" checked/>

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ocupacion_id" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Hobbie 1</label>
						<div class="col-md-12">
							<select name="hobbie_id" class="form-control">
								<option  disabled="" selected="">Selecciona el hobbie</option>
								@foreach(Hobbie::all() as $hobbie)
								@if($usuario->hobbie_id == $hobbie->id)
								<option value="{{$hobbie->id}}" selected>{{$hobbie->name}}</option>
								@else
								<option value="{{$hobbie->id}}">{{$hobbie->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Hobbie 2</label>
						<div class="col-md-12">
							<select name="hobbie_id_2" class="form-control">
								<option  disabled="" selected="">Selecciona el hobbie</option>
								@foreach(Hobbie::all() as $hobbie)
								@if($usuario->hobbie_id_2 == $hobbie->id)
								<option value="{{$hobbie->id}}" selected>{{$hobbie->name}}</option>
								@else
								<option value="{{$hobbie->id}}">{{$hobbie->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de graduación</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el año de graduación" value="{{$usuario->ano_graduacion}}" name="ano_graduacion" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->ano_graduacion)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ano_graduacion" checked />

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ano_graduacion" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Especialización</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese la Especialización" value="{{$usuario->especializacion}}" name="especializacion" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->especializacion)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_especializacion" checked />

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_especializacion" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Universidad de la Especialización</label>
						<div class="col-md-11">
							<select name="especializacion_id" class="form-control">
								<option  disabled="" selected="">Selecciona la universidad</option>
								@foreach(Universidad::all() as $universidad)
								@if($usuario->especializacion_id == $universidad->id)
								<option value="{{$universidad->id}}" selected="">{{$universidad->name}}</option>
								@else
								<option value="{{$universidad->id}}">{{$universidad->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->especializacion_id)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_especializacion_id" checked/>

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_especializacion_id" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de graduación Especialización</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el año de graduación" value="{{$usuario->ano_graduacion_especializacion}}" name="ano_graduacion_especializacion" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->ano_graduacion_especializacion)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ano_graduacion_especializacion" checked/>

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ano_graduacion_especializacion" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Otro curso</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el nombre del otro curso" value="{{$usuario->otro_curso}}" name="otro_curso" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->otro_curso)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_otro_curso" checked />

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_otro_curso" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de graduación Otro Curso</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el año de graduación" value="{{$usuario->ano_graduacion_especializacion}}" name="ano_graduacion_especializacion" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->ano_graduacion_especializacion)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ano_graduacion_especializacion" checked/>

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ano_graduacion_especializacion" />

							@endif
                        </div>
					</div>
					
					<div class="form-group">
						<label class="col-md-12">Nombre Compañia / Empresa</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el nombre de la compañia" value="{{$usuario->nombre_compania}}" name="nombre_compania" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->nombre_compania)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_nombre_compania" checked/>

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_nombre_compania" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Teléfono Compañia / Empresa</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el nombre de la compañia" value="{{$usuario->telefono_compania}}" name="telefono_compania" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->telefono_compania)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_telefono_compania" />

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_telefono_compania" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Estado Civil</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el estado civil" value="{{$usuario->estado_civil}}" required="" name="estado_civil" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->estado_civil)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_estado_civil" checked />

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_estado_civil" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Hijos</label>
						<div class="col-md-11">
							<input type="number" class="form-control" placeholder="Ingrese los hijos" value="{{$usuario->hijos}}" name="hijos" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->hijos)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_hijos" checked />

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_hijos" />

							@endif
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Hijos graduados</label>
						<div class="col-md-11">
							<input type="number" class="form-control" placeholder="Ingrese los hijos" value="{{$usuario->hijos_graduados}}" name="hijos_graduados" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							@if($usuario->campos->hijos_graduados)
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_hijos_graduados" checked />

							@else
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_hijos_graduados" />

							@endif
                        </div>
					</div>

						<button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Guardar</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- /.row -->

</div>
@else
<div class="container-fluid">

	<!-- .row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Editar Usuario</h3>

				<form class="form-horizontal" method="post" action="../edit">
					<input type="hidden" name="identificador" value="{{$usuario->id}}">
					{{ csrf_field()}}
					<div class="form-group">
						<label class="col-md-12">Nombre</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese nombre" value="{{$usuario->name}}" name="name" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Primer Apellido</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese el apellido" value="{{$usuario->apellido}}" name="apellido" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Segundo Apellido</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese el apellido" value="{{$usuario->sapellido}}" name="sapellido" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Email</label>
						<div class="col-md-12">
							<input type="email" required="" class="form-control" placeholder="Ingrese el email" value="{{$usuario->email}}" name="correo" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Número de documento</label>
						<div class="col-md-11">
							<input type="text" required="" class="form-control" placeholder="Ingrese el documento" value="{{$usuario->numero_documento}}" name="numero_documento" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_numero_documento" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de Graduación</label>
						<div class="col-md-11">
							<input type="number" class="form-control" placeholder="Ingrese el Año" value="{{$usuario->graduacion}}" name="graduacion" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>

                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_graduacion" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Sexo</label>
						<div class="col-md-11">
							<select name="genero" class="form-control">
								@if($usuario->genero == "M")
								<option value="M" selected="">Masculino</option>
								@else
								<option value="M">Masculino</option>
								@endif
								@if($usuario->genero == "F")
								<option value="F" selected="">Femenino</option>
								@else
								<option value="F">Femenino</option>
								@endif
							</select> 
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>

                            <input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_genero" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Cumpleaños</label>
						<div class="col-md-11">
							<input type="date" class="form-control" placeholder="Ingrese su cumpleaños" value="{{$usuario->cumpleanos}}" name="cumpleanos" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_date" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Pais de Nacimiento</label>
						<div class="col-md-11">
							<input type="text" class="form-control" placeholder="Ingrese el apellido" value="{{$usuario->pais_nacimiento}}" name="pais_nacimiento" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_pais_nacimiento" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Ciudad</label>
						<div class="col-md-11">
							<input type="text" class="form-control" placeholder="Ingrese la ciudad" value="{{$usuario->ciudad}}" name="ciudad" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ciudad" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Celular</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el celular" value="{{$usuario->celular}}" name="celular" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_celular" />
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Teléfono</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el teléfono" value="{{$usuario->telefono}}" name="telefono" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_telefono" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Dirección</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese la dirección" value="{{$usuario->direccion}}" name="direccion" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_direccion" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Universidad</label>
						<div class="col-md-11">
							<select name="universidad_id" class="form-control">
								<option  disabled="" selected="">Selecciona la universidad</option>
								@foreach(Universidad::all() as $universidad)
								@if($usuario->universidad_id == $universidad->id)
								<option value="{{$universidad->id}}" selected="">{{$universidad->name}}</option>
								@else
								<option value="{{$universidad->id}}">{{$universidad->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_universidad" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Ocupación</label>
						<div class="col-md-11">
							<select name="ocupacion_id" class="form-control">
								<option  disabled="" selected="">Selecciona la ocupación</option>
								@foreach(Ocupacion::all() as $ocupacion)
								@if($usuario->ocupacion_id == $ocupacion->id)
								<option value="{{$ocupacion->id}}" selected="">{{$ocupacion->name}}</option>
								@else
								<option value="{{$ocupacion->id}}">{{$ocupacion->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ocupacion_id" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Hobbie 1</label>
						<div class="col-md-12">
							<select name="hobbie_id" class="form-control">
								<option  disabled="" selected="">Selecciona el hobbie</option>
								@foreach(Hobbie::all() as $hobbie)
								@if($usuario->hobbie_id == $hobbie->id)
								<option value="{{$hobbie->id}}" selected>{{$hobbie->name}}</option>
								@else
								<option value="{{$hobbie->id}}">{{$hobbie->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Hobbie 2</label>
						<div class="col-md-12">
							<select name="hobbie_id_2" class="form-control">
								<option  disabled="" selected="">Selecciona el hobbie</option>
								@foreach(Hobbie::all() as $hobbie)
								@if($usuario->hobbie_id_2 == $hobbie->id)
								<option value="{{$hobbie->id}}" selected>{{$hobbie->name}}</option>
								@else
								<option value="{{$hobbie->id}}">{{$hobbie->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de graduación</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el año de graduación" value="{{$usuario->ano_graduacion}}" name="ano_graduacion" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ano_graduacion" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Especialización</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese la Especialización" value="{{$usuario->especializacion}}" name="especializacion" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_especializacion" />
                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Universidad de la Especialización</label>
						<div class="col-md-11">
							<select name="especializacion_id" class="form-control">
								<option  disabled="" selected="">Selecciona la universidad</option>
								@foreach(Universidad::all() as $universidad)
								@if($usuario->especializacion_id == $universidad->id)
								<option value="{{$universidad->id}}" selected="">{{$universidad->name}}</option>
								@else
								<option value="{{$universidad->id}}">{{$universidad->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>

							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_especializacion_id" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de graduación Especialización</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el año de graduación" value="{{$usuario->ano_graduacion_especializacion}}" name="ano_graduacion_especializacion" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ano_graduacion_especializacion" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Otro curso</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el nombre del otro curso" value="{{$usuario->otro_curso}}" name="otro_curso" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>

							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_otro_curso" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de graduación Otro Curso</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el año de graduación" value="{{$usuario->ano_graduacion_especializacion}}" name="ano_graduacion_especializacion" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
	
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_ano_graduacion_especializacion" />

                        </div>
					</div>
					
					<div class="form-group">
						<label class="col-md-12">Nombre Compañia / Empresa</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el nombre de la compañia" value="{{$usuario->nombre_compania}}" name="nombre_compania" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_nombre_compania" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Teléfono Compañia / Empresa</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el nombre de la compañia" value="{{$usuario->telefono_compania}}" name="telefono_compania" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>

							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_telefono_compania" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Estado Civil</label>
						<div class="col-md-11">
							<input type="tel" class="form-control" placeholder="Ingrese el estado civil" value="{{$usuario->estado_civil}}" required="" name="estado_civil" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>

							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_estado_civil" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Hijos</label>
						<div class="col-md-11">
							<input type="number" class="form-control" placeholder="Ingrese los hijos" value="{{$usuario->hijos}}" name="hijos" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_hijos" />

                        </div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Hijos graduados</label>
						<div class="col-md-11">
							<input type="number" class="form-control" placeholder="Ingrese los hijos" value="{{$usuario->hijos_graduados}}" name="hijos_graduados" />
						</div>
						<div class="col-md-1 text-center">
							<label class="">Privado</label>
							<input type="checkbox" style="margin-left: 20px;" class="form-control" name="privado_hijos_graduados" />

                        </div>
					</div>

						<button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Guardar</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- /.row -->

</div>
@endif
@endsection
@section('scripts')
<script src="/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
<script>
	$(document).ready(function() {
		$('.dropify').dropify();
        // $('#tabla_usuarios').DataTable({
        //  "language": {
        //      "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
        //  }
        // });
        $(".select2").select2();

        
    });

</script>
@if (session('mensaje'))
<script type="text/javascript">
	swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
</script>
@endif
@endsection