@extends('layouts.dashboard')
@section('title')
<title>Filtrar usuarios - Listado de usuarios</title>
@endsection
@section('css')
<!-- Wizard CSS -->
<link href="/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
@endsection
@section('content')

<div class="container-fluid">

	<!-- .row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Filtrar usuarios</h3>

				<form class="form-horizontal" method="GET" action="{{ route('usuarios-index') }}">
                    <div class="form-group">
						<label class="col-md-12"> Apellido</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese el apellido" name="apellido" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Sexo</label>
						<div class="col-md-12">
							<select name="genero" class="form-control">
                                <option disabled selected="">Selecciona el género</option>
								<option value="M">Masculino</option>
								<option value="F">Femenino</option>
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Universidad</label>
						<div class="col-md-12">
							<select name="universidad" class="form-control select2">
								<option disabled selected="">Selecciona la universidad</option>
                                @foreach($universidades as $universidad)
                                    <option value="{{ $universidad->id }}">{{ $universidad->name }}</option>
                                @endforeach
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Ocupación</label>
						<div class="col-md-12">
							<select  name="ocupacion" class="form-control select2">
								<option disabled selected>Selecciona la ocupación</option>
                                @foreach($ocupaciones as $ocupacion)
                                    <option value="{{ $ocupacion->id }}">{{ $ocupacion->name }}</option>
                                @endforeach
							</select> 
						</div>
					</div>
                    <div class="form-group">
						<label class="col-md-12" for="example-email">Año de graduación</label>
						<div class="col-md-12">
							<select name="anio_graduacion" class="form-control select2">
								<option disabled selected>Selecciona el año</option>
                                @foreach($anio_graduacion as $anio)
                                    <option value="{{ $anio->graduacion }}">{{ $anio->graduacion }}</option>
                                @endforeach
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Carrera</label>
						<div class="col-md-12">
							<select name="carrera" class="form-control select2">
								<option disabled  selected="">Selecciona la carrera</option>
								@foreach($carreras as $carrera)
								<option value="{{$carrera->id}}">{{$carrera->name}}</option>
								@endforeach
							</select> 
						</div>
					</div>
                    <div class="form-group">
						<label class="col-md-12" for="example-email">Estado civil</label>
						<div class="col-md-12">
							<select name="estado_civil" class="form-control select2">
								<option disabled  selected="">Selecciona el estado civil</option>
								@foreach($estados_civiles as $estado)
								<option value="{{$estado->estado_civil}}">{{$estado->estado_civil}}</option>
								@endforeach
							</select> 
						</div>
					</div>
                    <div class="form-group">
						<label class="col-md-12" for="example-email">Hobbie</label>
						<div class="col-md-12">
							<select name="hobbie" class="form-control select2">
								<option disabled  selected="">Selecciona el hobbie</option>
								@foreach($hobbies as $hobbie)
								<option value="{{$hobbie->id}}">{{$hobbie->name}}</option>
								@endforeach
							</select> 
						</div>
					</div>
						<button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Filtrar</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- /.row -->

</div>
@endsection
@section('scripts')
<script src="/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
<script>
	$(document).ready(function() {
		// $('.dropify').dropify();
        // $('#tabla_usuarios').DataTable({
        //  "language": {
        //      "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
        //  }
        // });
        $(".select2").select2();

        
    });

</script>
@if (session('mensaje'))
<script type="text/javascript">
	swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
</script>
@endif
@endsection