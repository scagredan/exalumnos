<?php 
use App\Universidad;
use App\Carrera;
use App\Ocupacion;
use App\Hobbie;

 ?>
@extends('layouts.dashboard')
@section('title')
<title>Editar usuarios - Listado de usuarios</title>
@endsection
@section('css')
<!-- Wizard CSS -->
<link href="/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" >
<link href="/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
@endsection
@section('content')

<div class="container-fluid">

	<!-- .row -->
	<div class="row">
		<div class="col-sm-12">
			<div class="white-box">
				<h3 class="box-title m-b-0">Crear Usuario</h3>

				<form class="form-horizontal" method="post" action="store">
					{{ csrf_field()}}
					<div class="form-group">
						<label class="col-md-12">Nombres</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese nombre"  name="name" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Primer Apellido</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese el apellido" name="apellido" />
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-md-12">Email</label>
						<div class="col-md-12">
							<input type="email" required="" class="form-control" placeholder="Ingrese el email" name="correo" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Número de documento</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese el documento" name="numero_documento" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Contraseña</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese la contraseña" name="password" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de Graduación Colegio</label>
						<div class="col-md-12">
							<input type="number" class="form-control" placeholder="Ingrese el Año" name="graduacion" required/>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Sexo</label>
						<div class="col-md-12">
							<select name="genero" class="form-control">
								<option value="M">Masculino</option>
								<option value="F" selected="">Femenino</option>
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Cumpleaños</label>
						<div class="col-md-12">
							<input type="date" class="form-control" placeholder="Ingrese su cumpleaños"  name="cumpleanos" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Pais de Nacimiento</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese el apellido"  name="pais_nacimiento" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Ciudad</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese la ciudad"  name="ciudad" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Celular</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese el celular"  name="celular" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Teléfono</label>
						<div class="col-md-12">
							<input type="tel" class="form-control" placeholder="Ingrese el teléfono" name="telefono" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Dirección</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese la dirección" name="direccion" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Universidad</label>
						<div class="col-md-12">
							<select name="universidad" class="form-control">
								<option  disabled="" selected="">Selecciona la universidad</option>
								@foreach(Universidad::all() as $universidad)
								<option value="{{$universidad->id}}">{{$universidad->name}}</option>
								@endforeach
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Ocupación</label>
						<div class="col-md-12">
							<select name="ocupacion_id" class="form-control">
								<option  disabled="" selected="">Selecciona la ocupación</option>
								@foreach(Ocupacion::all() as $ocupacion)
								<option value="{{$ocupacion->id}}">{{$ocupacion->name}}</option>
								@endforeach
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de graduación Universidad</label>
						<div class="col-md-12">
							<input type="number" class="form-control" placeholder="Ingrese el año de graduación"  name="ano_graduacion" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Especialización</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese la Especialización"  name="especializacion" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Universidad de la Especialización</label>
						<div class="col-md-12">
							<select name="especializacion_id" class="form-control">
								<option  disabled="" selected="">Selecciona la universidad</option>
								@foreach(Universidad::all() as $universidad)
								<option value="{{$universidad->id}}">{{$universidad->name}}</option>
								@endforeach
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de graduación Especialización</label>
						<div class="col-md-12">
							<input type="number" class="form-control" placeholder="Ingrese el año de graduación" name="ano_graduacion_especializacion" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Otro curso</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese el nombre del otro curso" name="otro_curso" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Año de graduación Otro Curso</label>
						<div class="col-md-12">
							<input type="number" class="form-control" placeholder="Ingrese el año de graduación" name="ano_graduacion_especializacion" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Hobbie 1</label>
						<div class="col-md-12">
							<select name="hobbie_id" class="form-control">
								<option  disabled="" selected="">Selecciona el hobbie</option>
								@foreach(Hobbie::all() as $hobbie)
								<option value="{{$hobbie->id}}">{{$hobbie->name}}</option>
								@endforeach
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12" for="example-email">Hobbie 2</label>
						<div class="col-md-12">
							<select name="hobbie_id_2" class="form-control">
								<option  disabled="" selected="">Selecciona el hobbie</option>
								@foreach(Hobbie::all() as $hobbie)
								<option value="{{$hobbie->id}}">{{$hobbie->name}}</option>
								@endforeach
							</select> 
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Nombre Compañia / Empresa</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese el nombre de la compañia" name="nombre_compania" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Teléfono Compañia / Empresa</label>
						<div class="col-md-12">
							<input type="text" class="form-control" placeholder="Ingrese el telefono de la compañia" name="telefono_compania" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Estado Civil</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese el estado civil" name="estado_civil" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Hijos</label>
						<div class="col-md-12">
							<input type="number" class="form-control" placeholder="Ingrese los hijos"  name="hijos" />
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12">Hijos graduados</label>
						<div class="col-md-12">
							<input type="number" class="form-control" placeholder="Ingrese los hijos" name="hijos_graduados" />
						</div>
					</div>

						<button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Guardar</button>
					</form>
				</div>
			</div>
		</div>
	</div>

	<!-- /.row -->

</div>
@endsection
@section('scripts')
<script src="/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
<script>
	$(document).ready(function() {
		$('.dropify').dropify();
        // $('#tabla_usuarios').DataTable({
        //  "language": {
        //      "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
        //  }
        // });
        $(".select2").select2();

        
    });

</script>
@if (session('mensaje'))
<script type="text/javascript">
	swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
</script>
@endif
@endsection