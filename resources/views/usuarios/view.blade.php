<?php 
use App\Universidad;
use App\Carrera;
use App\Ocupacion;
use App\Hobbie;

 ?>
@extends('layouts.dashboard')
@section('title')
<title>Editar usuarios - Listado de usuarios</title>
@endsection
@section('css')
<!-- Wizard CSS -->
<link href="/plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/dropzone-master/dist/dropzone.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/dropify/dist/css/dropify.min.css" rel="stylesheet" >
<link href="/plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
<link href="/plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="http://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" rel="Stylesheet"></link>
@endsection
@section('content')

<div class="container-fluid">

    <!-- .row -->
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Ver Usuario</h3>

                <form class="form-horizontal" method="post" action="../edit">
                    <input disabled="" type="hidden" name="identificador" value="{{$usuario->id}}">
                    {{ csrf_field()}}
                    <div class="form-group">
                        <label class="col-md-12">Nombre</label>
                        <div class="col-md-12">
                            <input disabled="" type="text" required="" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->name}}" name="name" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Primer Apellido</label>
                        <div class="col-md-12">
                            <input disabled="" type="text" required="" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->apellido}}" name="apellido" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Segundo Apellido</label>
                        <div class="col-md-12">
                            <input disabled="" type="text" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->sapellido}}" name="sapellido" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Email</label>
                        <div class="col-md-12">
                            <input disabled="" type="email" required="" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->email}}" name="correo" />
                        </div>
                    </div>
                    <div class="form-group">
						<label class="col-md-12">Número de documento</label>
						<div class="col-md-12">
							<input type="text" required="" class="form-control" placeholder="Ingrese el documento" value="{{$usuario->numero_documento}}" name="numero_documento" />
						</div>
					</div>
                    <div class="form-group">
                        <label class="col-md-12">Año de Graduación</label>
                        <div class="col-md-12">
                            <input disabled="" type="number" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->graduacion}}" name="graduacion" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="example-email">Sexo</label>
                        <div class="col-md-12">
                            <select disabled="" name="genero" class="form-control">
                                @if($usuario->genero == "M")
                                <option value="M" selected="">Masculino</option>
                                @else
                                <option value="M">Masculino</option>
                                @endif
                                @if($usuario->genero == "F")
                                <option value="F" selected="">Femenino</option>
                                @else
                                <option value="F">Femenino</option>
                                @endif
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Cumpleaños</label>
                        <div class="col-md-12">
                            <input disabled="" type="date" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->cumpleanos}}" name="cumpleanos" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Pais de Nacimiento</label>
                        <div class="col-md-12">
                            <input disabled="" type="text" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->pais_nacimiento}}" name="pais_nacimiento" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Ciudad</label>
                        <div class="col-md-12">
                            <input disabled="" type="text" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->ciudad}}" name="ciudad" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Celular</label>
                        <div class="col-md-12">
                            <input disabled="" type="tel" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->celular}}" name="celular" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Teléfono</label>
                        <div class="col-md-12">
                            <input disabled="" type="tel" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->telefono}}" name="telefono" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Dirección</label>
                        <div class="col-md-12">
                            <input disabled="" type="tel" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->direccion}}" name="direccion" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="example-email">Universidad</label>
                        <div class="col-md-12">
                            <select disabled="" name="universidad" class="form-control">
                                <option  disabled="" selected="">No ha ingresado información</option>
                                @foreach(Universidad::all() as $universidad)
                                @if($usuario->universidad_id == $universidad->id)
                                <option value="{{$universidad->id}}" selected="">{{$universidad->name}}</option>
                                @else
                                <option value="{{$universidad->id}}">{{$universidad->name}}</option>
                                @endif
                                @endforeach
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="example-email">Ocupación</label>
                        <div class="col-md-12">
                            <select disabled="" name="ocupacion_id" class="form-control">
                                <option  disabled="" selected="">No ha ingresado información</option>
                                @foreach(Ocupacion::all() as $ocupacion)
                                @if($usuario->ocupacion_id == $ocupacion->id)
                                <option value="{{$ocupacion->id}}" selected="">{{$ocupacion->name}}</option>
                                @else
                                <option value="{{$ocupacion->id}}">{{$ocupacion->name}}</option>
                                @endif
                                @endforeach
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">
						<label class="col-md-12" for="example-email">Hobbie 1</label>
						<div class="col-md-12">
							<select disabled name="hobbie_id" class="form-control">
								<option   disabled="" selected="">Selecciona el hobbie</option>
								@foreach(Hobbie::all() as $hobbie)
								@if($usuario->hobbie_id == $hobbie->id)
								<option value="{{$hobbie->id}}" selected>{{$hobbie->name}}</option>
								@else
								<option value="{{$hobbie->id}}">{{$hobbie->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
					</div>
                    <div class="form-group">
						<label class="col-md-12" for="example-email">Hobbie 2</label>
						<div class="col-md-12">
							<select disabled name="hobbie_id" class="form-control">
								<option  disabled="" selected="">Selecciona el hobbie</option>
								@foreach(Hobbie::all() as $hobbie)
								@if($usuario->hobbie_id_2 == $hobbie->id)
								<option value="{{$hobbie->id}}" selected>{{$hobbie->name}}</option>
								@else
								<option value="{{$hobbie->id}}">{{$hobbie->name}}</option>
								@endif
								@endforeach
							</select> 
						</div>
					</div>
                    <div class="form-group">
                        <label class="col-md-12">Año de graduación</label>
                        <div class="col-md-12">
                            <input disabled="" type="tel" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->ano_graduacion}}" name="ano_graduacion" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Especialización</label>
                        <div class="col-md-12">
                            <input disabled="" type="tel" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->especializacion}}" name="especializacion" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" for="example-email">Universidad de la Especialización</label>
                        <div class="col-md-12">
                            <select disabled="" name="especializacion_id" class="form-control">
                                <option  disabled="" selected="">No ha ingresado información</option>
                                @foreach(Universidad::all() as $universidad)
                                @if($usuario->especializacion_id == $universidad->id)
                                <option value="{{$universidad->id}}" selected="">{{$universidad->name}}</option>
                                @else
                                <option value="{{$universidad->id}}">{{$universidad->name}}</option>
                                @endif
                                @endforeach
                            </select> 
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Año de graduación Especialización</label>
                        <div class="col-md-12">
                            <input disabled="" type="tel" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->ano_graduacion_especializacion}}" name="ano_graduacion_especializacion" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Otro curso</label>
                        <div class="col-md-12">
                            <input disabled="" type="tel" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->otro_curso}}" name="otro_curso" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Año de graduación Otro Curso</label>
                        <div class="col-md-12">
                            <input disabled="" type="tel" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->ano_graduacion_especializacion}}" name="ano_graduacion_especializacion" />
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label class="col-md-12">Nombre Compañia / Empresa</label>
                        <div class="col-md-12">
                            <input disabled="" type="tel" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->nombre_compania}}" name="nombre_compania" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Teléfono Compañia / Empresa</label>
                        <div class="col-md-12">
                            <input disabled="" type="tel" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->telefono_compania}}" name="telefono_compania" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Estado Civil</label>
                        <div class="col-md-12">
                            <input disabled="" type="tel" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->estado_civil}}" name="estado_civil" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Hijos</label>
                        <div class="col-md-12">
                            <input disabled="" type="number" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->hijos}}" name="hijos" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Hijos graduados</label>
                        <div class="col-md-12">
                            <input disabled="" type="number" class="form-control" placeholder="No ha ingresado información" value="{{$usuario->hijos_graduados}}" name="hijos_graduados" />
                        </div>
                    </div>

                        <button type="submit" class="btn btn-primary waves-effect waves-light m-r-10">Guardar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- /.row -->

</div>
@endsection
@section('scripts')
<script src="/plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>
<script src="/plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
<script src="/plugins/bower_components/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.js" ></script>
<script>
    $(document).ready(function() {
        $('.dropify').dropify();
        // $('#tabla_usuarios').DataTable({
        //  "language": {
        //      "url": "https://cdn.datatables.net/plug-ins/1.10.11/i18n/Spanish.json"
        //  }
        // });
        $(".select2").select2();

        
    });

</script>
@if (session('mensaje'))
<script type="text/javascript">
    swal("{{ session('titulo') }}", "{{ session('mensaje') }}", "{{ session('tipo') }}");
</script>
@endif
@endsection