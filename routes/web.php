<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Route::get('/mailable', function () {
	Mail::to(App\User::where('email', 'LIKE', '%@%.%')->get())->send(new App\Mail\UsuariosRegistradosSemana());
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/admin/usuarios/edit', 'UsuariosController@update')->name('usuarios-edit-post');
Route::post('/admin/usuarios/completar', 'UsuariosController@completar')->name('usuarios-completar-post');


Auth::routes();

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::get('/home', 'HomeController@index')->name('home');
Route::post('/register2', 'UsuariosController@registrar');

Route::group(array('before' => 'AuthCheck','prefix' => 'user', 'middleware' => ['auth']), function()
{
	Route::group(array('prefix' => 'empresas'), function()
{
	Route::get('/index', 'EmpresasController@index');
	Route::get('/store', 'EmpresasController@storeView');
	Route::post('/store', 'EmpresasController@store');
	Route::get('/view/{identificador}', 'EmpresasController@view');
	Route::get('/edit/{identificador}', 'EmpresasController@editOne');
	Route::post('/edit', 'EmpresasController@update');
	Route::post('/delete', 'EmpresasController@delete');
});
	Route::group(array('prefix' => 'ofertas'), function()
{
	Route::get('/index', 'OfertaController@index');
	Route::get('/store', 'OfertaController@storeView');
	Route::post('/store', 'OfertaController@store');
	Route::get('/view/{identificador}', 'OfertaController@view');
	Route::get('/edit/{identificador}', 'OfertaController@editOne');
	Route::post('/edit', 'OfertaController@update');
	Route::post('/delete', 'OfertaController@delete');
});

	Route::get('/perfil/{identificador}', 'UsuariosController@perfil');

});
Route::group(array('before' => 'AuthCheck','prefix' => 'admin', 'middleware' => ['auth']), function()
{
// Usuarios
Route::group(array('prefix' => 'usuarios'), function()
{
	Route::get('/index', 'UsuariosController@index')->name('usuarios-index');
	Route::get('/store', 'UsuariosController@storeView')->name('usuarios-store');
	Route::post('/store', 'UsuariosController@store')->name('usuarios-store-post');
	Route::get('/view/{identificador}', 'UsuariosController@view')->name('usuarios-view');
	Route::get('/edit/{identificador}', 'UsuariosController@editOne')->name('usuarios-edit');
	Route::post('/delete', 'UsuariosController@delete')->name('usuarios-delete');
	Route::get('/filter', 'UsuariosController@filter')->name('usuarios-filter');
	Route::get('/export', 'UsuariosController@exportarExcel')->name('usuarios-export');
});

// Empresas
Route::group(array('prefix' => 'empresas'), function()
{
	Route::get('/index', 'EmpresasController@index')->name('empresas-index');
	Route::get('/store', 'EmpresasController@storeView')->name('empresas-store');
	Route::post('/store', 'EmpresasController@store')->name('usuarios-store-post');
	Route::get('/view/{identificador}', 'EmpresasController@view')->name('empresas-view');
	Route::get('/edit/{identificador}', 'EmpresasController@editOne')->name('empresas-edit');
	Route::post('/edit', 'EmpresasController@update')->name('empresas-edit-post');
	Route::post('/delete', 'EmpresasController@delete')->name('empresas-delete');
});

// Necesidades
Route::group(array('prefix' => 'necesidades'), function()
{
	Route::get('/index', 'NecesidadController@index')->name('necesidades-index');
	Route::get('/store', 'NecesidadController@store')->name('necesidades-store');
	Route::get('/view/{identificador}', 'NecesidadController@view')->name('necesidades-view');
	Route::get('/edit/{identificador}', 'NecesidadController@editOne')->name('necesidades-edit');
	Route::post('/edit', 'NecesidadController@update')->name('necesidades-edit-post');
	Route::post('/delete', 'NecesidadController@delete')->name('necesidades-delete');
});

// Ofertas
Route::group(array('prefix' => 'ofertas'), function()
{
	Route::get('/index', 'OfertaController@index')->name('ofertas-index');
	Route::get('/store', 'OfertaController@storeView')->name('ofertas-store');
	Route::post('/store', 'OfertaController@store')->name('ofertas-store-post');
	Route::get('/view/{identificador}', 'OfertaController@view')->name('ofertas-view');
	Route::get('/edit/{identificador}', 'OfertaController@editOne')->name('ofertas-edit');
	Route::post('/edit', 'OfertaController@update')->name('ofertas-edit-post');
	Route::post('/delete', 'OfertaController@delete')->name('ofertas-delete');
});

// Ocupaciones
Route::group(array('prefix' => 'ocupaciones'), function()
{
	Route::get('/index', 'OcupacionController@index')->name('ocupaciones-index');
	Route::get('/store', 'OcupacionController@storeView')->name('ocupaciones-store');
	Route::post('/store', 'OcupacionController@store')->name('ocupaciones-store-post');
	Route::get('/view/{identificador}', 'OcupacionController@view')->name('ocupaciones-view');
	Route::get('/edit/{identificador}', 'OcupacionController@editOne')->name('ocupaciones-edit');
	Route::post('/edit', 'OcupacionController@update')->name('ocupaciones-edit-post');
	Route::post('/delete', 'OcupacionController@delete')->name('ocupaciones-delete');
});

// Universidades
Route::group(array('prefix' => 'universidades'), function()
{
	Route::get('/index', 'UniversidadController@index')->name('universidades-index');
	Route::get('/store', 'UniversidadController@storeView')->name('universidades-store');
	Route::post('/store', 'UniversidadController@store')->name('universidades-store-post');
	Route::get('/view/{identificador}', 'UniversidadController@view')->name('universidades-view');
	Route::get('/edit/{identificador}', 'UniversidadController@editOne')->name('universidades-edit');
	Route::post('/edit', 'UniversidadController@update')->name('universidades-edit-post');
	Route::post('/delete', 'UniversidadController@delete')->name('universidades-delete');
});

// Categorias
Route::group(array('prefix' => 'categorias'), function()
{
	Route::get('/index', 'CategoriaController@index')->name('categorias-index');
	Route::get('/store', 'CategoriaController@storeView')->name('categorias-store');
	Route::post('/store', 'CategoriaController@store')->name('categorias-store-post');
	Route::get('/view/{identificador}', 'CategoriaController@view')->name('categorias-view');
	Route::get('/edit/{identificador}', 'CategoriaController@editOne')->name('categorias-edit');
	Route::post('/edit', 'CategoriaController@update')->name('categorias-edit-post');
	Route::post('/delete', 'CategoriaController@delete')->name('categorias-delete');
});

// Hobbies
Route::group(array('prefix' => 'hobbies'), function()
{
	Route::get('/index', 'HobbieController@index')->name('hobbies-index');
	Route::get('/store', 'HobbieController@storeView')->name('hobbies-store');
	Route::post('/store', 'HobbieController@store')->name('hobbies-store-post');
	Route::get('/view/{identificador}', 'HobbieController@view')->name('hobbies-view');
	Route::get('/edit/{identificador}', 'HobbieController@editOne')->name('hobbies-edit');
	Route::post('/edit', 'HobbieController@update')->name('hobbies-edit-post');
	Route::post('/delete', 'HobbieController@delete')->name('hobbies-delete');
});

// biblioteca
Route::group(array('prefix' => 'bibliotecas'), function()
{
	Route::get('/index', 'BibliotecaController@index')->name('biblioteca-index');
	Route::get('/store', 'BibliotecaController@store')->name('biblioteca-store');
	Route::get('/view/{identificador}', 'BibliotecaController@view')->name('biblioteca-view');
	Route::get('/edit/{identificador}', 'BibliotecaController@editOne')->name('biblioteca-edit');
	Route::post('/edit', 'BibliotecaController@update')->name('biblioteca-edit-post');
	Route::post('/delete', 'BibliotecaController@delete')->name('biblioteca-delete');
});



});